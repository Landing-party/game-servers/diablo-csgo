#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <multicolors>
#include <cstrike>
#include <fpvm_interface>
#include <war3_vip>
//#include <santa>

new bool:bIsVSHRunning;

#define CROSSBOW_MODEL "models/weapons/v_crossbow.mdl" // custom view model
#define CROSSBOW_WORLDMODEL "models/weapons/w_crossbow_mp.mdl" // custom view model

new g_crossModel;
new g_wcrossModel;

#include <autoexecconfig> //AutoExecConfig_SetFile etc

#define PLUGIN_VERSION "0.1"
#define DIABLO_DB "dbmod"

#define MAXRACES 8
#define MAX_FLASH 75
#define MAX_WEAPONS 10

#define  TEAM_SPECTATOR 1
#define  TEAM_T 2
#define  TEAM_CT 3

#define CSAddon_NONE            0
#define CSAddon_Flashbang1      (1<<0)
#define CSAddon_Flashbang2      (1<<1)
#define CSAddon_HEGrenade       (1<<2)
#define CSAddon_SmokeGrenade    (1<<3)
#define CSAddon_C4              (1<<4)
#define CSAddon_DefuseKit       (1<<5)
#define CSAddon_PrimaryWeapon   (1<<6)
#define CSAddon_SecondaryWeapon (1<<7)
#define CSAddon_Holster         (1<<8) 

// The following are from the SourceSDK, used for the optional flags field of War3_FlashScreen
#define FFADE_IN            0x0001        // Just here so we don't pass 0 into the function, YOUR SCREEN DECAYS AND YOU SEE CLEARLY SLOWLY
#define FFADE_OUT            0x0002        // Fade out (not in)
#define FFADE_MODULATE        0x0004        // Modulate (don't blend)
#define FFADE_STAYOUT        0x0008        // ignores the duration, stays faded out until new ScreenFade message received
#define FFADE_PURGE            0x0010        // Purges all other fades, replacing them with this one
#define FFADE_OFF            0x0010        // Purges all other fades, replacing them with this one

// The following are color presets for the War3_FlashScreen function
#define RGBA_COLOR_RED        {255,0,0,20} //FOR FLASHLIGHT SCREEN
#define RGBA_COLOR_GREEN    {0,255,0,50}
#define RGBA_COLOR_BLUE        {0,0,255,50} //FOR FLASHLIGHT SCREEN
#define RGBA_COLOR_YELLOW    {255,255,0,3}
#define RGBA_COLOR_ORANGE    {255,69,0,3}
#define RGBA_COLOR_PURPLE    {128,0,128,3}
#define RGBA_COLOR_CYAN        {255,0,255,3}
#define RGBA_COLOR_WHITE    {255,255,255,3}
#define RGBA_COLOR_BLACK    {0,0,0,3}
#define RGBA_COLOR_GREY        {128,128,128,3}
#define RGBA_COLOR_PINK        {255,20,147,3}
#define RGBA_COLOR_MAROON    {128,0,0,3}
#define RGBA_COLOR_SKYBLUE    {135,206,25,3}
#define RGBA_COLOR_GOLD        {255,215,0,3}
#define RGBA_COLOR_BROWN    {139,69,19,3}
#define RGBA_COLOR_VIOLET    {238,130,238,3}
#define RGBA_COLOR_BLIND    {255,155,50,230}

#define IN_ATTACK		(1 << 0)
#define IN_JUMP			(1 << 1)
#define IN_DUCK			(1 << 2)
#define IN_FORWARD		(1 << 3)
#define IN_BACK			(1 << 4)
#define IN_USE			(1 << 5)
#define IN_CANCEL		(1 << 6)
#define IN_LEFT			(1 << 7)
#define IN_RIGHT		(1 << 8)
#define IN_MOVELEFT		(1 << 9)
#define IN_MOVERIGHT	(1 << 10)
#define IN_ATTACK2		(1 << 11)
#define IN_RUN			(1 << 12)
#define IN_RELOAD		(1 << 13)
#define IN_ALT1			(1 << 14)
#define IN_ALT2			(1 << 15)
#define IN_SCORE		(1 << 16)   // Used by client.dll for when scoreboard is held down
#define IN_SPEED		(1 << 17)	// Player is holding the speed key
#define IN_WALK			(1 << 18)	// Player holding walk key
#define IN_ZOOM			(1 << 19)	// Zoom key for HUD zoom
#define IN_WEAPON1		(1 << 20)	// weapon defines these bits
#define IN_WEAPON2		(1 << 21)	// weapon defines these bits
#define IN_BULLRUSH		(1 << 22)
#define IN_GRENADE1		(1 << 23)	// grenade 1
#define IN_GRENADE2		(1 << 24)	// grenade 2

// How long to wait for a reconnect after a failed connection attempt to the database?
#define RECONNECT_INTERVAL 360.0

#define SPECTATOR_NONE 				0
#define SPECTATOR_FIRSTPERSON 		4
#define SPECTATOR_3RDPERSON 		5
#define SPECTATOR_FREELOOK	 		6

new Handle:g_hDatabase;
new Handle:g_hReconnectTimer;
new Handle:g_hThrownKnives; // Store thrown knives
new bool:g_bHeadshot[MAXPLAYERS+1];
new g_iCollisionGroup = -1;
new Handle:g_hRagdollArray = null;

//Cvars
new Handle:g_hCVPlayerExpire;
new Handle:g_hCVKillXP;
new Handle:g_hCVXPminplayers;
new Handle:g_hCVXPwinning;
new Handle:g_hCVXPassist;
new Handle:g_hCVXPdefuse;
new Handle:g_hCVXPplant;
new Handle:g_hCVXPrescue;
new Handle:sv_footsteps;
new Handle:g_hNoClipSpeedCvar;

new EngineVersion:game;

new raceLoaded, XPLoaded, LevelCalculated;
new menuRaceNumber;
new bool:blockSpawn = false;
new bool:isAimMap = false;
new round_end = -1;


//Paladin
new Handle:MissTimers[MAXPLAYERS+1];

//Sounds
new String:levelupsnd[]="*diablomod.net/levelup.mp3";
new String:fire_expsnd[]="*diablomod.net/fire_explode.mp3";
new String:fireballsnd[]="*diablomod.net/firelaunch2.mp3";
new String:paladinsnd[]="*diablomod.net/charge.mp3";
new String:revivesnd[]="*diablomod.net/revivecast.mp3";
new String:medshotsnd[]="*items/medshot4.wav";
new String:arrowsnd[]="*diablomod.net/arrow.mp3";
new String:itembrokensnd[]="*diablomod.net/itembroken.mp3";
new String:repairsnd[]="*diablomod.net/repair.mp3";
new String:dropsnd[]="*diablomod.net/drop.mp3";
new String:identifysnd[]="*diablomod.net/identify.mp3";
new String:recieveisnd[]="*diablomod.net/recievei.mp3";
new String:wingssnd[]="*diablomod.net/wings.mp3";
new String:dagonsnd[]="*diablomod.net/dagon.mp3";
new String:hooksnd[]="*diablomod.net/hook.mp3";

//new issanta;
//new santais;
//new String:flashlightSnd[]="*diablomod.net/flashlight1.mp3";

enum Ragdolls
{
	Ragdollent,
	victim2,
	attacker2,
	String:victimName[MAX_NAME_LENGTH],
	String:attackerName[MAX_NAME_LENGTH],
	bool:scanned,
	Float:gameTime,
	String:weaponused[32],
	RagdollTeam
}

// Constants
enum Slots
{
	Slot_Primary,
	Slot_Secondary,
	Slot_Knife,
	Slot_Grenade,
	Slot_C4,
	Slot_None
};

new race_heal[9] = { 100, //No
110, //Mag
150, //Monk
130, //Paladin
100, //Assassin
110, //Necromancer
110, //Barbarian
170, //Ninja
120 /*Amazon*/};

new LevelXPTable[101] = { 0, 50, 125, 225, 340, 510, 765, 1150, 1500, 1950, 2550, 3300, 4000, 4800, 5800, 7000, 8500, 9500, 10500, 11750, 13000, //21
14300, 15730, 17300, 19030, 20900, 23000, 24000, 25200, 26400, 27700, 29000, 30500, 32000, 33600, 35300, 37000, 39000, //38
41000, 43000, 45100, 47400, 49800, 52300, 55000, 57800, 60700, 63700, 66900, 70200, //50
73700, 77400, 80000, 82400, 84900, 87500, 90000, 92700, 95500, 98300, 101000, 104000, 107000, 110000, 113000, 116000, 120000, //67
123000, 126700, 130000, 134000, 138000, 142000, 146000, 150000, 154000, 158000, 163000, 168000, 173000, //80
178000, 183000, 188000, 194000, 200000, 216000, 232000, 255000, 282000, 309000, 325000, 367000, 401000, 427000, 464000, 482000, 505000, 537000, 559000, 579000, 600000/*101*/};

new MAXLEVELXPDEFINED = sizeof(LevelXPTable);

new g_iDBPlayerUniqueID[MAXPLAYERS+1];
new extra_changed[MAXPLAYERS+1];
new ii_chat[MAXPLAYERS+1];
new ii_award[MAXPLAYERS+1];
new hint_disable[MAXPLAYERS+1];
new player_totallvl[MAXPLAYERS+1];
new player_class_xp[MAXPLAYERS+1][MAXRACES+1];
new player_class_lvl[MAXPLAYERS+1][MAXRACES+1];
new player_xp[MAXPLAYERS+1];
new player_lvl[MAXPLAYERS+1] = 1;
new player_class[MAXPLAYERS+1];
new player_strength[MAXPLAYERS+1];
new player_agility[MAXPLAYERS+1];
new player_intelligence[MAXPLAYERS+1];
new player_dextery[MAXPLAYERS+1];
new player_point[MAXPLAYERS+1];
new loading_xp[MAXPLAYERS+1];
new loading_race[MAXPLAYERS+1];
new first_time_select_race[MAXPLAYERS+1];
new on_knife[MAXPLAYERS+1];
new Float:cast_end[MAXPLAYERS+1];
new bool:casting[MAXPLAYERS+1];
new monk_shield[MAXPLAYERS+1];
new golden_bullet[MAXPLAYERS+1];
new ultra_armor[MAXPLAYERS+1];
new longjumps[MAXPLAYERS+1];
new bow[MAXPLAYERS+1];
new Float:bowdelay[MAXPLAYERS+1];
new player_item_id[MAXPLAYERS+1];
new item_durability[MAXPLAYERS+1];
new String:player_item_name[MAXPLAYERS+1][128];   //The items name

new flashbattery[MAXPLAYERS+1] = MAX_FLASH;
new flashbatteryMax[MAXPLAYERS+1] = MAX_FLASH;
new bool:flashlight_enabled[MAXPLAYERS+1];

new player_colored[MAXPLAYERS+1];
new player_colored_left[MAXPLAYERS+1];
new player_fireball_sprite[2049];
new player_hideradar_ent[MAXPLAYERS+1] = INVALID_ENT_REFERENCE;
new bool:player_induck[MAXPLAYERS+1];
new bool:player_inshift[MAXPLAYERS+1];
new bool:player_inuse[MAXPLAYERS+1];
new player_knife[MAXPLAYERS+1];
new player_knife2[MAXPLAYERS+1];
new player_trapgren[MAXPLAYERS+1];
new g_fLastFlags[MAXPLAYERS+1];
new g_fLastButtons[MAXPLAYERS+1];
new g_iJumps[MAXPLAYERS+1];
new fireball_collected[MAXPLAYERS+1];

//Items
new invisible_cast[MAXPLAYERS+1];
new player_ring[MAXPLAYERS+1];
new item_boosted[MAXPLAYERS+1];
new jumps[MAXPLAYERS+1];
new falling[MAXPLAYERS+1];
new Float:fallingtime[MAXPLAYERS+1];
new earthstomp[MAXPLAYERS+1];
new bool:used_item[MAXPLAYERS+1];
new player_b_vampire[MAXPLAYERS+1];	//Vampyric damage
new player_b_damage[MAXPLAYERS+1];		//Bonus damage
new player_b_gamble[MAXPLAYERS+1];
new player_b_money[MAXPLAYERS+1];		//Money bonus
new player_b_gravity[MAXPLAYERS+1];	//Gravity bonus : 1 = best
new player_b_inv[MAXPLAYERS+1];		//Invisibility bonus
new player_b_grenade[MAXPLAYERS+1];	//Grenade bonus = 1/chance to kill
new player_b_reduceH[MAXPLAYERS+1];	//Reduces player health each round start
new player_b_theif[MAXPLAYERS+1];		//Amount of money to steal
new player_b_respawn[MAXPLAYERS+1];	//Chance to respawn upon death
new player_b_explode[MAXPLAYERS+1];	//Radius to explode upon death
new player_b_heal[MAXPLAYERS+1];		//Ammount of hp to heal each 1 second
new player_b_heal_timer[MAXPLAYERS+1];		//heal each every 5 seconds
new player_b_blind[MAXPLAYERS+1];		//Chance 1/Value to blind the enemy
new player_b_fireshield[MAXPLAYERS+1];	//Protects against explode and grenade bonus 
new bool:player_b_fireshield_activated[MAXPLAYERS+1];	//Protects against explode and grenade bonus 
new player_slowed[MAXPLAYERS+1] = -1;	//slow timer
new player_b_meekstone[MAXPLAYERS+1];	//Ability to lay a fake c4 and detonate 
new player_b_teamheal[MAXPLAYERS+1];	//How many hp to heal when shooting a teammate 
new player_b_redirect[MAXPLAYERS+1];	//How much damage will the player redirect 
new player_b_fireball[MAXPLAYERS+1];	//Ability to shot off a fireball value = radius *
new player_b_ghost[MAXPLAYERS+1];		//Ability to walk through walls
new player_b_eye[MAXPLAYERS+1];	         //Ability to snarkattack
new player_b_blink[MAXPLAYERS+1];	//Abiliy to use railgun
new player_b_windwalk[MAXPLAYERS+1];	//Ability to windwalk
new player_b_usingwind[MAXPLAYERS+1];	//Is player using windwalk
new player_b_froglegs[MAXPLAYERS+1];
new player_b_silent[MAXPLAYERS+1];
new player_b_dagon[MAXPLAYERS+1];		//Abliity to nuke opponents
new player_b_sniper[MAXPLAYERS+1];		//Ability to kill faster with scout
new player_b_jumpx[MAXPLAYERS+1];
new player_b_smokehit[MAXPLAYERS+1];
new player_b_extrastats[MAXPLAYERS+1];
new player_b_firetotem[MAXPLAYERS+1];
new player_b_hook[MAXPLAYERS+1];
new player_b_darksteel[MAXPLAYERS+1];
new player_b_illusionist[MAXPLAYERS+1];
new player_b_mine[MAXPLAYERS+1];
new player_b_mine_count[MAXPLAYERS+1];
new player_b_chameleon[MAXPLAYERS+1];
new wear_sun[MAXPLAYERS+1];
new player_sword[MAXPLAYERS+1]; 
new player_ultra_armor_left[MAXPLAYERS+1];
new player_ultra_armor[MAXPLAYERS+1];
new moneyshield[MAXPLAYERS+1];
new Float:gravitytimer[MAXPLAYERS+1];
new Float:TotemTimer[2048];
new c4state[MAXPLAYERS+1];
new c4fake[MAXPLAYERS+1];
new player_b_teamheal_using[MAXPLAYERS+1] = -1;
new player_b_teamheal_shielded[MAXPLAYERS+1] = -1;
new Float:player_b_fireball_timer[MAXPLAYERS+1];
new Float:player_race_fireball_timer[MAXPLAYERS+1];
new ghoststate[MAXPLAYERS+1];
new Float:player_b_ghost_end[MAXPLAYERS+1];
new player_b_eye_created[MAXPLAYERS+1];
new player_b_eye_viewing[MAXPLAYERS+1];
new Float:player_b_blink_timer[MAXPLAYERS+1];
new bool:inteleportcheck[MAXPLAYERS+1];
new Float:teleportpos[MAXPLAYERS+1][3];
new Float:emptypos[3];
new Float:oldpos[MAXPLAYERS+1][3];
new ClientTracer;
new absincarray[]={0,4,-4,8,-8,12,-12,18,-18,22,-22,25,-25};//,27,-27,30,-30,33,-33,40,-40}; //for human it needs to be smaller
//new g_unClientSprite[MAXPLAYERS+1];
new player_b_firetotem_left[MAXPLAYERS+1];
new player_b_firetotem_owner[MAXPLAYERS+1];
new Float:player_b_froglegs_timer[MAXPLAYERS+1];
new player_b_illusionist_activated[MAXPLAYERS+1];
new Float:player_b_illusionist_end[MAXPLAYERS+1];
new bool:player_frommenu[MAXPLAYERS+1];
new worldM_owner[10000];
new player_firstconnect[MAXPLAYERS+1];
new player_newrace[MAXPLAYERS+1];
new player_spend_1_time[MAXPLAYERS+1];
new player_spendmoney[MAXPLAYERS+1];
new player_lastmoney[MAXPLAYERS+1];
new player_lasthint[MAXPLAYERS+1];

//Offsets
new m_OffsetClrRender=-1;
new m_OffsetSpeed=-1;
//new m_OffsetActiveWeapon=-1;
//Paladin LJ
new m_vecVelocity_0, m_vecVelocity_1, m_vecBaseVelocity; //offsets

new UserMsg:g_umsgFade = INVALID_MESSAGE_ID;
new UserMsg:g_umsgShake = INVALID_MESSAGE_ID;
new bool:g_bCanEnumerateMsgType = false;
new bool:g_InReload[MAXPLAYERS+1];
new bool:g_InAttack[MAXPLAYERS+1];
new bool:g_bHoldingProp[MAXPLAYERS+1];
new g_bHoldingCount[MAXPLAYERS+1];
new g_haskit[MAXPLAYERS+1];
new Float:g_bHoldingTimeFrame[MAXPLAYERS+1];

//Barbarian cache clips
//new const g_AmmoBoxQty[] = {0, 35, 90, 90, 200, 30, 120, 32, 100, 52, 100};
// Player Settings
new g_PlayerPrimaryAmmo[MAXPLAYERS+1] = {0, ...};
new g_PlayerSecondaryAmmo[MAXPLAYERS+1] = {0, ...};

//Barbarian Weapon Entity Members and Data
//new g_iAmmo = -1;
new g_hActiveWeapon = -1;
//new g_iPrimaryAmmoType = -1;
new g_iClip1 = -1;

new WhiteSprite;
new HaloSprite;
new BlueGlowSprite;

new String:diablo_bonus[128];

public Plugin:myinfo = 
{
	name = "Diablo mod",
	author = "hitmany",
	description = "Diablo mod",
	version = PLUGIN_VERSION,
	url = "http://diablomod.net"
}

public OnPluginStart()
{
	AutoExecConfig_SetFile("plugin.diablo");
	AutoExecConfig_SetCreateFile(true);
	AutoExecConfig_SetPlugin(INVALID_HANDLE);
	
	InitDatabase();
	
	LoadTranslations("diablo.common.phrases");
	
	HookEventEx("player_spawn", Event_OnPlayerSpawn);
	HookEvent("item_pickup", Event_ItemPickup);
	HookEventEx("player_death", Event_OnPlayerDeath, EventHookMode_Pre);
	HookEventEx("round_end", Event_OnRoundEnd);
	HookEventEx("bomb_defused",Event_BombDefusedEvent);
	HookEventEx("bomb_planted",Event_BombPlantedEvent);
	HookEventEx("hostage_rescued",Event_HostageRescuedEvent);
	HookEvent("round_start",RoundStartEvent);
	//HookEventEx("round_freeze_end",FreezeEndEvent);
	//HookEvent("player_say", Event_OnPlayerSay);
	RegConsoleCmd("say_team", Event_OnPlayerSay);
	RegConsoleCmd("say", Event_OnPlayerSay);
	HookEvent("player_jump",PlayerJumpEvent);
	HookEvent("player_changename", Event_ChangeName);
	HookEvent("player_blind", Event_OnFlashPlayer, EventHookMode_Pre);
	HookEvent("weapon_fire", weapon_fire);
	AddNormalSoundHook(OnNormalSoundPlayed);
	sv_footsteps = FindConVar("sv_footsteps");
	g_hNoClipSpeedCvar = FindConVar("sv_noclipspeed");
	SetConVarFloat(g_hNoClipSpeedCvar, 2.0);
	
	g_umsgFade = GetUserMessageId("Fade");
	if (g_umsgFade == INVALID_MESSAGE_ID)
	{
		LogError("This game doesn't support Fade!");
	}

	g_umsgShake = GetUserMessageId("Shake");
	if (g_umsgShake == INVALID_MESSAGE_ID)
	{
		LogError("This game doesn't support Shake!");
	}

	if(GetFeatureStatus(FeatureType_Native, "GetUserMessageType") == FeatureStatus_Available)
	{
		g_bCanEnumerateMsgType = true;
	}
	
	g_hThrownKnives = CreateArray();
	
	//HookUserMessage(GetUserMessageId("ProcessSpottedEntityUpdate"), Hook_ProcessSpottedEntityUpdate, true);
	
	CreateTimer(1.0,TimerEverySecond,_,TIMER_REPEAT);
	CreateTimer(0.5,TimerEveryHalfSecond,_,TIMER_REPEAT);
	CreateTimer(0.2,TimerEveryQuarSecond,_,TIMER_REPEAT);
	
	
	//Offsets
	m_OffsetClrRender=FindSendPropOffs("CBaseAnimating","m_clrRender");
	m_vecVelocity_0 = FindSendPropInfo("CBasePlayer","m_vecVelocity[0]");
    m_vecVelocity_1 = FindSendPropInfo("CBasePlayer","m_vecVelocity[1]");
    m_vecBaseVelocity = FindSendPropInfo("CBasePlayer","m_vecBaseVelocity");
	g_iCollisionGroup = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	m_OffsetSpeed=FindSendPropInfo("CBasePlayer","m_flLaggedMovementValue");
	
	g_hRagdollArray = CreateArray(102);
	//_iAmmo = FindSendPropOffs("CCSPlayer", "m_iAmmo");
	g_hActiveWeapon = FindSendPropOffs("CCSPlayer", "m_hActiveWeapon");
	//g_iPrimaryAmmoType = FindSendPropOffs("CBaseCombatWeapon", "m_iPrimaryAmmoType");
	g_iClip1 = FindSendPropOffs("CBaseCombatWeapon", "m_iClip1");
	
	AddCommandListener(Command_Flashlight, "+lookatweapon");
	
	game = GetEngineVersion();
	
	g_hCVPlayerExpire = AutoExecConfig_CreateConVar("diablo_player_expire", "150", "Sets how many days until an unused player account is deleted (0 = never)", 0, true, 0.0);
	g_hCVKillXP = AutoExecConfig_CreateConVar("diablo_killxp", "25", "XP for kill", 0, true, 0.0);
	g_hCVXPminplayers = AutoExecConfig_CreateConVar("diablo_xp_minplr", "2", "Minumum player for recive XP", 0, true, 0.0);
	g_hCVXPwinning = AutoExecConfig_CreateConVar("diablo_xp_win", "2", "XP for team winner. XP per current online players", 0, true, 0.0);
	g_hCVXPassist = AutoExecConfig_CreateConVar("diablo_xp_assist", "10", "XP for assist kill", 0, true, 0.0);
	g_hCVXPdefuse = AutoExecConfig_CreateConVar("diablo_xp_defuse", "2", "XP for defuse. XP per current online players", 0, true, 0.0);
	g_hCVXPplant = AutoExecConfig_CreateConVar("diablo_xp_plant", "2", "XP for plant. XP per current online players", 0, true, 0.0);
	g_hCVXPrescue = AutoExecConfig_CreateConVar("diablo_xp_rescue", "2", "XP for recue. XP per current online players", 0, true, 0.0);
	
	RegConsoleCmd("diablo_setxp",ADM_SetXP,"Set a player's XP");
	RegConsoleCmd("diablo_setrace",ADM_SetRace,"Set a player's race");
	RegConsoleCmd("diablo_setitem",ADM_SetItem,"Set a player's item");
	RegConsoleCmd("menu",MenuCMD);
	RegConsoleCmd("ii",MenuItemInfo);
	RegConsoleCmd("iteminfo",MenuItemInfo);
	RegConsoleCmd("dropitem",DropItem);
	RegConsoleCmd("class",MenuCMDclass);
	RegConsoleCmd("changerace",MenuCMDclass);
	RegConsoleCmd("showxp",MenuCMDXP);
	RegConsoleCmd("xp",MenuCMDXP);
	RegConsoleCmd("reset",MenuCMDreset);
	RegConsoleCmd("skills",MenuCMDskills);
	RegConsoleCmd("showskills",MenuCMDskills);
	RegConsoleCmd("shop",MenuCMDshop);
	RegConsoleCmd("who",MenuCMDwho);
	RegConsoleCmd("buyitem",BuyItemCMD);
	RegConsoleCmd("zal",ZalCMD);
	RegConsoleCmd("upgrade",UpgradeCMD);
	
	for(new i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i) && IsClientAuthorized(i))
		{
			OnClientConnected(i);
			OnClientPutInServer(i);
		}
	}
	
	BuildPath(Path_SM, diablo_bonus, sizeof(diablo_bonus), "logs/diablo_bonus.log");
}

public OnPluginEnd()
{	
	// Try to save the stats!
	if(g_hDatabase != INVALID_HANDLE)
		SaveAllPlayers();
}

//Remove shadows
public OnMapStart()
{
	decl String:mapname[128];
	GetCurrentMap(mapname, sizeof(mapname));
	if (StrContains( mapname, "fun_", false) == 0 || StrContains( mapname, "$2000$", false) == 0 || StrContains( mapname, "aim_", false) == 0 || StrContains( mapname, "aim_", false) == 0 || StrContains( mapname, "awp_", false) == 0 || StrContains( mapname, "fy_", false) == 0 || StrContains( mapname, "35hp_", false) == 0 || StrContains( mapname, "sk_aim_go", false) == 0 || StrContains( mapname, "scout_", false) == 0)
	{
		isAimMap = true;
	}
	else
	{
		isAimMap = false;
	}
}

public OnAutoConfigsBuffered()
{
	AddFileToDownloadsTable("sound/diablomod.net/levelup.mp3");
	FakePrecacheSound(levelupsnd);
	FakePrecacheSound("items/flashlight1.wav");
	FakePrecacheSound("weapons/knife/knife_deploy.wav");
	FakePrecacheSound("weapons/hegrenade/explode5.wav");
	AddFileToDownloadsTable("sound/diablomod.net/flashlight1.mp3");
	FakePrecacheSound(fire_expsnd);
	AddFileToDownloadsTable("sound/diablomod.net/fire_explode.mp3");
	FakePrecacheSound(fireballsnd);
	AddFileToDownloadsTable("sound/diablomod.net/firelaunch2.mp3");
	FakePrecacheSound(paladinsnd);
	FakePrecacheSound(revivesnd);
	AddFileToDownloadsTable("sound/diablomod.net/revivecast.mp3");
	FakePrecacheSound(medshotsnd);
	AddFileToDownloadsTable("sound/diablomod.net/charge.mp3");
	FakePrecacheSound(arrowsnd);
	AddFileToDownloadsTable("sound/diablomod.net/arrow.mp3");
	AddFileToDownloadsTable("sound/diablomod.net/itembroken.mp3");
	FakePrecacheSound(itembrokensnd);
	AddFileToDownloadsTable("sound/diablomod.net/repair.mp3");
	FakePrecacheSound(repairsnd);
	AddFileToDownloadsTable("sound/diablomod.net/drop.mp3");
	FakePrecacheSound(dropsnd);
	AddFileToDownloadsTable("sound/diablomod.net/identify.mp3");
	FakePrecacheSound(identifysnd);
	AddFileToDownloadsTable("sound/diablomod.net/recievei.mp3");
	FakePrecacheSound(recieveisnd);
	AddFileToDownloadsTable("sound/diablomod.net/wings.mp3");
	FakePrecacheSound(wingssnd);
	AddFileToDownloadsTable("sound/diablomod.net/dagon.mp3");
	FakePrecacheSound(dagonsnd);
	AddFileToDownloadsTable("sound/diablomod.net/hook.mp3");
	FakePrecacheSound(hooksnd);
	
	AddFileToDownloadsTable("models/diablomod.net/monk_shield.dx90.vtx");
	AddFileToDownloadsTable("models/diablomod.net/monk_shield.mdl");
	AddFileToDownloadsTable("models/diablomod.net/monk_shield.vvd");
	AddFileToDownloadsTable("models/diablomod.net/monk_shield.phy");
	AddFileToDownloadsTable("materials/models/diablomod.net/monk_shield.vtf");
	AddFileToDownloadsTable("materials/models/diablomod.net/monk_shield.vmt");
	AddFileToDownloadsTable("materials/sprites/diablo/fireball.vmt");
	AddFileToDownloadsTable("materials/sprites/diablo/fireball.vtf");
	AddFileToDownloadsTable("materials/sprites/diablo/crossbolt.vmt");
	AddFileToDownloadsTable("materials/sprites/diablo/crossbolt.vtf");
	//AddFileToDownloadsTable("materials/sprites/diablo/firetotem.vmt");
	//AddFileToDownloadsTable("materials/sprites/diablo/firetotem.vtf");
	PrecacheModel("materials/sprites/diablo/fireball.vmt");
	PrecacheModel("models/error.mdl");
	PrecacheModel("materials/sprites/diablo/crossbolt.vmt");
	//PrecacheModel("materials/sprites/diablo/firetotem.vmt");
	PrecacheModel("models/diablomod.net/monk_shield.mdl");
	PrecacheModel("models/player/tm_phoenix_varianta.mdl");
	PrecacheModel("models/player/ctm_sas_varianta.mdl");
	PrecacheModel("models/weapons/w_knife_default_t_dropped.mdl");
	PrecacheModel("models/weapons/w_knife_default_ct_dropped.mdl");
	
	AddFileToDownloadsTable("materials/models/diablomod.net/base_totem.vmt");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_base.vtf");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_green.vmt");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_green.vtf");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_lava.vtf");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_lava.vmt");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_stone.vmt");
	AddFileToDownloadsTable("materials/models/diablomod.net/totem_stone.vtf");
	AddFileToDownloadsTable("models/diablomod.net/totem_fire.dx90.vtx");
	AddFileToDownloadsTable("models/diablomod.net/totem_fire.mdl");
	AddFileToDownloadsTable("models/diablomod.net/totem_fire.phy");
	AddFileToDownloadsTable("models/diablomod.net/totem_fire.vvd");
	AddFileToDownloadsTable("models/diablomod.net/totem_fire.dx90.vtx");
	AddFileToDownloadsTable("models/diablomod.net/totem_heal.dx90.vtx");
	AddFileToDownloadsTable("models/diablomod.net/totem_heal.mdl");
	AddFileToDownloadsTable("models/diablomod.net/totem_heal.phy");
	AddFileToDownloadsTable("models/diablomod.net/totem_heal.vvd");
	PrecacheModel("models/diablomod.net/totem_fire.mdl");
	PrecacheModel("models/diablomod.net/totem_heal.mdl");
	
	AddFileToDownloadsTable("models/diablomod.net/mine.dx90.vtx");
	AddFileToDownloadsTable("models/diablomod.net/mine.mdl");
	AddFileToDownloadsTable("models/diablomod.net/mine.phy");
	AddFileToDownloadsTable("models/diablomod.net/mine.vvd");
	AddFileToDownloadsTable("materials/models/diablomod.net/mine.vtf");
	AddFileToDownloadsTable("materials/models/diablomod.net/mine.vmt");
	PrecacheModel("models/diablomod.net/mine.mdl");
	
	g_crossModel = PrecacheModel(CROSSBOW_MODEL); // Custom model 
	g_wcrossModel = PrecacheModel(CROSSBOW_WORLDMODEL); // Custom model 
	
	WhiteSprite = PrecacheModel("materials/sprites/white.vmt");
	HaloSprite = PrecacheModel("materials/sprites/halo.vmt");
	BlueGlowSprite = PrecacheModel("materials/sprites/blueglow1.vmt");
	
	PrecacheModel("models/weapons/w_c4_planted.mdl"); //meekstone
	
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/crossbow_d.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/crossbow_d.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/crossbow_n.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/marine_d.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/marine_d.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/marine_n.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_clip.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_clip_nm.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_crossbow_bolt.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_crossbow_bolt.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/xbow_scope.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/xbow_scope_gradient.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/xbow_scope_gradient.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/xbow_scope_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_crossbow_mp.vmt");
	AddFileToDownloadsTable("materials/models/weapons/v_crossbow/w_xbow.vtf");
	AddFileToDownloadsTable("models/weapons/v_crossbow.dx90.vtx");
	AddFileToDownloadsTable("models/weapons/v_crossbow.vvd");
	AddFileToDownloadsTable("models/weapons/v_crossbow.mdl");
	AddFileToDownloadsTable("models/weapons/w_crossbow_mp.dx90.vtx");
	AddFileToDownloadsTable("models/weapons/w_crossbow_mp.vvd");
	AddFileToDownloadsTable("models/weapons/w_crossbow_mp.mdl");
	
	//Hide radar
	AddFileToDownloadsTable("materials/rxg/smokevol.vmt");
	AddFileToDownloadsTable("materials/rxg/smokevol.vtf");
	AddFileToDownloadsTable("models/rxg/smokevol.dx90.vtx");
	AddFileToDownloadsTable("models/rxg/smokevol.mdl");
	AddFileToDownloadsTable("models/rxg/smokevol.phy");
	AddFileToDownloadsTable("models/rxg/smokevol.vvd");
	PrecacheModel("models/rxg/smokevol.mdl");
	
	D2_PrecacheParticle("molotov_explosion");
	D2_PrecacheParticle("explosion_hegrenade_interior_fallback");
	D2_PrecacheParticle("extinguish_embers_small_01");
	
	SetConVarFloat(g_hNoClipSpeedCvar, 2.0);
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnMapEnd()
{
	SaveAllPlayers();
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("DiabloGiveXP", Native_GiveXP);
	RegPluginLibrary("diablo");
	return APLRes_Success;
}

public Native_GiveXP(Handle:plugin, numParams)
{
	new client=GetNativeCell(1);
	new xp=GetNativeCell(2);
	Give_Xp(client,xp,"free bonus");
}

/*public OnAllPluginsLoaded()
{
    bIsVSHRunning = LibraryExists("santa");
}

public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "santa"))
    {
        bIsVSHRunning = true;
    }
}

public OnLibraryRemoved(const String:name[])
{
    if (StrEqual(name, "santa"))
    {
        bIsVSHRunning = false;
    }
}*/


InitDatabase()
{
	ClearHandle(g_hReconnectTimer);
	
	if(SQL_CheckConfig(DIABLO_DB))
		SQL_TConnect(SQL_OnConnect, DIABLO_DB);
	else
		SQL_TConnect(SQL_OnConnect, "default"); // Default to 'default' section in the databases.cfg.
}

public SQL_OnConnect(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(hndl == INVALID_HANDLE)
	{
		LogError("Error connecting to database (reconnecting in %.0f seconds): %s", RECONNECT_INTERVAL, error);
		ClearHandle(g_hReconnectTimer);
		g_hReconnectTimer = CreateTimer(RECONNECT_INTERVAL, Timer_ReconnectDatabase);
		return;
	}
	
	// We're good now. Don't reconnect again. Just to be sure.
	ClearHandle(g_hReconnectTimer);
	
	g_hDatabase = hndl;
	
	SQL_SetCharset(g_hDatabase, "utf8");
	
	decl String:sQuery[1024];
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `class` ( \
		`id` int(8) unsigned NOT NULL, \
		`class` int(2) unsigned NOT NULL, \
		`xp` int(8) NOT NULL DEFAULT '0', \
		PRIMARY KEY (`id`,`class`) \
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
	if(!SQL_LockedFastQuery(g_hDatabase, sQuery))
	{
		decl String:sError[256];
		SQL_GetError(g_hDatabase, sError, sizeof(sError));
		SetFailState("Error creating class table: %s", sError);
		return;
	}
	
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `extra` ( \
		`id` int(8) unsigned NOT NULL, \
		`total_lvl` int(8) NOT NULL DEFAULT '0', \
		`ii_chat` int(11) NOT NULL, \
		`ii_award` int(11) NOT NULL, \
		`hint_disable` int(2) NOT NULL, \
		PRIMARY KEY (`id`) \
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
	if(!SQL_LockedFastQuery(g_hDatabase, sQuery))
	{
		decl String:sError[256];
		SQL_GetError(g_hDatabase, sError, sizeof(sError));
		SetFailState("Error creating extra table: %s", sError);
		return;
	}
	
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `player` ( \
		`id` int(8) unsigned NOT NULL AUTO_INCREMENT, \
		`steamid` varchar(64) NOT NULL, \
		`name` varchar(33) NOT NULL, \
		`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ON UPDATE CURRENT_TIMESTAMP', \
		PRIMARY KEY (`id`), \
		UNIQUE KEY `id` (`id`), \
		KEY `id_2` (`id`) \
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
	if(!SQL_LockedFastQuery(g_hDatabase, sQuery))
	{
		decl String:sError[256];
		SQL_GetError(g_hDatabase, sError, sizeof(sError));
		SetFailState("Error creating player table: %s", sError);
		return;
	}
	
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `skill` ( \
		`id` int(8) unsigned NOT NULL, \
		`class` int(2) unsigned NOT NULL, \
		`str` int(2) NOT NULL, \
		`agi` int(2) NOT NULL, \
		`int` int(2) NOT NULL, \
		`dex` int(2) NOT NULL, \
		`quest_cur` int(2) NOT NULL DEFAULT '0', \
		`quest_count1` int(2) NOT NULL DEFAULT '0', \
		`quest_count2` int(2) NOT NULL DEFAULT '0', \
		PRIMARY KEY (`id`,`class`) \
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
	if(!SQL_LockedFastQuery(g_hDatabase, sQuery))
	{
		decl String:sError[256];
		SQL_GetError(g_hDatabase, sError, sizeof(sError));
		SetFailState("Error creating skill table: %s", sError);
		return;
	}
	
	// Cleanup our database.
	DatabaseClear();
	
	// Add all already connected players now
	for(new i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i) && IsClientAuthorized(i))
		{
			AddPlayer(i);
		}
	}
}

DatabaseClear()
{
	if(!g_hDatabase)
		return;
	
	new String:sQuery[256];
	// Have players expire after x days and delete them from the database?
	if(GetConVarInt(g_hCVPlayerExpire) > 0)
	{
		Format(sQuery, sizeof(sQuery), "time <= %d", GetTime()-(86400*GetConVarInt(g_hCVPlayerExpire)));
	}
	
	// Delete players who are Level 1 and haven't played for 3 days
	Format(sQuery, sizeof(sQuery), "SELECT id FROM player WHERE %s", sQuery);
	SQL_TQuery(g_hDatabase, SQL_DeleteExpiredPlayers, sQuery);
}

public SQL_DeleteExpiredPlayers(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("DatabaseClear: player expire query failed: %s", error);
	}
	
	// Delete them at once.
	new Transaction:hTransaction = SQL_CreateTransaction();
	
	new iPlayerId, String:sQuery[128];
	while(SQL_MoreRows(hndl))
	{
		if(!SQL_FetchRow(hndl))
			continue;
		
		iPlayerId = SQL_FetchInt(hndl, 0);
		
		// Don't delete players who are connected right now.
		if (GetClientByPlayerID(iPlayerId) != -1)
			continue;
		
		Format(sQuery, sizeof(sQuery), "DELETE FROM class WHERE id = %d", iPlayerId);
		SQL_AddQuery(hTransaction, sQuery);
		
		Format(sQuery, sizeof(sQuery), "DELETE FROM extra WHERE id = %d", iPlayerId);
		SQL_AddQuery(hTransaction, sQuery);
		
		Format(sQuery, sizeof(sQuery), "DELETE FROM skill WHERE id = %d", iPlayerId);
		SQL_AddQuery(hTransaction, sQuery);
		
		Format(sQuery, sizeof(sQuery), "DELETE FROM player WHERE id = %d", iPlayerId);
		SQL_AddQuery(hTransaction, sQuery);
	}
	
	SQL_ExecuteTransaction(g_hDatabase, hTransaction, _, SQLTxn_LogFailure);
}

TryToOpenChangerace(client)
{
	if(loading_xp[client]==0 && !IsFakeClient(client))
	{
		if(player_xp[client]>0)
		{
			SaveData(client);
		}
		loading_xp[client]=1;
		GetAllXP(client);
	}

}

GetAllXP(client)
{
	if(g_iDBPlayerUniqueID[client] == 0)
	{
		AddPlayer(client);
		return;
	}
	
	decl String:sQuery[255];
	Format(sQuery, sizeof(sQuery), "SELECT `class`, `xp` FROM `class` WHERE ( `id` = '%d' );", g_iDBPlayerUniqueID[client] );
	SQL_TQuery(g_hDatabase, SQL_LoadAllDataPlayer, sQuery, GetClientUserId(client));
	
	Format(sQuery, sizeof(sQuery), "SELECT `ii_chat`, `ii_award`, `hint_disable` FROM `extra` WHERE ( `id` = '%d' );", g_iDBPlayerUniqueID[client]);
	SQL_TQuery(g_hDatabase, SQL_GetPlayerExtra, sQuery, GetClientUserId(client));

}

public SQL_GetPlayerExtra(Handle:owner, Handle:hndl, const String:error[],  any:userid)
{
	new client = GetClientOfUserId(userid);
	if(!client)
		return;
	
	
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("Unable to load extra (%s)", error);
		//AddPlayer(client);
		PrintToConsole(client, "%T", "[Diablomod] Unable to load your extra",client);
		return;
	}
	if(SQL_FetchRow(hndl))
	{
		ii_chat[client] = DSQLPlayerInt(hndl, "ii_chat");
		ii_award[client] = DSQLPlayerInt(hndl, "ii_award");
		hint_disable[client] = DSQLPlayerInt(hndl, "hint_disable");
	}
	else
	{
		ii_chat[client] = 1;
		ii_award[client] = 1;
		hint_disable[client] = 0;
	}
}

AddPlayer(client)
{
	if(!g_hDatabase)
		return;
	
	loading_xp[client]=1;
	first_time_select_race[client]=1;
	

	decl String:sQuery[256];
	if(!IsFakeClient(client))
	{
		new String:authid[64];
		GetClientAuthId(client, AuthId_Steam2, authid, sizeof(authid));
		
		Format(sQuery, sizeof(sQuery), "SELECT `id` FROM `player` WHERE `steamid` = '%s';", authid);
	}
	
	SQL_TQuery(g_hDatabase, SQL_GetPlayerInfo, sQuery, GetClientUserId(client));
}

public SQL_GetPlayerInfo(Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId(userid);
	if(!client)
		return;
	
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		// TODO: Retry later?
		LogError("Unable to load player data (%s)", error);
		//AddPlayer(client);
		PrintToConsole(client, "%T", "[Diablomod] Unable to load your ID from DB",client);
		return;
	}
	
	// First time this player connects?
	if(SQL_GetRowCount(hndl) == 0 || !SQL_FetchRow(hndl))
	{
		InsertPlayer(client);
		return;
	}
	
	g_iDBPlayerUniqueID[client] = SQL_FetchInt(hndl, 0);
	
	decl String:sQuery[255];
	Format(sQuery, sizeof(sQuery), "SELECT `class`, `xp` FROM `class` WHERE ( `id` = '%d' );", g_iDBPlayerUniqueID[client] );
	SQL_TQuery(g_hDatabase, SQL_LoadAllDataPlayer, sQuery, GetClientUserId(client));
}

InsertPlayer(client)
{
	
	if(IsFakeClient(client))
		return;
	
	decl String:sQuery[512];
	decl String:sName[MAX_NAME_LENGTH], String:sNameEscaped[MAX_NAME_LENGTH*2+1];
	decl String:steamid[64];
	GetClientName(client, sName, sizeof(sName));
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	SQL_EscapeString(g_hDatabase, sName, sNameEscaped, sizeof(sNameEscaped));
	
	// Store the steamid of the player
	Format(sQuery, sizeof(sQuery), "INSERT INTO `player` ( `id` , `steamid` , `name` , `time` ) VALUES ( NULL , '%s', '%s', NOW() );", steamid, sNameEscaped );
	SQL_TQuery(g_hDatabase, SQL_InsertPlayer, sQuery, GetClientUserId(client));
	TryToOpenChangerace(client);
}

public SQL_InsertPlayer(Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId(userid);
	if(!client)
		return;
	
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("Unable to insert player info (%s)", error);
		//AddPlayer(client);
		PrintToConsole(client, "%T", "[Diablomod] Unable to insert your ID in DB",client);
		return;
	}
	
	g_iDBPlayerUniqueID[client] = SQL_GetInsertId(owner);
	AddPlayer(client);
}

public SQL_LoadAllDataPlayer(Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId(userid);
	if(!client)
		return;
	
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("Unable to read xp, class (%s)", error);
		//AddPlayer(client);
		PrintToConsole(client, "%T", "[Diablomod] Unable to load your xp",client);
		return;
	}
	
	for(new iRace=1;iRace<MAXRACES+1;iRace++)
	{
		player_class_xp[client][iRace] = 0;
		player_class_lvl[client][iRace] = 1;
	}
	player_totallvl[client] = 8;
	while(SQL_MoreRows(hndl))
	{
		if(SQL_FetchRow(hndl))
		{
			raceLoaded = DSQLPlayerInt(hndl, "class");
			XPLoaded = DSQLPlayerInt(hndl, "xp");
			player_class_xp[client][raceLoaded] = XPLoaded;
			LevelCalculated = 1;
			for (new i = 1; i <= sizeof(LevelXPTable)-1; i++ )
			{
				// User has enough XP to advance to the next level
				if ( XPLoaded >= LevelXPTable[i])
				{
					LevelCalculated = i+1;
					player_totallvl[client]++;
				}
				else
				{
					break;
				}
			}
			player_class_lvl[client][raceLoaded] = LevelCalculated;
		}
	}

	ShowChangeraceMenu(client);
}

SaveData(client, Transaction:hTransaction=Transaction:INVALID_HANDLE)
{
	if(g_hDatabase == INVALID_HANDLE)
		return;
	
	if(IsFakeClient(client))
		return;
	
	if(g_iDBPlayerUniqueID[client] < 0)
	{
		InsertPlayer(client);
		return;
	}
	
	if(loading_xp[client] == 1 || loading_race[client] == 1)
		return;
	
	// Save the user's XP!
	if ( player_xp[client] > 0 && player_class[client] != 0 )
	{
	
		decl String:sQuery[8192];
		Format(sQuery, sizeof(sQuery), "REPLACE INTO `class` ( `id` , `class` , `xp` ) VALUES ( '%d', '%d', '%d');", g_iDBPlayerUniqueID[client], player_class[client], player_xp[client] );
		// Add the query to the transaction instead of running it right away.
		if(hTransaction != INVALID_HANDLE)
			SQL_AddQuery(hTransaction, sQuery);
		else
			SQL_TQuery(g_hDatabase, SQL_DoNothing, sQuery);
	}
	
	if ( (player_strength[client] > 0) || (player_agility[client] > 0) || (player_intelligence[client] > 0) || (player_dextery[client] > 0) )
	{
	
		decl String:sQuery[8192];
		Format(sQuery, sizeof(sQuery), "REPLACE INTO `skill` (`id`, `class`, `str`, `agi`, `int`, `dex`) VALUES ('%d', '%d', '%d', '%d', '%d', '%d');", g_iDBPlayerUniqueID[client], player_class[client], player_strength[client], player_agility[client], player_intelligence[client], player_dextery[client] );
		// Add the query to the transaction instead of running it right away.
		if(hTransaction != INVALID_HANDLE)
			SQL_AddQuery(hTransaction, sQuery);
		else
			SQL_TQuery(g_hDatabase, SQL_DoNothing, sQuery, GetClientUserId(client));
	}
	
	decl String:sQuery[8192];
	Format(sQuery, sizeof(sQuery), "REPLACE INTO `extra` (`id`, `total_lvl`, `ii_chat`, `ii_award`, `hint_disable`) VALUES ('%d', '%d', '%d', '%d', '%d');", g_iDBPlayerUniqueID[client], player_totallvl[client], ii_chat[client], ii_award[client], hint_disable[client] );
	// Add the query to the transaction instead of running it right away.
	if(hTransaction != INVALID_HANDLE)
	{
		SQL_AddQuery(hTransaction, sQuery);
		extra_changed[client] = 0;
	}
	else
	{
		SQL_TQuery(g_hDatabase, SQL_DoNothing, sQuery, GetClientUserId(client));
	}

}

public SQL_DoNothing(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	new client = GetClientOfUserId(data);
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("Error executing query: %s", error);
		PrintToConsole(client, "%T", "[Diablomod] Unable to SAVE your xp",client);
	}
}

public Action:Timer_ReconnectDatabase(Handle:timer)
{
	// Try to connect again after it first failed on plugin load.
	g_hReconnectTimer = INVALID_HANDLE;
	InitDatabase();
	return Plugin_Stop;
}

SaveAllPlayers()
{
	if(g_hDatabase == INVALID_HANDLE)
		return;
	
	// Save all players at once instead of firing seperate queries for every player.
	// This is to optimize sqlite usage.
	new Transaction:hTransaction = SQL_CreateTransaction();
	
	for(new i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i) && IsClientAuthorized(i))
			SaveData(i, hTransaction);
	}
	
	SQL_ExecuteTransaction(g_hDatabase, hTransaction, _, SQLTxn_LogFailure);
}

public LoadRaceData(client,race)
{
	if(!g_hDatabase)
		return;
	
	loading_race[client] = 1;

	decl String:sQuery[256];	
	new any:data = CreateDataPack();
	WritePackCell(data, client);
	WritePackCell(data, race);
	Format(sQuery, sizeof(sQuery), "SELECT `str`, `agi`, `int`, `dex` FROM `skill` WHERE `id` = '%d' AND `class` = '%d';", g_iDBPlayerUniqueID[client],race);
	SQL_TQuery(g_hDatabase, SQL_GetPlayerRace, sQuery, data);
}

public SQL_GetPlayerRace(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	ResetPack(data);
	
	new client, newrace;
	
	client = ReadPackCell(data);
	newrace = ReadPackCell(data);
	
	CloseHandle(data);
	
	if(!client)
		return;
	
	if(hndl == INVALID_HANDLE || strlen(error) > 0)
	{
		LogError("Unable to load race (%s)", error);
		//AddPlayer(client);
		PrintToConsole(client, "%T", "[Diablomod] Unable to load your race",client);
		return;
	}
	if(SQL_FetchRow(hndl))
	{
		player_strength[client] = DSQLPlayerInt(hndl, "str");
		player_agility[client] = DSQLPlayerInt(hndl, "agi");
		player_intelligence[client] = DSQLPlayerInt(hndl, "int");
		player_dextery[client] = DSQLPlayerInt(hndl, "dex");
	}
	
	player_class[client] = newrace;
	player_lvl[client] = player_class_lvl[client][newrace];
	player_xp[client] = player_class_xp[client][newrace];
	
	player_point[client]=(player_lvl[client]-1)*2-player_intelligence[client]-player_strength[client]-player_dextery[client]-player_agility[client];
	
	if(player_point[client]!=0) 
	{
		if(player_point[client]<0)
		{
			player_point[client] = player_lvl[client]*2-2;
			player_intelligence[client] = 0;
			player_strength[client] = 0;
			player_agility[client] = 0;
			player_dextery[client] = 0;
		}
		
		skilltree(client);
	}

	InitRace(client);
	
	loading_race[client] = 0;
}

public InitRace(client)
{
	switch(player_class[client])
	{
		case 1:
		{
		
		}
		case 2:
		{
			if(first_time_select_race[client] == 1)
			{
				first_time_select_race[client] = 0;
				/*if(IsPlayerAlive(client))
				{
					new team=GetClientTeam(client);
					new searchteam=(team==2)?3:2;
					SetEntityModel(client,(searchteam==2)?"models/player/tm_leet_variantb.mdl":"models/player/ctm_gsg9.mdl");
				}*/
				monk_shield[client]=1+RoundToFloor(player_intelligence[client]/10.0);
			}
		}
		case 3:
		{
			if(first_time_select_race[client] == 1)
			{
				first_time_select_race[client] = 0;
				new JumpsMax=RoundToFloor(player_intelligence[client]/10.0);
				if(JumpsMax==0) JumpsMax=1;
				longjumps[client]=JumpsMax;
			}
		}
		case 4:
		{
			if(first_time_select_race[client] == 1)
			{
				player_knife[client] = 1 + RoundToFloor(player_intelligence[client]/20.0);
			}
		}
		case 5:
		{
			g_haskit[client] = 1;
		}
		case 7:
		{
			Client_RemoveAllWeapons(client, "weapon_knife", true);
			if(first_time_select_race[client] == 1)
			{
				player_knife[client] = 5 + RoundToFloor(player_intelligence[client]/10.0);
			}
		}
	}
	set_renderchange(client);
	set_gravitychange(client);
	set_speedchange(client);
	if(bow[client] == 1)
	{
		FPVMI_RemoveViewModelToClient(client, "weapon_knife");
		FPVMI_RemoveWorldModelToClient(client, "weapon_knife");
		new zeus = GetPlayerWeaponSlot(client, 2);

		if(zeus != -1) 
		{
			new taser = GivePlayerItem(client, "weapon_taser");
			EquipPlayerWeapon(client, taser);
			RemovePlayerItem(client, taser);
			RemoveEdict(taser);
		}
		else
		{
			RemovePlayerItem(client, zeus);
			RemoveEdict(zeus);
			new taser = GivePlayerItem(client, "weapon_taser");
			EquipPlayerWeapon(client, taser);
			RemovePlayerItem(client, taser);
			RemoveEdict(taser);
			GivePlayerItem(client, "weapon_taser");
		}
		CreateTimer(0.1, SwitchCrow, client);
		bow[client] = 0;
	}
	if(player_lvl[client] < 11)
	{
		player_frommenu[client] = 0;
		menu_raceinfo(client, player_class[client]);
	}
}

public InitRaceSpawn(client)
{
	if(player_newrace[client])
	{
		ResetPlayer(client);
		LoadRaceData(client,player_newrace[client]);
		player_newrace[client] = 0;
	}
	switch(player_class[client])
	{
		case 2:
		{
			monk_shield[client]=1+RoundToFloor(player_intelligence[client]/10.0);
		}
		case 3:
		{
			new JumpsMax=RoundToFloor(player_intelligence[client]/10.0);
			if(JumpsMax==0) JumpsMax=1;
			longjumps[client]=JumpsMax;
			golden_bullet[client]=0;
		}
		case 4:
		{
			player_knife[client] = 1 + RoundToFloor(player_intelligence[client]/20.0);
		}
		case 5:
		{
			g_haskit[client] = 1;
		}
		case 6:
		{
			ultra_armor[client] = 0;
		}
		case 7:
		{
			Client_RemoveAllWeapons(client, "weapon_knife", true);
			player_knife[client] = 5 + RoundToFloor(player_intelligence[client]/10.0);
		}
	}
	ResetPlayerRGB(client);
	player_colored[client] = 0;
	fireball_collected[client] = 0;
	set_renderchange(client);
	set_gravitychange(client);
	set_speedchange(client);
	moneyshield[client] = 0;
	c4state[client] = 0;
	c4fake[client] = 0;
	ghoststate[client] = 0;
	player_b_eye_created[client] = 0;
	player_b_eye_viewing[client] = 0;
	SetClientViewEntity(client, client);
	if (player_b_usingwind[client] > 0) 
	{
		player_b_usingwind[client] = 0;
	}
	if(player_b_firetotem_left[client])
	{
		player_b_firetotem_left[client] = 0;
	}
	if(player_b_mine[client])
	{
		player_b_mine_count[client] = player_b_mine[client];
	}
	if(player_b_chameleon[client] && player_class[client] != 2)
	{
		new team=GetClientTeam(client);
		new searchteam=(team==2)?3:2;
		SetEntityModel(client,(searchteam==2)?"models/player/tm_phoenix_varianta.mdl":"models/player/ctm_sas_varianta.mdl");
	}
	player_ultra_armor_left[client]=player_ultra_armor[client];
	if(IsPlayerAlive(client) && GetPlayerPremium(client))
    {
		if(VIPGetPlayerarmour(client))
		{
			GivePlayerItem(client, "item_assaultsuit");
			SetEntProp(client, Prop_Send, "m_ArmorValue", 100, 1);
			if(GetClientTeam(client) == CS_TEAM_CT) GivePlayerItem(client, "item_defuser");
		}
		//GivePlayerItem(client, "weapon_decoy");
		if(VIPGetPlayergrenade(client) && (player_class[client] != 7))
		{
			GivePlayerItem(client, "weapon_molotov");
			GivePlayerItem(client, "weapon_hegrenade");
			GivePlayerItem(client, "weapon_flashbang");
			GivePlayerItem(client, "weapon_smokegrenade");
		}
    }
	decl String:mapname[128];
	GetCurrentMap(mapname, sizeof(mapname));
	if (StrContains( mapname, "fun_allinone", false) == 0)
	{
		Client_RemoveAllWeapons(client, "weapon_knife", true);
	}
	SpawnWeaponRegister(client);
}

public set_hideradar(client, hide)
{
	switch(hide)
	{
		case 0:
		{
			if(player_hideradar_ent[client] != INVALID_ENT_REFERENCE)
			{
				new iEntityHideRadar = EntRefToEntIndex(player_hideradar_ent[client]);
				if(iEntityHideRadar != INVALID_ENT_REFERENCE) AcceptEntityInput(iEntityHideRadar, "Kill");
				player_hideradar_ent[client] = INVALID_ENT_REFERENCE;
			}
		}
		case 1:
		{
			
			if(player_hideradar_ent[client] == INVALID_ENT_REFERENCE)
			{
				new iEntityHideRadar, Float:pos[3];
				GetEntPropVector(client, Prop_Data, "m_vecOrigin", pos);
				iEntityHideRadar = CreateEntityByName("prop_physics_multiplayer");
				SetEntityModel(iEntityHideRadar, "models/rxg/smokevol.mdl");
				new String:ownerinfo[16];
				Format(ownerinfo, sizeof(ownerinfo), "hideradar%d",client);
				DispatchKeyValue(iEntityHideRadar, "targetname", ownerinfo); 
				DispatchKeyValue(iEntityHideRadar, "modelscale", "0.2"); 
				TeleportEntity(iEntityHideRadar, pos, NULL_VECTOR, NULL_VECTOR);
				DispatchSpawn(iEntityHideRadar);
				new Float:minbounds[3], Float:maxbounds[3];
				minbounds = {64.0, 64.0, 64.0};  //1410 & 1403
				maxbounds = {64.0, 64.0, 64.0};				
				SetEntPropVector(iEntityHideRadar, Prop_Send, "m_vecMins", minbounds);
				SetEntPropVector(iEntityHideRadar, Prop_Send, "m_vecMins", minbounds);
				SetEntityMoveType(iEntityHideRadar, MOVETYPE_NONE);
				AcceptEntityInput(iEntityHideRadar, "DisableMotion");
				SDKHook(iEntityHideRadar, SDKHook_ShouldCollide, OnCollision); 
				SetVariantString("!activator");
				AcceptEntityInput(iEntityHideRadar, "SetParent", client, iEntityHideRadar);
				SetEdictFlags(iEntityHideRadar, (GetEdictFlags(iEntityHideRadar)&(~FL_EDICT_ALWAYS))|FL_EDICT_DONTSEND);
				player_hideradar_ent[client] = EntIndexToEntRef(iEntityHideRadar);					
			}
		}
	}
}

public bool:OnCollision(entity, collisiongroup, contentsmask, bool:originalResult) 
{
	if(collisiongroup == 13 || collisiongroup == 0) return false;
	return true;
}

public set_renderchange(client)
{
	if(ValidPlayer(client,true))
	{	
		new render = 255;

		if (player_class[client] == 7)
		{
			new inv_bonus = 255 - player_b_inv[client];
			render = 20;
			
			if(player_b_inv[client]>0)
			{
				while(inv_bonus>0)
				{
					inv_bonus-=20;
					render--;
				}
			}
			
			if(player_b_usingwind[client] == 1)
			{
				render/=2;
			}
			//PrintToChat(client, "rend %d", render);
			
			if(render<0) render=0;
			//PrintToChat(client, "rend %d", render);
			
			if(GetEntityAlpha(client)!=render)
			{
				SetEntityAlpha(client,render);
			}
			SDKHookEx(client, SDKHook_PostThinkPost, OnPostThinkPost);
		}
		else if(invisible_cast[client] == 1)
		{
			if(player_b_inv[client]>0)
			{
				render = RoundToFloor((10.0/255.0)*(255-player_b_inv[client]));
				if(GetEntityAlpha(client)!=render)
				{
					SetEntityAlpha(client,render);
				}
				SDKHookEx(client, SDKHook_PostThinkPost, OnPostThinkPost);
			}
			else
			{
				if(GetEntityAlpha(client)!=20)
				{
					SetEntityAlpha(client,20);
				}
			}
		}
		else
		{
			render = 255;
			if(player_b_inv[client]>0) render = player_b_inv[client];
			
			if(player_b_usingwind[client] == 1)
			{
				render=75;
			}
			
			if(GetEntityAlpha(client)!=render)
			{
				SetEntityAlpha(client,render);
			}
			if((player_b_inv[client] < 21) && (player_b_inv[client] > 0))
			{
				render = RoundToFloor((10.0/255.0)*(255-player_b_inv[client]));
				if(GetEntityAlpha(client)!=render)
				{
					SetEntityAlpha(client,render);
				}
				SDKHookEx(client, SDKHook_PostThinkPost, OnPostThinkPost);
			}
			else
			{
				SDKUnhook(client, SDKHook_PostThinkPost, OnPostThinkPost);
			}
		}
		
		if(render < 21)
		{
			set_hideradar(client, 1);
		}
		else
		{
			set_hideradar(client, 0);
		}
	}
}

public set_gravitychange(client)
{
	if(IsPlayerAlive(client))
	{
		if(player_class[client] == 7)
		{
			new Float:add_grav = player_dextery[client]*0.0008;
			if(player_b_gravity[client]>6) SetEntityGravity(client, 0.14-add_grav);
			else if(player_b_gravity[client]>3) SetEntityGravity(client, 0.17-add_grav);
			else SetEntityGravity(client, 0.2-add_grav);
		}
		else
		{
			new float:value = 1.0*(1.0-player_b_gravity[client]/11.25);
			SetEntityGravity(client,value);
		}
	}
}

public get_gravitychange(client)
{
	if(player_class[client] == 7)
	{
		new Float:add_grav = player_dextery[client]*0.0008;
		if(player_b_gravity[client]>6) SetEntityGravity(client, 0.14-add_grav);
		else if(player_b_gravity[client]>3) SetEntityGravity(client, 0.17-add_grav);
		else return 0.2-add_grav;
	}
	else
	{
		new float:value = 1.0*(1.0-player_b_gravity[client]/11.25);
		return value;
	}
}

public set_speedchange(client)
{
	if (IsPlayerAlive(client))
	{
		new Float:speeds;
		if(player_class[client] == 7) speeds = 0.2 + (player_dextery[client]*0.006);
		else if(player_class[client] == 4) speeds = 0.1 + (player_dextery[client]*0.006);
		else if(player_class[client] == 6) speeds = -0.1 + (player_dextery[client]*0.006);
		else speeds = RoundToFloor(player_dextery[client]*0.006);
		speeds = 1.0 + speeds;
		SetEntDataFloat(client,m_OffsetSpeed,speeds,true);
	}
}

public OnPostThinkPost(client)
{
	if(player_class[client] == 7)
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits", CSAddon_NONE);
	}
	if(invisible_cast[client] == 1)
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits", CSAddon_NONE);
	}
	if((player_b_inv[client] < 21) && (player_b_inv[client] > 0))
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits", CSAddon_NONE);
	}
}

public OnEntityCreated(entity, const String:classname[])
{
    if (strcmp(classname, "hegrenade_projectile", false) == 0)
    {
		CreateTimer(0.1, GrenKill, entity);        
    }
}  

public Action:GrenKill(Handle:timer, any:entity)
{
	if(IsValidEntity(entity))
	{
		new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
		if (owner != -1)
		{
			if(player_trapgren[owner] == 1)
			{
				new Float:pos[3];
				GetEntPropVector(entity, Prop_Data, "m_vecOrigin", pos);
				new iHe = CreateEntityByName("hegrenade_projectile");
				new trigger = CreateEntityByName("trigger_multiple");
				if (iHe != -1)
				{
					SetEntPropFloat(iHe, Prop_Send, "m_flElasticity", 0);
					SetEntPropEnt(iHe, Prop_Send, "m_hOwnerEntity", owner);
					DispatchSpawn(iHe);
					DispatchKeyValue(trigger, "spawnflags", "1");
					DispatchKeyValue(trigger, "wait", "0");
					SetEntityModel(trigger, "models/error.mdl");
					DispatchSpawn(trigger);
					TeleportEntity(iHe, pos, NULL_VECTOR, NULL_VECTOR);
					TeleportEntity(trigger, pos, NULL_VECTOR, NULL_VECTOR);
					
					new Float:m_vecMins[3] = {-87.0, -87.0, -87.0}, 
					Float:m_vecMaxs[3] = {87.0, 87.0, 175.0};
					
					SetEntPropVector(trigger, Prop_Send, "m_vecMins", m_vecMins);
					SetEntPropVector(trigger, Prop_Send, "m_vecMaxs", m_vecMaxs);
					SetEntProp(trigger, Prop_Send, "m_nSolidType", 2);
					new m_fEffects = GetEntProp(trigger, Prop_Send, "m_fEffects");
					m_fEffects |= 32;
					SetEntProp(trigger, Prop_Send, "m_fEffects", m_fEffects);
					ActivateEntity(trigger);
					SDKHook(trigger, SDKHook_Touch, OnStartTouchGren);
					SetEntPropEnt(trigger, Prop_Send, "m_hOwnerEntity", iHe);
				}
				AcceptEntityInput(entity, "kill");
				player_trapgren[owner] = 0;
			}
		}
	}
}

public OnStartTouchGren(entity, other)
{
	if((other != -1) && (entity != -1))
	{			
		if(0 < other <= MaxClients)
		{
			new caller = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
			new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
		
			if(GetClientTeam(other) != GetClientTeam(owner))
			{
				if(player_inshift[other] == 0)
				{
					new Float:fAng[3], Float:fVel[3];
					new Float:pos[3];
					GetEntPropVector(caller, Prop_Data, "m_vecOrigin", pos);
					fVel[2] = 400.0;
					fAng[2] = 100.0;
					SetEntPropFloat(caller, Prop_Send, "m_flElasticity", 1);
					TeleportEntity(caller, pos, fAng, fVel);
					AcceptEntityInput(entity, "kill");
					
					CreateTimer(0.5, GrenDestroy, caller);
				}
			}
		}
	}
}

public Action:GrenDestroy(Handle:timer, any:caller)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	SetEntProp(caller, Prop_Data, "m_takedamage", 2);
	SetEntProp(caller, Prop_Data, "m_iHealth", 1);
	SDKHooks_TakeDamage(caller, 0, 0, 1.0);
	new explode = CreateEntityByName("env_explosion");
	///new String:StringDamage[3];
	//Format(StringDamage, sizeof(StringDamage), "%d", 55+player_intelligence[owner]);
	DispatchKeyValue(explode, "iMagnitude", "20");
	DispatchKeyValue(explode, "targetname", "huntgren");
	DispatchKeyValue(explode, "iMagnitude", "20");
	DispatchKeyValue(explode, "iRadiusOverride", "150");
	SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
	
	DispatchSpawn(explode);
	new Float:position[3];
	GetEntPropVector(caller, Prop_Send, "m_vecOrigin", position);
	TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
	AcceptEntityInput(explode, "Explode");
	CreateLevelupParticle(caller, "explosion_hegrenade_interior_fallback");
	new Float:playerDistance, Float:OtherPlayerPos[3];
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i,true))
		{
			if(GetClientTeam(owner) != GetClientTeam(i))
			{
				GetClientAbsOrigin(i,OtherPlayerPos);
				playerDistance = GetVectorDistance(position,OtherPlayerPos);
				if(playerDistance<150.0)
				{
					DealCustomDamage(i, owner, 98, "huntgren");
				}
			}
		}
	}
	//AcceptEntityInput(caller, "Kill");
	//RemoveEdict(caller);
	AcceptEntityInput(explode, "kill");
	//RemoveEdict(explode);
	//SetEntProp(caller, Prop_Data, "m_nNextThinkTick", 1); //for smoke
	//EmitAmbientSound( fire_expsnd, position, _, SNDLEVEL_TRAIN  );*/
}

public skilltree(client)
{
	if(player_point[client] < 1) 
		return;
	
	new Handle:menu = CreateMenu(SkilltreeMenuHandler, MENU_ACTIONS_ALL);
	
	new String:menu_title[48], String:points[32];
	
	Format(menu_title, sizeof(menu_title), "%T", "Skilltree_Menu_Title", client);
	Format(points, sizeof(points), "%T", "points", client);
	Format(menu_title, sizeof(menu_title), "%s\n%d %s", menu_title, player_point[client], points);
	
	SetSafeMenuTitle(menu, menu_title);
	
	new String:menu_item[200], String:item_descr[200];
	
	Format(menu_item, sizeof(menu_item), "%T", "Intelligence [current]", client, player_intelligence[client]);
	Format(item_descr, sizeof(item_descr), "%T", "Intelligence DESCR", client);
	Format(menu_item, sizeof(menu_item), "%s\n%s", menu_item, item_descr);
	AddMenuItem(menu, "choose_int", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Strength [current]", client, player_strength[client]);
	Format(item_descr, sizeof(item_descr), "%T", "Strength DESCR", client, player_strength[client]*2);
	Format(menu_item, sizeof(menu_item), "%s\n%s", menu_item, item_descr);
	AddMenuItem(menu, "choose_str", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Agility [current]", client, player_agility[client]);
	Format(item_descr, sizeof(item_descr), "%T", "Agility DESCR", client);
	Format(menu_item, sizeof(menu_item), "%s\n%s", menu_item, item_descr);
	AddMenuItem(menu, "choose_agi", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Dextery [current]", client, player_dextery[client]);
	Format(item_descr, sizeof(item_descr), "%T", "Dextery DESCR", client);
	Format(menu_item, sizeof(menu_item), "%s\n%s", menu_item, item_descr);
	AddMenuItem(menu, "choose_dex", menu_item);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public SkilltreeMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "choose_int", false))
			{
				if (player_intelligence[client]<50)
				{
					player_point[client]-=1;
					player_intelligence[client]+=1;
				}
				else 
				{
					Diablo_ChatMessage(client,"%T","You have maximum intelligence",client);
				}
			}
			if (StrEqual(info, "choose_str", false))
			{
				if (player_strength[client]<50)
				{
					player_point[client]-=1;
					player_strength[client]+=1;
				}
				else 
				{
					Diablo_ChatMessage(client,"%T","You have maximum strength",client);
				}
			}
			if (StrEqual(info, "choose_agi", false))
			{
				if (player_agility[client]<50)
				{
					player_point[client]-=1;
					player_agility[client]+=1;
				}
				else 
				{
					Diablo_ChatMessage(client,"%T","You have maximum agility",client);
				}
			}
			if (StrEqual(info, "choose_dex", false))
			{
				if (player_dextery[client]<50)
				{
					player_point[client]-=1;
					player_dextery[client]+=1;
				}
				else 
				{
					Diablo_ChatMessage(client,"%T","You have maximum dextery",client);
				}
			}
			if (player_point[client] > 0) 
					skilltree(client);
			
			set_gravitychange(client);
			set_speedchange(client);
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
 
	return 0;
}

public reset_skill(client)
{	
	player_point[client] = (player_lvl[client]-1)*2;
	player_intelligence[client] = 0;
	player_strength[client] = 0;
	player_agility[client] = 0;
	player_dextery[client] = 0;
	
	skilltree(client);
}


public ShowChangeraceMenu(client)
{
	new Handle:menu = CreateMenu(ChangeraceMenuHandler, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Race_Menu_Title", client);
	
	SetCorrectMenuPagination(menu, 8);
	
	new String:menu_item[64];
	
	Format(menu_item, sizeof(menu_item), "%T", "RACE1 {level}", client, player_class_lvl[client][1]);
	AddMenuItem(menu, "choose_race1", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE2 {level}", client, player_class_lvl[client][2]);
	AddMenuItem(menu, "choose_race2", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE3 {level}", client, player_class_lvl[client][3]);
	AddMenuItem(menu, "choose_race3", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE4 {level}", client, player_class_lvl[client][4]);
	AddMenuItem(menu, "choose_race4", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE5 {level}", client, player_class_lvl[client][5]);
	AddMenuItem(menu, "choose_race5", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE6 {level}", client, player_class_lvl[client][6]);
	AddMenuItem(menu, "choose_race6", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE7 {level}", client, player_class_lvl[client][7]);
	AddMenuItem(menu, "choose_race7", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE8 {level}", client, player_class_lvl[client][8]);
	AddMenuItem(menu, "choose_race8", menu_item);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
	
	loading_xp[client]=0;
}

stock SetCorrectMenuPagination(Handle:menu, numSlots)
{
    if (GetMenuStyleHandle(MenuStyle_Valve) != GetMenuStyleHandle(MenuStyle_Radio) && numSlots < 10)
        SetMenuPagination(menu, MENU_NO_PAGINATION);
}

public ChangeraceMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			menuRaceNumber = StringToInt(info[11]);
			if(loading_race[client] == 0)
			{
				if(player_firstconnect[client])
				{
					ResetPlayer(client);
					LoadRaceData(client,menuRaceNumber);
					player_firstconnect[client] = 0;
				}
				else
				{
					player_newrace[client] = menuRaceNumber;
					ShowHUDHint(client, "%T", "Race will change in NEXT spawn/round", client);
					Diablo_ChatMessage(client, "%T", "Race will change in NEXT spawn/round", client);
				}
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
 
	return 0;
}

public showskills(client)
{
	new Handle:menu = CreateMenu(ShowskillsMenuHandler, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Skills_menu", client);
	
	new String:menu_item[1024], String:menu_item2[1024];
	
	Format(menu_item, sizeof(menu_item), "%T", "You have {strength} strength - this give you {HP} HP", client, player_strength[client], player_strength[client]*2);
	Format(menu_item2, sizeof(menu_item2), "%T", "You have {dex} Dextery - speed boost of {boost} and reduce magic damage taken with {reduce}", client, player_dextery[client], player_dextery[client], player_dextery[client]*2);
	Format(menu_item2, sizeof(menu_item2), "%s%s", menu_item, menu_item2);
	Format(menu_item, sizeof(menu_item), "%T", "You have {agi} Agility - Helps to find unique items and reduce damage from normal attacks by {reduce}", client, player_agility[client], player_agility[client]);
	Format(menu_item2, sizeof(menu_item2), "%s%s", menu_item2, menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "You have {int} intelligence - this will make the USE items more efficient", client, player_intelligence[client]);
	Format(menu_item2, sizeof(menu_item2), "%s%s", menu_item2, menu_item);
	AddMenuItem(menu, "2_skills", menu_item2);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public ShowskillsMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	return 0;
}

public DisplayMainMenu(client)
{
	new Handle:menu = CreateMenu(MainMenuHandler, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Main_Menu_Title", client);
	
	//SetCorrectMenuPagination(menu, 8);
	
	new String:menu_item[64];
	
	Format(menu_item, sizeof(menu_item), "%T", "Change race", client);
	AddMenuItem(menu, "menu_race", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Item Information", client);
	AddMenuItem(menu, "menu_ii", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Drop Current Item", client);
	AddMenuItem(menu, "menu_drop", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Show Help", client);
	AddMenuItem(menu, "menu_help", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Shop", client);
	AddMenuItem(menu, "menu_shop", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Skills Information", client);
	AddMenuItem(menu, "menu_skills", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Settings", client);
	AddMenuItem(menu, "menu_settings", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "See another player's information", client);
	AddMenuItem(menu, "menu_players", menu_item);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MainMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "menu_race", false))
			{
				TryToOpenChangerace(client);
			}
			if (StrEqual(info, "menu_ii", false))
			{
				iteminfo(client,2,1);
			}
			if (StrEqual(info, "menu_drop", false))
			{
				dropitem(client);
			}
			if (StrEqual(info, "menu_help", false))
			{
				showhelp(client);
			}
			if (StrEqual(info, "menu_shop", false))
			{
				DisplayShopMenu(client);
			}
			if (StrEqual(info, "menu_skills", false))
			{
				showskills(client);
			}
			if (StrEqual(info, "menu_settings", false))
			{
				showsettings(client);
			}
			if (StrEqual(info, "menu_players", false))
			{
				DisplayWho(client);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
 
	return 0;
}

public showsettings(client)
{
	new String:enabled[32], String:disabled[32], String:ii_chat_t[100], String:ii_award_t[100], String:hint_disable_t[100], String:menu_item[200];
	Format(enabled, sizeof(enabled), "%T", "Enabled", client);
	Format(disabled, sizeof(disabled), "%T", "Disabled", client);
	Format(ii_chat_t, sizeof(ii_chat_t), "%T", "ii_chat", client);
	Format(ii_award_t, sizeof(ii_award_t), "%T", "ii_award", client);
	Format(hint_disable_t, sizeof(hint_disable_t), "%T", "hint_disable", client);
	
	new Handle:menu = CreateMenu(MenuHandlerSettings, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Settings", client);
	if(ii_chat[client])
	{
		Format(menu_item, sizeof(menu_item), "%s %s", ii_chat_t, enabled);
		AddMenuItem(menu, "menu_ii_chat", menu_item);
	}
	else
	{
		Format(menu_item, sizeof(menu_item), "%s %s", ii_chat_t, disabled);
		AddMenuItem(menu, "menu_ii_chat", menu_item);
	}
	if(ii_award[client])
	{
		Format(menu_item, sizeof(menu_item), "%s %s", ii_award_t, enabled);
		AddMenuItem(menu, "menu_award", menu_item);
	}
	else
	{
		Format(menu_item, sizeof(menu_item), "%s %s", ii_award_t, disabled);
		AddMenuItem(menu, "menu_award", menu_item);
	}
	if(hint_disable[client])
	{
		Format(menu_item, sizeof(menu_item), "%s %s", hint_disable_t, disabled);
		AddMenuItem(menu, "hint_disable", menu_item);
	}
	else
	{
		Format(menu_item, sizeof(menu_item), "%s %s", hint_disable_t, enabled);
		AddMenuItem(menu, "hint_disable", menu_item);
	}
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandlerSettings(Handle:menu, MenuAction:action, param1, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "menu_ii_chat"))
			{
				extra_changed[param1] = 1;
				if(ii_chat[param1])
				{
					ii_chat[param1] = 0;
				}
				else
				{
					ii_chat[param1] = 1;
				}
			}
			if (StrEqual(info, "menu_award"))
			{
				extra_changed[param1] = 1;
				if(ii_award[param1])
				{
					ii_award[param1] = 0;
				}
				else
				{
					ii_award[param1] = 1;
				}
			}
			if (StrEqual(info, "hint_disable"))
			{
				extra_changed[param1] = 1;
				if(hint_disable[param1])
				{
					hint_disable[param1] = 0;
				}
				else
				{
					hint_disable[param1] = 1;
				}
			}
			showsettings(param1);
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: DisplayMainMenu(param1);
			}
		}
	}
 
	return 0;
}

public showhelp(client)
{
	new String:menu_item[200];
	
	new Handle:menu = CreateMenu(MenuHandlerHelp, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Help_menu", client);
	Format(menu_item, sizeof(menu_item), "%T", "Introduction", client);
	AddMenuItem(menu, "help_main", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Races", client);
	AddMenuItem(menu, "help_classes", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Items", client);
	AddMenuItem(menu, "help_items", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Commands", client);
	AddMenuItem(menu, "help_commands", menu_item);
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandlerHelp(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "help_main"))
			{
				//FakeClientCommandEx(param1, "say !intro");
				showintro(client);
			}
			if (StrEqual(info, "help_classes"))
			{
				player_frommenu[client] = 1;
				ShowClassHelp(client);
			}
			if (StrEqual(info, "help_items"))
			{
				//FakeClientCommandEx(client, "say !itemdesc");
				showitemsdescr(client);
			}
			if (StrEqual(info, "help_commands"))
			{
				//FakeClientCommandEx(client, "say !commands");
				showcommanddescr(client);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: DisplayMainMenu(client);
			}
		}
	}
 
	return 0;
}

public showintro(client)
{
	new String:menu_item[200];
	
	new Handle:menu = CreateMenu(MenuHandlerIntro, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Introduction", client);
	new String:title[512];
	Format(title,sizeof(title),"Diablo - is a mod like Warcraft mod");
	Format(title[strlen(title)],sizeof(title),"\nwith levels, magic and items");
	Format(title[strlen(title)],sizeof(title),"\nYou got XP, levels by killing enemies");
	Format(title[strlen(title)],sizeof(title),"\nand completing game objectives");
	AddMenuItem(menu, "help_main", title);
	Format(title,sizeof(title),"Race abilities can be active with R and hoding knife");
	Format(title[strlen(title)],sizeof(title),"\nAlso they can be activated if you stand still with knife");
	Format(title[strlen(title)],sizeof(title),"\nYou will receive an item for kill.");
	AddMenuItem(menu, "help_main2", title);
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandlerIntro(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "help_main"))
			{
				showintro(client);
			}
			if (StrEqual(info, "help_main2"))
			{
				showintro(client);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: showhelp(client);
			}
		}
	}
 
	return 0;
}

public showitemsdescr(client)
{
	new String:menu_item[200];
	
	new Handle:menu = CreateMenu(MenuHandlerItemsDescr, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "What is Items?", client);
	new String:title[512];
	Format(title,sizeof(title),"You will receive an item for kill");
	Format(title[strlen(title)],sizeof(title),"\nItem will give you additional abilities");
	Format(title[strlen(title)],sizeof(title),"\nItem strength will decrease");
	Format(title[strlen(title)],sizeof(title),"\nif enemies will damage you");
	Format(title[strlen(title)],sizeof(title),"\nYou will lose the item if his strength is 0");
	AddMenuItem(menu, "help_main", title);
	AddMenuItem(menu, "help_main3", "You can repair item in the Shop");
	Format(title,sizeof(title),"Items can be passive (invisibility, vampiric)");
	Format(title[strlen(title)],sizeof(title),"\nand active (totems, mines, fireballs)");
	Format(title[strlen(title)],sizeof(title),"\nActive items can be activated with E button.");
	AddMenuItem(menu, "help_main2", title);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandlerItemsDescr(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if (StrEqual(info, "help_main"))
			{
				showintro(client);
			}
			if (StrEqual(info, "help_main2"))
			{
				showintro(client);
			}
			if (StrEqual(info, "help_main3"))
			{
				showintro(client);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: showhelp(client);
			}
		}
	}
 
	return 0;
}

public showcommanddescr(client)
{
	new String:menu_item[200];
	
	new Handle:menu = CreateMenu(MenuHandlerCommsDescr, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Select a command for more info", client);
	new String:title[512];
	AddMenuItem(menu, "1", "say menu - Main menu");
	AddMenuItem(menu, "2", "say class - Change race");
	AddMenuItem(menu, "3", "say ii - Item description");
	AddMenuItem(menu, "4", "say drop - Drop item");
	AddMenuItem(menu, "5", "say shop - Open shop menu");
	AddMenuItem(menu, "6", "say skills - Show your skills");
	AddMenuItem(menu, "7", "say help - Open help menu");
	AddMenuItem(menu, "8", "say who - Players list with races, levels and items");
	AddMenuItem(menu, "9", "say xp - Show experience points");
	AddMenuItem(menu, "10", "say reset - Reset skills points and open skills menu");
	AddMenuItem(menu, "11", "say buyitem - To buy item directly");
	AddMenuItem(menu, "12", "say upgrade - To upgrade item directly");
	AddMenuItem(menu, "13", "say zal - To buy Zal item directly");
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MenuHandlerCommsDescr(Handle:menu, MenuAction:action, client, selection)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			showcommanddescr(client);
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(selection)
			{
				case MenuCancel_Exit: showhelp(client);
			}
		}
	}
 
	return 0;
}

public DisplayShopMenu(client)
{
	new Handle:menu = CreateMenu(ShopMenuHandler, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "%T", "Shop_Menu_Title", client);
	
	new String:menu_item[64];
	
	Format(menu_item, sizeof(menu_item), "%T", "Repair/upgrade item", client);
	AddMenuItem(menu, "menu_repair", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "Buy new item", client);
	AddMenuItem(menu, "menu_buyitem", menu_item);
	if(GetPlayerPremium(client))
	{
		Format(menu_item, sizeof(menu_item), "%T", "Buy exp", client);
		AddMenuItem(menu, "menu_buyexp", menu_item);
	}
	else
	{
		Format(menu_item, sizeof(menu_item), "%T", "Buy exp no VIP", client);
		AddMenuItem(menu, "menu_buyexpnovip", menu_item, ITEMDRAW_DISABLED);
	}
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public ShopMenuHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			new currency = GetCurrency(client);
			if (StrEqual(info, "menu_repair"))
			{
				if(player_item_id[client] > 0)
				{
					if(currency > 8999)
					{
						upgrade_item(client);
						if(round_end)
						{
							player_spendmoney[client] += 9000;
							if(!player_spend_1_time[client])
							{
								player_lastmoney[client] = currency;
								player_spend_1_time[client] = 1;
							}
						}
						AddCurrency(client, -9000);
					}
					else
					{
						Diablo_ChatMessage(client,"%T","Insufficient funds",client);
					}
				}
			}
			if (StrEqual(info, "menu_buyitem"))
			{
				if(player_item_id[client] == 0)
				{
					if(currency > 4999)
					{
						award_item(client, 0);
						if(round_end)
						{
							player_spendmoney[client] += 5000;
							if(!player_spend_1_time[client])
							{
								player_lastmoney[client] = currency;
								player_spend_1_time[client] = 1;
							}
						}
						AddCurrency(client, -5000);
					}
					else
					{
						Diablo_ChatMessage(client,"%T","Insufficient funds",client);
					}
				}
				else
				{
					Diablo_ChatMessage(client,"%T","Already own item",client);
				}
			}
			if (StrEqual(info, "menu_buyexp"))
			{
				if(currency > 7499)
				{
					new xp_award = GetRandomInt(12,50);
					new String:killaward[64];
					Format(killaward, sizeof(killaward), "%T", "purchasing Zal", client);
					Give_Xp(client,xp_award,killaward);
					if(round_end)
					{
						player_spendmoney[client] += 7500;
						if(!player_spend_1_time[client]) 
						{
							player_lastmoney[client] = currency;
							player_spend_1_time[client] = 1;
						}
					}
					AddCurrency(client, -7500);
				}
				else
				{
					Diablo_ChatMessage(client,"%T","Insufficient funds",client);
				}
			}
			if (StrEqual(info, "menu_buyexpnovip"))
			{
				Diablo_ChatMessage(client,"%T","Buy VIP",client);
				DisplayShopMenu(client);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: DisplayMainMenu(client);
			}
		}
	}
 
	return 0;
}

public ShowClassHelp(client)
{
	new Handle:menu = CreateMenu(RaceInfoHandler, MENU_ACTIONS_ALL);
	SetMenuTitle(menu, "Info", client);
	
	new String:menu_item[64];
	
	Format(menu_item, sizeof(menu_item), "%T", "RACE1", client);
	AddMenuItem(menu, "choose_race1", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE2", client);
	AddMenuItem(menu, "choose_race2", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE3", client);
	AddMenuItem(menu, "choose_race3", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE4", client);
	AddMenuItem(menu, "choose_race4", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE5", client);
	AddMenuItem(menu, "choose_race5", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE6", client);
	AddMenuItem(menu, "choose_race6", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE7", client);
	AddMenuItem(menu, "choose_race7", menu_item);
	Format(menu_item, sizeof(menu_item), "%T", "RACE8", client);
	AddMenuItem(menu, "choose_race8", menu_item);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public RaceInfoHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			new currency = GetCurrency(client);
			if (StrEqual(info, "choose_race1"))
			{
				menu_raceinfo(client, 1);
			}
			if (StrEqual(info, "choose_race2"))
			{
				menu_raceinfo(client, 3);
			}
			if (StrEqual(info, "choose_race3"))
			{
				menu_raceinfo(client, 3);
			}
			if (StrEqual(info, "choose_race4"))
			{
				menu_raceinfo(client, 4);
			}
			if (StrEqual(info, "choose_race5"))
			{
				menu_raceinfo(client, 5);
			}
			if (StrEqual(info, "choose_race6"))
			{
				menu_raceinfo(client, 6);
			}
			if (StrEqual(info, "choose_race7"))
			{
				menu_raceinfo(client, 7);
			}
			if (StrEqual(info, "choose_race8"))
			{
				menu_raceinfo(client, 8);
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit: showhelp(client);
			}
		}
	}
 
	return 0;
}

public DisplayWho(client)
{
	new found=0;
	new targetlist[MAXPLAYERS+1];
	new String:name[32];
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i))
		{
			targetlist[found++]=i;
		}
	}
	if(found>1)
	{
		//redundant code..maybe we should optmize?
		new Handle:hMenu=CreateMenu(playerinfoSelected1);
		SetMenuExitButton(hMenu,true);
		SetSafeMenuTitle(hMenu,"%T\n ","Select a player to view its information",client);
		// Iteriate through the players and print them out
		decl String:playername[32];
		decl String:playerbuf[4];
		decl String:racename[64];
		decl String:racenumtext[64];
		decl String:menuitem[100] ;
		for(new i=0;i<found;i++)
		{
			new clientindex=targetlist[i];
			Format(playerbuf,sizeof(playerbuf),"%d",clientindex);  //target index
			GetClientName(clientindex,playername,sizeof(playername));
			if(player_class[clientindex] == 0)
			{
				Format(racename, sizeof(racename), "%T", "No race", client);
			}
			else
			{
				Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[clientindex]);
				Format(racename, sizeof(racename), "%T", racenumtext, client);
			}
			
			Format(menuitem,sizeof(menuitem),"%T","{player} ({racename} LVL {amount})",client,playername,racename,player_lvl[clientindex]);
			
			AddMenuItem(hMenu,playerbuf,menuitem);
		}
		DisplayMenu(hMenu,client,MENU_TIME_FOREVER);
	}
}

public playerinfoSelected1(Handle:menu,MenuAction:action,client,selection)
{
	if(action==MenuAction_Select)
	{
		decl String:SelectionInfo[4];
		decl String:SelectionDispText[256];
		new SelectionStyle;
		GetMenuItem(menu,selection,SelectionInfo,sizeof(SelectionInfo),SelectionStyle, SelectionDispText,sizeof(SelectionDispText));
		new target=StringToInt(SelectionInfo);
		if(ValidPlayer(target))
			playertargetMenu(client,target);
		else
			Diablo_ChatMessage(client,"%T","Player has left the server",client);
	}
	if(action==MenuAction_End)
	{
		CloseHandle(menu);
	}
	if(action==MenuAction_Cancel)
	{
		DisplayMainMenu(client);
	}
}


playertargetMenu(client,target) 
{
	new Handle:hMenu=CreateMenu(playertargetMenuSelected);
	SetMenuExitButton(hMenu,true);

	new String:targetname[32];
	GetClientName(target,targetname,sizeof(targetname));

	new String:racename[64];
	new String:racenumtext[64];
	new String:skillname[64];

	if(player_class[target] == 0)
	{
		Format(racename, sizeof(racename), "%T", "No race", client);
	}
	else
	{
		Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[target]);
		Format(racename, sizeof(racename), "%T", racenumtext, client);
	}

	new level;
	level=player_lvl[target];

	new String:title[3000];

	Format(title,sizeof(title),"%T\n \n","Information for {player}",client,targetname);
	Format(title,sizeof(title),"%s%T\n \n",title,"Total levels: {amount}",client,player_totallvl[target]);

	if(level<101)
	{
		Format(title,sizeof(title),"%s%T",title,"Current Race: {racename} (LVL {amount}) XP: {amount}/{amount}",client,racename,level,player_xp[target],DiabloGetReqXP(level));
	}
	else
	{
		Format(title,sizeof(title),"%s%T",title,"Current Race: {racename} (LVL {amount}) XP: {amount}",client,racename,level,player_xp[target]);
	}
	Format(title,sizeof(title),"%s\n",title);
	
	// IF FALSE:
	// ONLY SHOW ITEMS IF YOU ARE OWNER
	// DON'T SHOW OTHER PLAYERS ITEMS
	//  if(client==target)
	Format(title,sizeof(title),"%s\n \n%T\n",title,"Item:",client);

	new String:itemname[64];
	if(player_item_id[target] == 0)
	{
		Format(itemname, sizeof(itemname), "%T", "No item", client);
	}
	else if(player_item_id[target] == 100)
	{
		Format(itemname, sizeof(itemname), "%s", player_item_name[target]);
	}
	else
	{
		Format(itemname, sizeof(itemname), "%T", player_item_name[target], client);
	}
	
	Format(title,sizeof(title),"%s\n%s",title,itemname);
	Format(title,sizeof(title),"%s\n \n",title);


	SetSafeMenuTitle(hMenu,"%s",title);
	// Iteriate through the races and print them out




	new String:buf[3];

	IntToString(target,buf,sizeof(buf));
	new String:str[100];
	Format(str,sizeof(str),"%T","Refresh",client);
	AddMenuItem(hMenu,buf,str);

	new String:selectionDisplayBuff[150];

	Format(selectionDisplayBuff,sizeof(selectionDisplayBuff),"%T","See all players with race {racename}",client,racename) ;
	AddMenuItem(hMenu,buf,selectionDisplayBuff); 

	Format(selectionDisplayBuff,sizeof(selectionDisplayBuff),"%T","Spectate Player",client) ;
	AddMenuItem(hMenu,buf,selectionDisplayBuff); 

	DisplayMenu(hMenu,client,MENU_TIME_FOREVER);
}

public playertargetMenuSelected(Handle:menu,MenuAction:action,client,selection)
{
	if(action==MenuAction_Select)
	{
		decl String:SelectionInfo[4];
		decl String:SelectionDispText[256];
		new SelectionStyle;
		GetMenuItem(menu,selection,SelectionInfo,sizeof(SelectionInfo),SelectionStyle, SelectionDispText,sizeof(SelectionDispText));
		new target=StringToInt(SelectionInfo);
		if(!ValidPlayer(target))
		{
			Diablo_ChatMessage(client,"%T","Player has left the server",client);
		}
		else
		{
			if(selection==0)
			{
				playertargetMenu(client,target);
			}
			if(selection==1)
			{
				new raceid=player_class[target];
				if(player_class[target] > 0)
				{
					playersWhoAreThisRaceMenu(client,raceid);
				}
				else
				{
					playertargetMenu(client,target);
				}
			}
			if(selection==2)
			{
				if(ValidPlayer(target,true))
				{
				  SetEntDataEnt2(client, FindSendPropInfo("CBasePlayer", "m_hObserverTarget"),target,true);
				}
				else
				{
				  Diablo_ChatMessage(client,"%T","Player Not Alive",client);
				}
				playertargetMenu(client,target);
			}
		}
	}
	if(action==MenuAction_End)
	{
		CloseHandle(menu);
	}
	if(action==MenuAction_Cancel)
	{
		DisplayMainMenu(client);
	}
}

playersWhoAreThisRaceMenu(client,raceid)
{
	new Handle:hMenu=CreateMenu(playersWhoAreThisRaceSel);
	SetMenuExitButton(hMenu,true);

	new String:racename[64];
	new String:racenumtext[64];
	Format(racenumtext, sizeof(racenumtext), "RACE%d", raceid);
	Format(racename, sizeof(racename), "%T", racenumtext, client);

	SetSafeMenuTitle(hMenu,"%T\n \n","People who are race: {racename}",client,racename);

	decl String:playername[64];
	decl String:playerbuf[4];

	for(new x=1;x<=MaxClients;x++)
	{
		if(ValidPlayer(x)&&player_class[x]==raceid)
		{
			Format(playerbuf,sizeof(playerbuf),"%d",x);  //target index
			GetClientName(x,playername,sizeof(playername));
			decl String:menuitemstr[100];
			decl String:teamname[10];
			GetShortTeamName( GetClientTeam(x),teamname,sizeof(teamname));
			Format(menuitemstr,sizeof(menuitemstr),"%T","{player} (Level {amount}) [{team}]",client,playername,player_lvl[x],teamname);
			AddMenuItem(hMenu,playerbuf,menuitemstr);
		}
	}
	DisplayMenu(hMenu,client,MENU_TIME_FOREVER);  
}
public playersWhoAreThisRaceSel(Handle:menu,MenuAction:action,client,selection)
{
	if(action==MenuAction_Select)
	{
		decl String:SelectionInfo[4];
		decl String:SelectionDispText[256];
		new SelectionStyle;
		GetMenuItem(menu,selection,SelectionInfo,sizeof(SelectionInfo),SelectionStyle, SelectionDispText,sizeof(SelectionDispText));
		new target=StringToInt(SelectionInfo);
		if(ValidPlayer(target))
			playertargetMenu(client,target);
		else
			Diablo_ChatMessage(client,"%T","Player has left the server",client);

	}
	if(action==MenuAction_End)
	{
		CloseHandle(menu);
	}
	if(action==MenuAction_Cancel)
	{
		DisplayMainMenu(client);
	}
}

public dropitem(client)
{
	decl String:hinttext[200];
	if (player_item_id[client] == 0)
	{
		Format(hinttext, sizeof(hinttext), "%T", "You have no item to drop!", client);
		PrintHintText(client, hinttext);
		return;
	} 
		
	if (item_durability[client] <= 0) 
	{
		Format(hinttext, sizeof(hinttext), "%T", "Item lost due to durability!", client);
		PrintHintText(client, hinttext);
		Diablo_ChatMessage(client,"%T","Item lost due to durability chat",client);
		EmitSoundToAll(itembrokensnd,client);
	}
	else 
	{
		Format(hinttext, sizeof(hinttext), "%T", "Item dropped", client);
		PrintHintText(client, hinttext);
		EmitSoundToAll(dropsnd,client);
	}
	
	player_item_id[client] = 0;
	player_item_name[client] = "None";
	player_b_gamble[client] = 0;	//Because gamble uses reset skills
		
	if (player_b_extrastats[client] > 0)
	{
		SubtractStats(client,player_b_extrastats[client]);
	}
	if(player_ring[client]>0) SubtractRing(client);
	player_ring[client]=0;
	
	reset_item_skills(client);
	//set_task(3.0,"changeskin_id_1",id)
	
	set_renderchange(client);
	set_gravitychange(client);
}

public reset_item_skills(client)
{
	item_boosted[client] = 0;
	item_durability[client] = 0;
	jumps[client] = 0;
	gravitytimer[client] = 0;
	player_b_fireball_timer[client] = 0;
	player_race_fireball_timer[client] = 0;
	player_b_froglegs_timer[client] = 0;
	player_b_vampire[client] = 0;	//Vampyric damage
	player_b_damage[client] = 0;		//Bonus damage
	player_b_money[client] = 0;		//Money bonus
	player_b_gravity[client] = 0;	//Gravity bonus : 1 = best
	player_b_inv[client] = 0;		//Invisibility bonus
	player_b_grenade[client] = 0;	//Grenade bonus = 1/chance to kill
	player_b_reduceH[client] = 0;	//Reduces player health each round start
	player_b_theif[client] = 0;		//Amount of money to steal
	player_b_respawn[client] = 0;	//Chance to respawn upon death
	player_b_explode[client] = 0;	//Radius to explode upon death
	player_b_heal[client] = 0;		//Ammount of hp to heal each 5 second
	player_b_blind[client] = 0;		//Chance 1/Value to blind the enemy
	player_b_fireshield[client] = 0;	//Protects against explode and grenade bonus 
	player_b_meekstone[client] = 0;	//Ability to lay a fake c4 and detonate 
	player_b_teamheal[client] = 0;	//How many hp to heal when shooting a teammate 
	player_b_redirect[client] = 0;	//How much damage will the player redirect 
	player_b_fireball[client] = 0;	//Ability to shot off a fireball value = radius *
	player_b_ghost[client] = 0;		//Ability to walk through walls
	player_b_eye[client] = 0;	         //Ability to snarkattack
	player_b_blink[client] = 0;	//Abiliy to use railgun
	player_b_windwalk[client] = 0;	//Ability to windwalk
	player_b_usingwind[client] = 0;	//Is player using windwalk
	player_b_froglegs[client] = 0;
	player_b_silent[client] = 0;
	player_b_dagon[client] = 0;		//Abliity to nuke opponents
	player_b_sniper[client] = 0;		//Ability to kill faster with scout
	player_b_jumpx[client] = 0;
	player_b_smokehit[client] = 0;
	player_b_extrastats[client] = 0;
	player_b_firetotem[client] = 0;
	player_b_hook[client] = 0;
	player_b_chameleon[client] = 0;
	player_b_darksteel[client] = 0;
	player_b_illusionist[client] = 0;
	player_b_mine[client] = 0;
	wear_sun[client] = 0;
	player_sword[client] = 0; 
	player_ultra_armor_left[client] = 0;
	player_ultra_armor[client] = 0;
	moneyshield[client] = 0;
}

public award_item(client, itemnum)
{
	if (player_item_id[client] != 0)
	{
		return;
	}
	
	new rannum = GetRandomInt(1,68);
	
	new maxfind = player_agility[client];
	if (maxfind > 15) maxfind = 15;
	
	new rf = GetRandomInt(1,25-maxfind);
	
	if (itemnum > 0) rannum = itemnum;
	else if (itemnum < 0) return;
	
	if (rf == 3 && itemnum == 0) //We found a rare item			
	{
		award_unique_item(client);
		rannum = -1;
	}
	
	if(itemnum == 100) award_unique_item(client);
	
	if((player_class[client] == 7) && (rannum == 49 || rannum == 64)) rannum = 50;
	if((player_class[client] == 7) && (rannum == 17)) rannum = 18;
	if((player_class[client] == 7) && (rannum == 31)) rannum = 32;
	
	if((player_totallvl[client] < 13) && (rannum == 17)) rannum = 18;
	
	EmitSoundToAll(recieveisnd,client);
	
	//Set durability, make this item dependant?
	item_durability[client] = 250;
	switch(rannum)
	{
		case 1:
		{
			player_item_name[client] = "Bronze Amplifier";
			player_item_id[client] = rannum;
			player_b_damage[client] = GetRandomInt(1,3);
		}
		case 2:
		{
			player_item_name[client] = "Silver Amplifier";
			player_item_id[client] = rannum;
			player_b_damage[client] = GetRandomInt(3,6);
		}
		case 3:
		{
			player_item_name[client] = "Gold Amplifier";
			player_item_id[client] = rannum;
			player_b_damage[client] = GetRandomInt(6,10);
		}
		case 4:
		{
			player_item_name[client] = "Vampyric Staff";
			player_item_id[client] = rannum;
			player_b_vampire[client] = GetRandomInt(1,4);
		}
		case 5:
		{
			player_item_name[client] = "Vampyric Amulet";
			player_item_id[client] = rannum;
			player_b_vampire[client] = GetRandomInt(4,6);
		}
		case 6:
		{
			player_item_name[client] = "Vampyric Scepter";
			player_item_id[client] = rannum;
			player_b_vampire[client] = GetRandomInt(6,9);
		}
		case 7:
		{
			player_item_name[client] = "Small bronze bag";
			player_item_id[client] = rannum;
			player_b_money[client] = GetRandomInt(150,500);
		}
		case 8:
		{
			player_item_name[client] = "Medium silver bag";
			player_item_id[client] = rannum;
			player_b_money[client] = GetRandomInt(500,1200);
		}
		case 9:
		{
			player_item_name[client] = "Large gold bag";
			player_item_id[client] = rannum;
			player_b_money[client] = GetRandomInt(1200,3000);
		}
		case 10:
		{
			player_item_name[client] = "Small angel wings";
			player_item_id[client] = rannum;
			player_b_gravity[client] = GetRandomInt(1,5);
			if(IsPlayerAlive(client)) set_gravitychange(client);
		}
		case 11:
		{
			player_item_name[client] = "Arch angel wings";
			player_item_id[client] = rannum;
			player_b_gravity[client] = GetRandomInt(5,9);
			if(IsPlayerAlive(client)) set_gravitychange(client);
		}
		case 12:
		{
			player_item_name[client] = "Invisibility Rope";
			player_item_id[client] = rannum;
			player_b_inv[client] = GetRandomInt(150,200);
			if(IsPlayerAlive(client)) set_renderchange(client);
		}
		case 13:
		{
			player_item_name[client] = "Invisibility Coat";
			player_item_id[client] = rannum;
			player_b_inv[client] = GetRandomInt(110,150);
			if(IsPlayerAlive(client)) set_renderchange(client);
		}
		case 14:
		{
			player_item_name[client] = "Invisibility Armor";
			player_item_id[client] = rannum;
			player_b_inv[client] = GetRandomInt(70,110);
			if(IsPlayerAlive(client)) set_renderchange(client);
		}
		case 15:
		{
			player_item_name[client] = "Firerope";
			player_item_id[client] = rannum;
			player_b_grenade[client] = GetRandomInt(3,6);
		}
		case 16:
		{
			player_item_name[client] = "Fire Amulet";
			player_item_id[client] = rannum;
			player_b_grenade[client] = GetRandomInt(2,4);
		}
		case 17:
		{
			player_item_name[client] = "Stalkers ring";
			player_item_id[client] = rannum;
			player_b_reduceH[client] = 95;
			player_b_inv[client] = 9;
			item_durability[client] = 100;
			if(IsPlayerAlive(client))
			{
				set_renderchange(client);
				SetEntProp(client, Prop_Send, "m_iHealth", 5, 1);
				SDKHookEx(client, SDKHook_PostThinkPost, OnPostThinkPost);
				SetEntProp(client, Prop_Send, "m_iAddonBits", CSAddon_NONE);
				set_renderchange(client);
			}
		}
		case 18:
		{
			player_item_name[client] = "Arabian Boots";
			player_item_id[client] = rannum;
			player_b_theif[client] = GetRandomInt(500,1000);
		}
		case 19:
		{
			player_item_name[client] = "Phoenix Ring";
			player_item_id[client] = rannum;
			player_b_respawn[client] = GetRandomInt(3,6);
		}
		case 20:
		{
			player_item_name[client] = "Sorcerers ring";
			player_item_id[client] = rannum;
			player_b_respawn[client] = GetRandomInt(2,3);
		}
		case 21:
		{
			player_item_name[client] = "Chaos Orb";
			player_item_id[client] = rannum;
			player_b_explode[client] = GetRandomInt(150,275);
		}
		case 22:
		{
			player_item_name[client] = "Hell Orb";
			player_item_id[client] = rannum;
			player_b_explode[client] = GetRandomInt(200,400);
		}
		case 23:
		{
			player_item_name[client] = "Gold statue";
			player_item_id[client] = rannum;
			player_b_heal[client] = GetRandomInt(1,2);
		}
		case 24:
		{
			player_item_name[client] = "Daylight Diamond";
			player_item_id[client] = rannum;
			player_b_heal[client] = GetRandomInt(2,4);
		}
		case 25:
		{
			player_item_name[client] = "Blood Diamond";
			player_item_id[client] = rannum;
			player_b_heal[client] = GetRandomInt(4,7);
		}
		case 26:
		{
			player_item_name[client] = "Wheel of Fortune";
			player_item_id[client] = rannum;
			player_b_gamble[client] = GetRandomInt(2,3);
		}
		case 27:
		{
			player_item_name[client] = "Four leaf Clover";
			player_item_id[client] = rannum;
			player_b_gamble[client] = GetRandomInt(4,5);
		}
		case 28:
		{
			player_item_name[client] = "Amulet of the sun";
			player_item_id[client] = rannum;
			player_b_blind[client] = GetRandomInt(2,3);
		}
		case 29:
		{
			player_item_name[client] = "Sword of the sun";
			player_item_id[client] = rannum;
			player_b_blind[client] = GetRandomInt(4,5);
		}
		case 30:
		{
			player_item_name[client] = "Fireshield";
			player_item_id[client] = rannum;
			player_b_fireshield[client] = 1;
		}
		case 31:
		{
			player_item_name[client] = "Stealth Shoes";
			player_item_id[client] = rannum;
			player_b_silent[client] = 1;
		}
		case 32:
		{
			player_item_name[client] = "Meekstone";
			player_item_id[client] = rannum;
			player_b_meekstone[client] = 1;
		}
		case 33:
		{
			player_item_name[client] = "Medicine Glar";
			player_item_id[client] = rannum;
			player_b_teamheal[client] = GetRandomInt(10,20);
		}
		case 34:
		{
			player_item_name[client] = "Medicine Totem";
			player_item_id[client] = rannum;
			player_b_teamheal[client] = GetRandomInt(20,30);
		}
		case 35:
		{
			player_item_name[client] = "Iron Armor";
			player_item_id[client] = rannum;
			player_b_redirect[client] = GetRandomInt(3,6);
		}
		case 36:
		{
			player_item_name[client] = "Mitril Armor";
			player_item_id[client] = rannum;
			player_b_redirect[client] = GetRandomInt(6,11);
		}
		case 37:
		{
			player_item_name[client] = "Godly Armor";
			player_item_id[client] = rannum;
			player_b_redirect[client] = GetRandomInt(10,15);
		}
		case 38:
		{
			player_item_name[client] = "Fireball staff";
			player_item_id[client] = rannum;
			player_b_fireball[client] = GetRandomInt(50,100);
		}
		case 39:
		{
			player_item_name[client] = "Fireball scepter";
			player_item_id[client] = rannum;
			player_b_fireball[client] = GetRandomInt(100,200);
		}
		case 40:
		{
			player_item_name[client] = "Ghost Rope";
			player_item_id[client] = rannum;
			player_b_ghost[client] = GetRandomInt(3,6);
		}
		case 41:
		{
			player_item_name[client] = "Nicolas Eye";
			player_item_id[client] = rannum;
			player_b_eye[client] = 1;
		}
		case 42:
		{
			player_item_name[client] = "Knife Ruby";
			player_item_id[client] = rannum;
			player_b_blink[client] = 1;
			player_b_blink_timer[client] = GetGameTime();
		}
		case 43:
		{
			player_item_name[client] = "Lothars Edge";
			player_item_id[client] = rannum;
			player_b_windwalk[client] = GetRandomInt(4,7);
		}
		case 44:
		{
			player_item_name[client] = "Sword";
			player_item_id[client] = rannum;
			player_sword[client] = 1;
		}
		case 45:
		{
			player_item_name[client] = "Mageic Booster";
			player_item_id[client] = rannum;
			player_b_froglegs[client] = 1;
			player_b_froglegs_timer[client] = GetGameTime()-3.0;
		}
		case 46:
		{
			player_item_name[client] = "Dagon I";
			player_item_id[client] = rannum;
			player_b_dagon[client] = 1;
		}
		case 47:
		{
			player_item_name[client] = "Scout Extender";
			player_item_id[client] = rannum;
			player_b_sniper[client] = GetRandomInt(3,4);
		}
		case 48:
		{
			player_item_name[client] = "Scout Amplifier";
			player_item_id[client] = rannum;
			player_b_sniper[client] = GetRandomInt(2,3);
		}
		case 49:
		{
			player_item_name[client] = "Air booster";
			player_item_id[client] = rannum;
			player_b_jumpx[client] = 1;
		}
		case 50:
		{
			player_item_name[client] = "Iron Spikes";
			player_item_id[client] = rannum;
			player_b_smokehit[client] = 1;
		}
		case 51: //Point Booster removed
		{
			player_item_name[client] = "Iron Spikes";
			player_item_id[client] = rannum;
			player_b_smokehit[client] = 1;
		}
		case 52:
		{
			player_item_name[client] = "Fire totem";
			player_item_id[client] = rannum;
			player_b_firetotem[client] = GetRandomInt(250,400);
		}
		case 53: //Point Booster removed
		{
			player_item_name[client] = "Magic Hook";
			player_item_id[client] = rannum;
			player_b_hook[client] = 1;
		}
		case 54:
		{
			player_item_name[client] = "Darksteel Glove";
			player_item_id[client] = rannum;
			player_b_darksteel[client] = GetRandomInt(1,5);
		}
		case 55:
		{
			player_item_name[client] = "Darksteel Gaunlet";
			player_item_id[client] = rannum;
			player_b_darksteel[client] = GetRandomInt(7,9);
		}
		case 56: //Point Booster removed
		{
			player_item_name[client] = "Illusionists Cape";
			player_item_id[client] = rannum;
			player_b_illusionist[client] = 1;
		}
		case 57: //Point Booster removed
		{
			player_item_name[client] = "Techies scepter";
			player_item_id[client] = rannum;
			player_b_mine[client] = 3;
			player_b_mine_count[client] = 3;
		}
		case 58: //Point Booster removed
		{
			player_item_name[client] = "Ninja ring";
			player_item_id[client] = rannum;
			player_b_blink[client] = 1;
			player_b_blink_timer[client] = GetGameTime();
			player_b_froglegs[client] = 1;
			player_b_froglegs_timer[client] = GetGameTime()-3.0;
		}
		case 59: //Point Booster removed
		{
			player_item_name[client] = "Mage ring";
			player_item_id[client] = rannum;
			player_b_fireball[client] = GetRandomInt(50,80);
		}
		case 60: //Point Booster removed
		{
			player_item_name[client] = "Necromant ring";
			player_item_id[client] = rannum;
			player_b_respawn[client] = GetRandomInt(2,4);
			player_b_vampire[client] = GetRandomInt(3,5);
		}
		case 61: //Point Booster removed
		{
			player_item_name[client] = "Barbarian ring";
			player_item_id[client] = rannum;
			player_b_explode[client] = GetRandomInt(120,330);
		}
		case 62: //Point Booster removed
		{
			player_item_name[client] = "Paladin ring";
			player_item_id[client] = rannum;
			player_b_redirect[client] = GetRandomInt(7,17);
			player_b_blind[client] = GetRandomInt(3,4);
		}
		case 63: //Point Booster removed
		{
			player_item_name[client] = "Monk ring";
			player_item_id[client] = rannum;
			player_b_grenade[client] = GetRandomInt(1,4);
			player_b_heal[client] = GetRandomInt(4,7);
		}
		case 64: //Point Booster removed
		{
			player_item_name[client] = "Assassin ring";
			player_item_id[client] = rannum;
			player_b_jumpx[client] = 1;
			player_b_silent[client] = 1;
		}
		case 65: //Point Booster removed
		{
			player_item_name[client] = "Flashbang necklace";
			player_item_id[client] = rannum;
			wear_sun[client] = 1;
		}
		case 66: //Point Booster removed
		{
			player_item_name[client] = "Chameleon";
			player_item_id[client] = rannum;
			player_b_chameleon[client] = 1;
			if(IsPlayerAlive(client))
			{
				new team=GetClientTeam(client);
				new searchteam=(team==2)?3:2;
				SetEntityModel(client,(searchteam==2)?"models/player/tm_phoenix_varianta.mdl":"models/player/ctm_sas_varianta.mdl");
			}
		}
		case 67: //Point Booster removed
		{
			player_item_name[client] = "Strong Armor";
			player_item_id[client] = rannum;
			player_ultra_armor[client]=GetRandomInt(3,6);
			player_ultra_armor_left[client]=player_ultra_armor[client];
		}
		case 68: //Point Booster removed
		{
			player_item_name[client] = "Ultra Armor";
			player_item_id[client] = rannum;
			player_ultra_armor[client]=GetRandomInt(7,11);
			player_ultra_armor_left[client]=player_ultra_armor[client];
		}
	}
	BoostRing(client);
	iteminfo(client,1,0);
	decl String:hinttext[200], String:itemname[200];
	Format(hinttext, sizeof(hinttext), "%T", "New Item", client);
	if(player_item_id[client] == 100)
	{
		Format(itemname, sizeof(itemname), "%s", player_item_name[client]);
	}
	else
	{
		Format(itemname, sizeof(itemname), "%T", player_item_name[client], client);
	}
	Format(hinttext, sizeof(hinttext), "%s%s", hinttext, itemname);
	PrintHintText(client, hinttext);
}

public iteminfo(client,type,sound) //type 1 award, 2 say
{
	if(player_item_id[client] == 0)
	{
		Diablo_ChatMessage(client,"%T","You don`t have item",client);
		return;
	}
	new String:itemvalue[100];
	
	if (player_item_id[client] <= 10)
		itemvalue = "Common";
	else if (player_item_id[client] <= 30) 
		itemvalue = "Uncommon";
	else 
		itemvalue = "Rare";
	
	if (player_item_id[client] > 42) itemvalue = "Unique";
	
	new String:itemEffect[2048], String:itemEffect2[2048];
	new String:itemValueLang[1024];
	new String:itemnameLANG[200];
	new String:space_or_newline[10];
	
	if(type == 2) //chat ii
	{
		if(ii_chat[client] == 1)
		{
			Format(space_or_newline, sizeof(space_or_newline), "\n");
		}
		else
		{
			Format(space_or_newline, sizeof(space_or_newline), " ");
		}
	}
	if(type == 1) //award ii
	{
		if(ii_award[client] == 1)
		{
			Format(space_or_newline, sizeof(space_or_newline), "\n");
		}
		else
		{
			Format(space_or_newline, sizeof(space_or_newline), " ");
		}
	}
	
	if(player_item_id[client] == 100)
	{
		Format(itemnameLANG, sizeof(itemnameLANG), "%s", player_item_name[client]);
	}
	else
	{
		Format(itemnameLANG, sizeof(itemnameLANG), "%T", player_item_name[client], client);
	}
	Format(itemValueLang, sizeof(itemValueLang), "%T", itemvalue, client);
	
	if(player_item_id[client] == 17)
	{
		Format(itemEffect, sizeof(itemEffect), "%T", "5 health only and nearly invisible.", client, space_or_newline);
	}
	else
	{
		if (player_b_damage[client] > 0) 
		{
			Format(itemEffect, sizeof(itemEffect), "%T", "+{int} extra damage", client, player_b_damage[client]);
		}
		if (player_b_vampire[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "+{int} HP for every enemy hit", client, player_b_vampire[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_money[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "+{int} money bonus each round start", client, space_or_newline, player_b_money[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_gravity[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "+{int}% gravity. Press E", client, space_or_newline, player_b_gravity[client], space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}	
		if (player_b_inv[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You visibility is reduced from 255 to {int}", client, space_or_newline, player_b_inv[client]);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_grenade[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You have a 1/{int} chance that your HE will kill instantly", client, space_or_newline, player_b_grenade[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_theif[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "1/5 chance to steal ${int} each time you hit", client, space_or_newline, player_b_theif[client], space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_respawn[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "1/{int} chance to respawn upon death", client, space_or_newline, player_b_respawn[client]);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_explode[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "When you die you will explode in a radius of {int} dealing 75 damage to all around you - intelligence boost this item range", client, space_or_newline, player_b_explode[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_heal[client] > 0) 
		{
			new healtxt = player_b_heal[client]*5;
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You will gain {int} hit points every second", client, player_b_heal[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline, healtxt);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_gamble[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You will gain a random skill each round start vararity is 1/{int}", client, space_or_newline, player_b_gamble[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_fireshield[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to damage,slow and blind all enemies around you, but it will reduce 10 HP every second. You can not be killed by chaos orb, hell orb or firerope.", client, space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_silent[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Your run becomes silent", client);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_meekstone[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to lay a fake bomb. Press E again to explode it.", client, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_teamheal[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to activate a shield on a firendly player. All shield damage is reflected. You die if you take damage. You will restore {int}HP to the player when you turn on the shield.", client, space_or_newline, player_b_teamheal[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_redirect[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You gain a damage reduction of {int} hitpoints", client, space_or_newline, player_b_redirect[client]);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_fireball[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You can cast a fireball pressing E - Intelligence boost this item. It will explode in a radius of {int}", client, space_or_newline, player_b_fireball[client], space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_ghost[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to walk through walls in a period of {int} seconds", client, space_or_newline, player_b_ghost[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_eye[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to place a magic eye (only one placement allowed) and E again to use it or stop", client, space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_blink[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You can blink by using the alternative attack with your knife. Intelligence boost the blink distance", client, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_windwalk[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to become invisible unable to attack and with incresed speed for {int} seconds", client, space_or_newline, player_b_windwalk[client], space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_sword[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Increases knife damage for 35 HP. Item upgrade randomly gives extra abilities to the item", client, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_froglegs[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Hold down JUMP+DUCK for longjump with 3 seconds delay", client, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_dagon[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to fire a lightning againt nearest opponent with {int} damage. You can upgrade this item to next level.", client, space_or_newline, player_b_dagon[client]*20, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_sniper[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You have 1/{int} chance to instant kill with scout (SSG 08)", client, space_or_newline, player_b_sniper[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_smokehit[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Your Smoke grenades will kill instantly if they hit the opponent", client, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_firetotem[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to put down a firetotem which explode after 7s. It ignites anyone in a {int} radius of the totem", client, space_or_newline, player_b_firetotem[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_hook[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to throw out a hook with a range of 1000 which pulls anyone it meets towards you. Intelligence makes the hook pull faster", client, space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_darksteel[client] > 0) 
		{
			new ddam = RoundToFloor(15+player_strength[client]*2*player_b_darksteel[client]/10.0)*3;
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You get {int} damage bonus when you hit the enemy from behind", client, space_or_newline, ddam, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_illusionist[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to become invisible to all players for 7s. Any damage taken while invisible will kill you", client, space_or_newline, space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_mine[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Press E to lay down an nearly invisible mine. Each mine explode for 50hp+intelligece damage on touch. {int} Mines each round is allowed", client, space_or_newline, player_b_mine[client], space_or_newline, space_or_newline, space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (wear_sun[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "Flashbang immunity", client);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_chameleon[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You will get enemy skin", client);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_ultra_armor[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "+{int} Barbarian armor every round. 1/{int} chance use armor.", client, space_or_newline, player_ultra_armor[client], player_ultra_armor[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_blind[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You have a 1/{int} chance to blind the enemy when you hit him.", client, space_or_newline, player_b_blind[client], space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
		if (player_b_jumpx[client] > 0) 
		{
			Format(itemEffect2, sizeof(itemEffect2), "%T", "You can jump {int} extra times in the air while pressing the jump button.", client, space_or_newline, player_b_jumpx[client], space_or_newline, space_or_newline);
			Format(itemEffect, sizeof(itemEffect), "%s%s%s", itemEffect, space_or_newline, itemEffect2);
		}
	}
	if(type == 2) //chat ii
	{
		if(ii_chat[client] == 1)
		{
			menu_ii(client, itemEffect, itemValueLang);
		}
		else
		{
			Diablo_ChatMessage(client,"%T","{name}{value}{dur} - {effect)",client,itemnameLANG,itemValueLang,item_durability[client],itemEffect);
		}
	}
	if(type == 1) //award ii
	{
		if(ii_award[client] == 1)
		{
			menu_ii(client, itemEffect, itemValueLang);
		}
		else
		{
			Diablo_ChatMessage(client,"%T","{name}{value}{dur} - {effect)",client,itemnameLANG,itemvalue,item_durability[client],itemEffect);
		}
	}
	if(sound == 1)
	{
		EmitSoundToAll(identifysnd,client);
	}
}

public upgrade_item(client)
{
	if(item_durability[client]>0) item_durability[client] += GetRandomInt(-50,50);
	if(item_durability[client]<1)
	{
		dropitem(client);
	}
	else
	{
		if(player_b_jumpx[client]>0) player_b_jumpx[client] += GetRandomInt(0,1);
		
		if(player_b_vampire[client]>0)
		{
			if(player_b_vampire[client]>20) player_b_vampire[client] += GetRandomInt(-1,2);
			else if(player_b_vampire[client]>10) player_b_vampire[client] += GetRandomInt(0,2);
			else player_b_vampire[client]+= GetRandomInt(1,3);
		}
		if(player_b_damage[client]>0) player_b_damage[client] += GetRandomInt(0,3);
		if(player_b_money[client]!=0) player_b_money[client]+= GetRandomInt(-100,300);
		if(player_b_gravity[client]>0)
		{
			if(player_b_gravity[client]<3) player_b_gravity[client]+=GetRandomInt(0,2);
			else if(player_b_gravity[client]<5) player_b_gravity[client]+=GetRandomInt(1,3);
			else if(player_b_gravity[client]<8) player_b_gravity[client]+=GetRandomInt(-1,3);
			else if(player_b_gravity[client]<10) player_b_gravity[client]+=GetRandomInt(0,1);
		}
		if(player_b_inv[client]>0)
		{
			if(player_b_inv[client]>200) player_b_inv[client]-=GetRandomInt(0,50);
			else if(player_b_inv[client]>100) player_b_inv[client]-=GetRandomInt(-25,50);
			else if(player_b_inv[client]>50) player_b_inv[client]-=GetRandomInt(-10,20);
			else if(player_b_inv[client]>25) player_b_inv[client]-=GetRandomInt(-10,10);
		}
		if(player_b_grenade[client]>0)
		{
			if(player_b_grenade[client]>4) player_b_grenade[client]-=GetRandomInt(0,2);
			else if(player_b_grenade[client]>2) player_b_grenade[client]-=GetRandomInt(0,1);
			else if(player_b_grenade[client]==2) player_b_grenade[client]-=GetRandomInt(-1,1);
		}
		if(player_b_reduceH[client]>0) player_b_reduceH[client]-=GetRandomInt(0,player_b_reduceH[client]);
		if(player_b_theif[client]>0) player_b_theif[client] += GetRandomInt(0,250);
		if(player_b_respawn[client]>0)
		{
			if(player_b_respawn[client]>2) player_b_respawn[client]-=GetRandomInt(0,1);
			else if(player_b_respawn[client]>1) player_b_respawn[client]-=GetRandomInt(-1,1);
		}
		if(player_b_explode[client]>0)player_b_explode[client] += GetRandomInt(0,50);
		if(player_b_heal[client]>0)
		{
			if(player_b_heal[client]>20) player_b_heal[client]+= GetRandomInt(-1,3);
			else if(player_b_heal[client]>10) player_b_heal[client]+= GetRandomInt(0,4);
			else player_b_heal[client]+= GetRandomInt(2,6);
		}
		if(player_b_blind[client]>0)
		{
			if(player_b_blind[client]>5) player_b_blind[client]-= GetRandomInt(0,2);
			else if(player_b_blind[client]>1) player_b_blind[client]-= GetRandomInt(0,1);
		}

		if(player_b_teamheal[client]>0) player_b_teamheal[client] += GetRandomInt(0,5);

		if(player_b_redirect[client]>0) player_b_redirect[client]+= GetRandomInt(0,2);
		if(player_b_fireball[client]>0) player_b_fireball[client]+= GetRandomInt(0,33);
		if(player_b_ghost[client]>0) player_b_ghost[client]+= GetRandomInt(0,1);
		if(player_b_windwalk[client]>0) player_b_windwalk[client] += GetRandomInt(0,1);

		if(player_b_dagon[client]>0) player_b_dagon[client] += GetRandomInt(0,1);
		if(player_b_dagon[client]>0)
		{
			if(player_b_dagon[client]>10) player_b_dagon[client]=10;
			switch(player_b_dagon[client])
			{
				case 1: player_item_name[client] = "Dagon I";
				case 2: player_item_name[client] = "Dagon II";
				case 3: player_item_name[client] = "Dagon III";
				case 4: player_item_name[client] = "Dagon IV";
				case 5: player_item_name[client] = "Dagon V";
				case 6: player_item_name[client] = "Dagon VI";
				case 7: player_item_name[client] = "Dagon VII";
				case 8: player_item_name[client] = "Dagon VIII";
				case 9: player_item_name[client] = "Dagon IX";
				case 10: player_item_name[client] = "Dagon X";
			}
		}
		if(player_b_sniper[client]>0)
		{
			if(player_b_sniper[client]>5) player_b_sniper[client]-=GetRandomInt(0,2);
			else if(player_b_sniper[client]>2) player_b_sniper[client]-=GetRandomInt(0,1);
			else if(player_b_sniper[client]>1) player_b_sniper[client]-=GetRandomInt(-1,1);
		}

		if(player_b_extrastats[client]>0) player_b_extrastats[client] += GetRandomInt(0,2);
		if(player_b_firetotem[client]>0) player_b_firetotem[client] += GetRandomInt(0,50);

		if(player_b_darksteel[client]>0) player_b_darksteel[client] += GetRandomInt(0,2);
		if(player_b_mine[client]>0) player_b_mine[client] += GetRandomInt(0,1);
		if(player_sword[client]>0)
		{
			if(player_b_jumpx[client]==0 && GetRandomInt(0,10)==10) player_b_jumpx[client]=1;
			if(player_b_vampire[client]==0 && GetRandomInt(0,10)==10) player_b_vampire[client]=1;
			if(player_b_gravity[client]==0 && GetRandomInt(0,10)==10) player_b_gravity[client]=1;
			if(player_b_respawn[client]==0 && GetRandomInt(0,10)==5) player_b_respawn[client]=15;
			else if(player_b_respawn[client]>2 && GetRandomInt(0,10)==5) player_b_respawn[client]+=GetRandomInt(0,1);
			if(player_b_ghost[client]==0 && GetRandomInt(0,10)==10) player_b_ghost[client]=1;
			if(player_b_darksteel[client]==0 && GetRandomInt(0,10)==10) player_b_darksteel[client]=1;
		}
		if(player_ultra_armor[client]>0) player_ultra_armor[client]++;
		EmitSoundToAll(repairsnd,client);
	}
}

public item_take_damage(client,damage)
{
	new itemdamage = 10;
	
	if (player_item_id[client] > 0 && item_durability[client] >= 0 && damage > 5)
	{
		//Make item take damage
		if (item_durability[client] - itemdamage <= 0)
		{
			item_durability[client]-=itemdamage;
			dropitem(client);
		}
		else
		{
			item_durability[client]-=itemdamage;
		}
		
	}
}

public menu_ii(client, String:text[], String:itemvalue[])
{
	new Handle:menu = CreateMenu(menu_iiHandler, MENU_ACTIONS_ALL);
	if(player_item_id[client] == 100)
	{
		SetMenuTitle(menu, "%s", player_item_name[client], client);
	}
	else
	{
		SetMenuTitle(menu, "%T", player_item_name[client], client);
	}
	
	new length = strlen(text);
	decl String:BackupDescr[2][1024], String:LongDescr[10][1024], String:Durab_t[50];
	Format(Durab_t, sizeof(Durab_t), "%T", "Durab", client);
	Format(text, 1024, "\n%s.\n%s %d\n\n%s", itemvalue, Durab_t, item_durability[client], text);
	
	AddMenuItem(menu, "2_skills", text);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public menu_iiHandler(Handle:menu, MenuAction:action, client, param2)
{
	return 0;
}

public award_unique_item(client)
{
	new String:Unique_names_Suffix[10][100];
	new String:Unique_names_Prefix[10][100];
	
	Unique_names_Suffix[1] = "Saintly amulet ";
	Unique_names_Suffix[2] = "Holy sword ";
	Unique_names_Suffix[3] = "Small staff ";
	Unique_names_Suffix[4] = "Bright rope ";
	Unique_names_Suffix[5] = "Shiny scepter ";
	
	Unique_names_Prefix[1] = "of the stars";
	Unique_names_Prefix[2] = "of power";
	Unique_names_Prefix[3] = "of zod";
	Unique_names_Prefix[4] = "of life";
	Unique_names_Prefix[5] = "of the sun";
	
	//Generate the items name
	
	new roll_1 = GetRandomInt(1,4);
	new roll_2 = GetRandomInt(1,4);
	
	new String:Unique_name[100];
	Format(Unique_name, sizeof(Unique_name), "%s%s", Unique_names_Suffix[roll_1],Unique_names_Prefix[roll_2]);
	
	player_item_name[client] = Unique_name;
	player_item_id[client] = 100;		
	
	//Generate and apply the stats
	
	if (roll_1 == 1) player_b_damage[client] = GetRandomInt(1,5);
	if (roll_1 == 2) player_b_vampire[client] = GetRandomInt(1,5);
	if (roll_1 == 3) player_b_money[client] = GetRandomInt(2500,5000);
	if (roll_1 == 4) player_b_reduceH[client] = GetRandomInt(20,50);
	if (roll_1 == 5) player_b_blind[client] = GetRandomInt(3,5);
	
	
	
	if (roll_2 == 1) player_b_grenade[client] = GetRandomInt(1,4);
	if (roll_2 == 2) player_b_respawn[client] = GetRandomInt(2,4);
	if (roll_2 == 3) player_b_explode[client] = GetRandomInt(150,400);
	if (roll_2 == 4) player_b_redirect[client] = GetRandomInt(5,10);
	if (roll_2 == 5) player_b_heal[client] = GetRandomInt(1,15);
	
	item_durability[client] = 350;
}

public menu_raceinfo(client, racenum)
{
	new Handle:menu = CreateMenu(menu_raceinfoHandler, MENU_ACTIONS_ALL);
	
	new String:racename[64], String:racedescr[1024], String:text[1024];
	
	switch(racenum)
	{
		case 1:
		{
			Format(racename, sizeof(racename), "%T", "RACE1", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE1_DESCR", client);
		}
		case 2:
		{
			Format(racename, sizeof(racename), "%T", "RACE2", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE2_DESCR", client);
		}
		case 3:
		{
			Format(racename, sizeof(racename), "%T", "RACE3", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE3_DESCR", client);
		}
		case 4:
		{
			Format(racename, sizeof(racename), "%T", "RACE4", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE4_DESCR", client);
		}
		case 5:
		{
			Format(racename, sizeof(racename), "%T", "RACE5", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE5_DESCR", client);
		}
		case 6:
		{
			Format(racename, sizeof(racename), "%T", "RACE6", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE6_DESCR", client);
		}
		case 7:
		{
			Format(racename, sizeof(racename), "%T", "RACE7", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE7_DESCR", client);
		}
		case 8:
		{
			Format(racename, sizeof(racename), "%T", "RACE8", client);
			Format(racedescr, sizeof(racedescr), "%T", "RACE8_DESCR", client);
		}
	}
	SetMenuTitle(menu, racename);
	
	Format(text, 1023, "%s", racedescr);
	
	AddMenuItem(menu, "raceinfo", text);
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public menu_raceinfoHandler(Handle:menu, MenuAction:action, client, param2)
{
	switch(action)
	{
		case MenuAction_Cancel:
		{
			switch(param2)
			{
				case MenuCancel_Exit:
				{
					if(player_frommenu[client]) ShowClassHelp(client);
				}
			}
		}
	}
	return 0;
}

public SubtractStats(client,amount)
{
	player_strength[client]-=amount;
	player_dextery[client]-=amount;
	player_agility[client]-=amount;
	player_intelligence[client]-=amount;
}

public SubtractRing(client)
{
	switch(player_ring[client])
	{
		case 1: player_intelligence[client]-=5;
		case 2: player_strength[client]-=5;
		case 3: player_agility[client]-=5;
	}
}

public BoostRing(client)
{
	switch(player_ring[client])
	{
		case 1: player_intelligence[client]+=5;
		case 2: player_strength[client]+=5;
		case 3: player_agility[client]+=5;
	}
}

public InitPlayer(client)
{
	g_iDBPlayerUniqueID[client] = 0;

	for(new iRace=1;iRace<MAXRACES+1;iRace++)
	{
		player_class_xp[client][iRace] = 0;
		player_class_lvl[client][iRace] = 1;
	}
	loading_race[client] = 0;
	
	ResetPlayer(client);
}

public ResetPlayer(client)
{
	player_xp[client] = 0;
	player_lvl[client] = 1;
	player_class[client] = 0;
	player_strength[client] = 0;
	player_agility[client] = 0;
	player_intelligence[client] = 0;
	player_dextery[client] = 0;
	player_point[client] = 0;
	loading_xp[client] = 0;
	player_item_id[client] = 0;
	item_durability[client] = 0;
	moneyshield[client] = 0;
	c4state[client] = 0;
	earthstomp[client] = 0;
	
	longjumps[client] = 0;
	monk_shield[client] = 0;
	player_knife[client] = 0;
	g_haskit[client] = 0;
	gravitytimer[client] = GetGameTime();
	player_b_fireball_timer[client] = GetGameTime();
	player_race_fireball_timer[client] = GetGameTime();
	player_b_froglegs_timer[client] = GetGameTime();
	player_b_teamheal_using[client] = -1;
	player_b_teamheal_shielded[client] = -1;
	player_b_eye_created[client] = 0;
	player_b_eye_viewing[client] = 0;
	player_lasthint[client] = 0;
	
	if(flashlight_enabled[client])
	{
		flashlight_enabled[client] = true;
		if(IsPlayerAlive(client))
		{
			SetEntProp(client, Prop_Send, "m_fEffects", GetEntProp(client, Prop_Send, "m_fEffects") ^ 4);
		}
	}
	
	set_hideradar(client, 0);
	reset_item_skills(client);
}

public SQLTxn_LogFailure(Handle:db, any:data, numQueries, const String:error[], failIndex, any:queryData[])
{
	LogError("Error executing query %d of %d queries: %s", failIndex, numQueries, error);
}

stock bool:SQL_LockedFastQuery(Handle:database, const String:query[], len=-1)
{
	SQL_LockDatabase(database);
	new bool:bSuccess = SQL_FastQuery(database, query, len);
	SQL_UnlockDatabase(database);
	return bSuccess;
}

public RoundStartEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	round_end = 0;
	SaveAllPlayers();
	new currency, newmoney;
	
	for(new player=1;player<=MaxClients;player++)
	{
		if(IsClientInGame(player) && !IsFakeClient(player) && IsClientAuthorized(player))
		{
			if(player_spend_1_time[player])
			{
				if(GetCurrency(player) != 800)
				{
					currency = GetCurrency(player);
					newmoney = currency - player_lastmoney[player];
					newmoney = currency - player_spendmoney[player];
					SetCurrency(player, newmoney);
				}
				player_spendmoney[player] = 0;
				player_spend_1_time[player] = 0;
			}
			if(player_point[player]!=0) 
			{		
				skilltree(player);
			}
			if(ValidPlayer(player,true) && (player_class[player] != 0))
			{
				ShowXP(player);
				ShowItemNewbie(player);
				if(bow[player] == 1)
				{
					new weapon1 = GetPlayerWeaponSlot(player, 0);
					new weapon2 = GetPlayerWeaponSlot(player, 1);
					FPVMI_RemoveViewModelToClient(player, "weapon_knife");
					FPVMI_RemoveWorldModelToClient(player, "weapon_knife");
					bow[player] = 0;
					if(weapon1 != -1)
					{
						ClientCommand(player, "slot1");  
					}
					else
					{
						ClientCommand(player, "slot2");
					}
				}
				add_money_bonus(player);
				add_redhealth_bonus(player);
				add_bonus_gamble(player);
				used_item[player] = false;
				player_b_illusionist_activated[player] = false;
				casting[player]=0;
				SpawnWeaponRegister(player);
			}
		}
	}
}

/*
public FreezeEndEvent(Handle:event,const String:name[],bool:dontBroadcast)
{

}*/

public OnClientConnected(client)
{
	player_hideradar_ent[client] = INVALID_ENT_REFERENCE;
	player_spend_1_time[client] = 0;
	InitPlayer(client);
}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_OnTakeDamagePost, Hook_OnTakeDamagePost);
	SDKHook(client, SDKHook_TraceAttack, TraceAttack);
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKHook(client, SDKHook_WeaponEquip, OnPostWeaponEquip);
	SDKHook(client, SDKHook_WeaponSwitch, OnWeaponSwitch);
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKHook(client, SDKHook_SetTransmit, Hook_SetTransmit);
	SDKHook(client, SDKHook_GroundEntChangedPost, Event_GroundEntChanged);
	if(!IsFakeClient(client)) AddPlayer(client);
	if(!IsFakeClient(client))        SendConVarValue(client, sv_footsteps, "0"); //Assassin footsteps
	ii_chat[client] = 1;
	ii_award[client] = 1;
	hint_disable[client] = 0;
	player_b_gamble[client] = 0;
	player_firstconnect[client] = 1;
	player_newrace[client] = 0;
}

public Action:TraceAttack(victim, &attacker, &inflictor, &Float:damage, &damagetype, &ammotype, hitbox, hitgroup) 
{
	if(attacker < MAXPLAYERS)
	{
		if(player_b_smokehit[attacker] == 1 && (GetClientTeam(attacker) != GetClientTeam(victim)))
		{
			decl String:sWeapon[32];
			GetEdictClassname(inflictor, sWeapon, sizeof(sWeapon));
			
			if(StrEqual(sWeapon, "smokegrenade_projectile", false))
			{
				damage = 300.0;
				return Plugin_Changed;
			}
		}
		
		if(attacker != 0)
		{
			if(MissTimers[attacker] != INVALID_HANDLE) 
			{ 
				KillTimer(MissTimers[attacker]); 
				MissTimers[attacker] = INVALID_HANDLE; 
			}
		}
		
		new intdamage = RoundToFloor(damage);
		new newdamage = intdamage;
		new newdamagestatic = intdamage;
		new nohint;
		if(GetClientTeam(attacker) != GetClientTeam(victim))
		{
			add_bonus_necromancer(attacker,victim);
			add_damage_bonus(attacker,victim,intdamage);
			add_newbie_bonus(attacker,victim,intdamage);
			add_vampire_bonus(intdamage,attacker);
			add_theif_bonus(attacker,victim);
			add_bonus_blind(attacker,victim);
			item_take_damage(victim,intdamage);
		}
		new String:targetname[60];
		GetEntPropString(inflictor, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
		if(StrEqual(targetname, "fireball", false))
		{	
			if(victim == attacker)
			{
				return Plugin_Handled;
			}
			else
			{
				damage = 55.0+player_intelligence[attacker];
				if(player_dextery[victim] > 0)
				{
					new Float:newdamagefireball;
					newdamagefireball = damage-player_dextery[victim];
					if(newdamagefireball > 0)
					{
						damage = newdamagefireball;
					}
				}
				
				return Plugin_Changed;
			}
		}
		if(StrEqual(targetname, "huntgren", false))
		{
			if(victim == attacker)
			{
				return Plugin_Handled;
			}
		}
		if(StrEqual(targetname, "meekstone_exp", false))
		{	
			return Plugin_Handled;
		}
		if(StrEqual(targetname, "mine_exp", false))
		{	
			return Plugin_Handled;
		}
		
		if(StrEqual(targetname, "explode_bonus", false))
		{	
			if((victim == attacker) || (player_b_fireshield[victim] == 1))
			{
				return Plugin_Handled;
			}
		}
		if(GetClientTeam(attacker) != GetClientTeam(victim))
		{
			if(golden_bullet[attacker] > 0)
			{
				hitgroup = 1;
				damagetype = (1 << 30); //Headshot
				new iWeapon = GetEntPropEnt(attacker, Prop_Send, "m_hActiveWeapon");
				decl String:sWeapon[32];
				GetEdictClassname(iWeapon, sWeapon, sizeof(sWeapon));
				new isarmored;
				if(GetEntProp(victim, Prop_Send, "m_ArmorValue") > 0) isarmored = 1;
				new headdmg = GetHeadshotDMG(sWeapon, isarmored, iWeapon);
				if(headdmg>0) newdamage = headdmg;
				golden_bullet[attacker]--;
			}
			
			//Paladin block every 7 bullet
			if((player_class[victim]==3) && (GetRandomInt(0,7)==1))
			{
				newdamage = 0;
				FlashScreen(victim,RGBA_COLOR_GREEN,1.0,1.0,FFADE_OFF);	
			}
			
			if(ultra_armor[victim]>0)
			{
				newdamage = 0;
				ultra_armor[victim]--;
			}
			
			if(player_ultra_armor_left[victim]>0)
			{
				if(GetRandomInt(0,player_ultra_armor[victim]) == 1)
				{
					player_ultra_armor_left[victim]--;
					newdamage = 0;
					FlashScreen(victim,RGBA_COLOR_GREEN,1.0,1.0,FFADE_OFF);
					nohint = 1;
				}
			}
			
			if(player_agility[victim]>0 && player_totallvl[attacker] > 14)
			{
				newdamage = newdamage-RoundToFloor(newdamage*player_agility[victim]*0.005);
			}
			if(moneyshield[victim] == 1)
			{
				newdamage = newdamage-RoundToFloor(newdamage/2.0);
			}
			if(player_b_redirect[victim])
			{
				newdamage = newdamage - player_b_redirect[victim];
			}
			if(player_sword[attacker] && on_knife[attacker])
			{
				newdamage = newdamage + 35;
			}
			if(player_b_darksteel[attacker])
			{
				if((hitgroup == 2) || (hitgroup == 3))
				{
					new target = GetClientAimTarget(attacker, false);
					if(target > 0)
					{
						if(IsBackDamage(attacker, target))
						{
							new dam = RoundToFloor(15+player_strength[attacker]*2*player_b_darksteel[attacker]/10.0);
							newdamage = dam + 35;
						}
					}
				}
			}
			if(newdamage!=intdamage)
			{
				damage = newdamage*1.0;
				if(newdamage < 0) damage =0;
				if(nohint == 0)
				{
					PrintHintText(attacker, "+%d", newdamage);
				}
				return Plugin_Changed;
			}
		}
	}

	return Plugin_Continue;
}

public add_bonus_blind(attacker,victim)
{
	if (player_b_blind[attacker] > 0)
	{
		new roll = GetRandomInt(1,player_b_blind[attacker]);
		if (roll == 1)
		{
			FlashScreen(victim,RGBA_COLOR_BLIND,1.0,1.0,FFADE_OFF);
		}
	}
}

public add_vampire_bonus(damage,attacker)
{
	if (player_b_vampire[attacker] > 0)
	{
		add_health(attacker,player_b_vampire[attacker]);
	}
}

public add_respawn_bonus(client)
{
	if (player_b_respawn[client] > 0)
	{
		new roll = GetRandomInt(1,player_b_respawn[client]);
		if (roll == 1)
		{
			new current_plr = CurrentLivePlayers();
			if(current_plr > 2)
			{
				CreateTimer(1.0, respawn_player, client);
			}
			else
			{
				Diablo_ChatMessage(client,"%T","More than 2 a live players required for a succesfull respawn",client);
			}
		}
	}
}

public add_bonus_explode(client)
{
	if(player_b_explode[client] > 0)
	{
		new String:radiusstr[4];
		Format(radiusstr, sizeof(radiusstr), "%d", player_b_explode[client]);
		new explode = CreateEntityByName("env_explosion");
		DispatchKeyValue(explode, "iMagnitude", "75");
		DispatchKeyValue(explode, "targetname", "explode_bonus"); 
		DispatchKeyValue(explode, "iRadiusOverride", radiusstr);
		SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", client);
		
		DispatchSpawn(explode);
		new Float:position[3];
		GetEntPropVector(client, Prop_Send, "m_vecOrigin", position);
		TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
		CreateLevelupParticle(client, "explosion_hegrenade_interior_fallback");
		EmitSoundToAll("weapons/hegrenade/explode5.wav", explode);
		AcceptEntityInput(explode, "Explode");
		AcceptEntityInput(explode, "kill");
	}
}

public Action:respawn_player(Handle:timer, any:client)
{
	CS_RespawnPlayer(client);
	AddCurrency(client,4000);
}
public add_theif_bonus(attacker,victim)
{
	if (player_b_theif[attacker] > 0)
	{
		if(GetCurrency(victim) != 0)
		{
			new roll1 = GetRandomInt(1,5);
			if (roll1 == 1)
			{
				if(GetCurrency(victim) > player_b_theif[attacker])
				{
					AddCurrency(victim, -player_b_theif[attacker]);
					AddCurrency(attacker, player_b_theif[attacker]);
				}
				else
				{
					SetCurrency(victim,0);
					AddCurrency(attacker, GetCurrency(victim));
				}
			}
		}
	}
}

public add_money_bonus(client)
{
	if (player_b_money[client] > 0)
	{
		if (GetCurrency(client) < 16000 - player_b_money[client]+player_intelligence[client]*50) 
		{
			AddCurrency(client,player_b_money[client]+player_intelligence[client]*50);
		} 
		else 
		{
			SetCurrency(client,16000);
		}
	}
}

public add_bonus_gamble(client)
{
	if (player_b_gamble[client] > 0 && IsPlayerAlive(client))
	{
		new durba=item_durability[client];
		decl String:hinttext[200];
		reset_item_skills(client);
		item_durability[client]=durba;
		new roll = GetRandomInt(1,player_b_gamble[client]);
		if (roll == 1)
		{
			Format(hinttext, sizeof(hinttext), "%T", "Round bonus: +5 damage", client);
			PrintHintText(client, hinttext);
			player_b_damage[client] = 5;
			Diablo_ChatMessage(client,"%T","Round bonus: +5 damage",client);
		}
		if (roll == 2)
		{
			Format(hinttext, sizeof(hinttext), "%T", "Round bonus: +5 Gravity bonus", client);
			PrintHintText(client, hinttext);
			player_b_gravity[client] = 5;
			Diablo_ChatMessage(client,"%T","Round bonus: +5 Gravity bonus",client);
		}
		if (roll == 3)
		{
			Format(hinttext, sizeof(hinttext), "%T", "Round bonus: +5 vampyric damage", client);
			PrintHintText(client, hinttext);
			player_b_vampire[client] = 5;
			Diablo_ChatMessage(client,"%T","Round bonus: +5 Gravity bonus",client);
		}
		if (roll == 4)
		{
			Format(hinttext, sizeof(hinttext), "%T", "Round bonus: +10 Heal bonus every 5 seconds", client);
			PrintHintText(client, hinttext);
			player_b_heal[client] = 10;
			Diablo_ChatMessage(client,"%T","Round bonus: +10 Heal bonus every 5 seconds",client);
		}
		if (roll == 5)
		{
			Format(hinttext, sizeof(hinttext), "%T", "Round bonus: 1/3 chance to instant HE kill", client);
			PrintHintText(client, hinttext);
			player_b_grenade[client] = 3;
			Diablo_ChatMessage(client,"%T","Round bonus: 1/3 chance to instant HE kill",client);
		}
	}
}

public add_redhealth_bonus(client)
{
	if (player_b_reduceH[client] > 0)
	{
		add_health(client,-player_b_reduceH[client]);
	}
	if(player_item_id[client]==17)	//stalker ring
	{
		SetEntProp(client, Prop_Send, "m_iHealth", 5, 1);
	}
}

public add_bonus_stomp(client)
{
	new Float:Clientpos[3];
	GetClientAbsOrigin(client,Clientpos);
	new curheight = RoundToFloor(Clientpos[2]);
	new dam = earthstomp[client]-curheight;
	
	earthstomp[client] = 0;
	
	if(dam > 10)
	{
		ShakeScreen(client,1.0,50.0,40.0);
		new AttackerTeam = GetClientTeam(client);
		new Float:VictimPos[3];
		new distance = 230+player_strength[client]*2;
		
		for(new victim=1;victim<=MaxClients;victim++)
        {
			if(ValidPlayer(victim,true))
			{
				GetClientAbsOrigin(victim,VictimPos);
				if(GetVectorDistance(Clientpos,VictimPos)<distance)
				{
					if(GetClientTeam(victim)!=AttackerTeam)
					{
						if(GetEntityFlags(victim) & FL_ONGROUND)
						{
							ShakeScreen(victim,1.0,50.0,40.0);
							DealCustomDamage(victim, client, dam, "wings");
							SetEntDataFloat(victim,m_OffsetSpeed,0.2,true);
							player_slowed[victim]++;
						}
					}
				}
			}
		}
		EmitSoundToAll(wingssnd,client);
	}
	else
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "The drop height is too small", client);
		PrintHintText(client, hinttext);
	}
}

public add_bonus_necromancer(attacker,victim)
{
	if (player_class[attacker] == 5)
	{
		new add_hp = 5 + RoundToFloor(player_intelligence[attacker]*0.3);
		add_health(attacker,add_hp);
	}

}

public add_barbarian_bonus(attacker)
{
	if (player_class[attacker] == 6)
	{	
		//add_health(attacker,20);
		//SetEntProp(attacker, Prop_Send, "m_bHasHelmet", 1);
		SetEntProp(attacker, Prop_Data, "m_ArmorValue", 100, 1);
		RefillAmmo(attacker);
	}
}

public add_damage_bonus(attacker,victim,damage)
{
	if ((player_b_damage[attacker] > 0) && (GetClientHealth(victim)>player_b_damage[attacker]+RoundToFloor(damage)))
	{
		new Float:dmg = player_b_damage[attacker]*1.0;
		SDKHooks_TakeDamage(victim, attacker, attacker, dmg, DMG_BULLET, -1, NULL_VECTOR, NULL_VECTOR);
	}
}

public add_newbie_bonus(attacker,victim,damage)
{	
	decl String:weapon[100];
    GetClientWeapon(attacker, weapon, 100);
	if (strcmp(weapon, "weapon_knife") != 0 && strcmp(weapon, "weapon_hegrenade") != 0 && strcmp(weapon, "weapon_incgrenade") != 0 && strcmp(weapon, "weapon_decoy") != 0 && strcmp(weapon, "weapon_flashbang") != 0 && strcmp(weapon, "weapon_smokegrenade") != 0) //not knife
    {
		if (player_lvl[attacker] < 15 && player_lvl[victim] > 15)
		{
			if (damage < 100)
			{
				new Float:maxhealth = race_heal[player_class[victim]]*1.0+player_strength[victim]*2.0;
				if(maxhealth > 170.0)
				{
					new Float:dmgbonus = maxhealth*0.3;
					new Float:total = dmgbonus + damage*1.0;
					if(total < 90.0)
					{
						SDKHooks_TakeDamage(victim, attacker, attacker, dmgbonus, DMG_BULLET, -1, NULL_VECTOR, NULL_VECTOR);
					}
				}
			}
		}
	}
}

public check_magic(client)					//Redirect and check which items will be triggered
{
	if(IsPlayerAlive(client))
	{
		if (player_b_meekstone[client] > 0) item_c4fake(client);
		if (player_b_fireball[client] > 0) item_fireball_cast(client);
		if (player_b_ghost[client] > 0) item_ghost(client);
		if (player_b_eye[client] > 0) item_eye(client);
		if (player_b_windwalk[client] > 0) item_windwalk(client);
		if (player_b_dagon[client] > 0) item_dagon(client);
		if (player_b_theif[client] > 0) item_convertmoney(client);
		if (player_b_firetotem[client] > 0) item_firetotem(client);
		if (player_b_hook[client] > 0) item_hook(client);
		if (player_b_gravity[client] > 0) item_gravitybomb(client);
		if (player_b_fireshield[client] > 0) item_rot(client);
		if (player_b_illusionist[client] > 0) item_illusion(client);
		if (player_b_money[client] > 0) item_money_shield(client);
		if (player_b_mine[client] > 0) item_mine(client);
		if (player_b_teamheal[client] > 0) item_teamshield(client);
		if (player_b_heal[client] > 0) item_totemheal(client);
	}
}

public item_money_shield(client)
{
	if(moneyshield[client])
	{
		moneyshield[client] = 0;
	}
	else
	{
		if (GetCurrency(client)-250 > 0)
		{
			AddCurrency(client,-250);
			moneyshield[client] = 1;
		}
		else
		{
			Diablo_ChatMessage(client,"%T","Insufficient funds",client);
		}
	}
}

public item_dagon(client)
{
	if (used_item[client])
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		new Float:position[3];
		new Float:playerDistance, Float:OtherPlayerPos[3];
		GetClientAbsOrigin(client,position);
		new Float:distance=600.0+player_intelligence[client]*20;
		new target=-1;
		new Float:best = 99999.0;
		for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i,true))
			{
				if(GetClientTeam(client) != GetClientTeam(i))
				{
					GetClientAbsOrigin(i,OtherPlayerPos);
					playerDistance = GetVectorDistance(position,OtherPlayerPos);
					if(playerDistance<distance)
					{
						if (playerDistance < best)
						{
							best = playerDistance;
							target = i;
						}
					}
				}
			}
		}
		if(target != -1)
		{
			new DagonDamage = player_b_dagon[client]*20;
			
			DagonDamage-=player_dextery[target]*2;
	
			if(DagonDamage < 0) DagonDamage = 0;
			
			FlashScreen(target,RGBA_COLOR_RED,1.0,1.0,FFADE_OFF);
			
			DealCustomDamage(target, client, DagonDamage, "dagon");
			
			new Float:Red[] = {255,0,0,255};
			new Float:WidthDagon;
			if (player_b_dagon[client] == 1) 
			{
				Red = {175,0,0,150};
				WidthDagon = 10.0;
			}
			else if (player_b_dagon[client] == 2)
			{
				Red = {225,0,0,180};
				WidthDagon = 20.0;
			}
			else if (player_b_dagon[client] > 2)
			{
				Red = {255,0,0,200};
				WidthDagon = 30.0;
			}
			
			used_item[client] = true;
			
			decl String:hinttext[200];
			Format(hinttext, sizeof(hinttext), "%T", "Your Dagon damage - {int}", client, DagonDamage);
			PrintHintText(client, hinttext);
			
			new Float:target_pos[3], Float:start_pos[3];
			GetClientAbsOrigin(client,start_pos);
			GetClientAbsOrigin(target,target_pos);
			target_pos[2]+=30.0;
			TE_SetupBeamPoints(start_pos,target_pos,HaloSprite,HaloSprite,0,35,1.0,WidthDagon,WidthDagon,0,200.0,Red,40);
			TE_SendToAll();
			EmitSoundToAll(dagonsnd,client);
			
			CreateBlinkParticle(target, "extinguish_embers_small_02");
			SetPlayerColored(target,255,0,0,255,1);
		}
	}
}

public item_fireball_cast(client)
{
	if(RoundToFloor(GetGameTime()-player_b_fireball_timer[client]) <= 3)
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "This ability can only be used every {int} seconds", client, 3);
		PrintHintText(client, hinttext);
	}
	else
	{
		player_b_fireball_timer[client] = GetGameTime();
		item_fireball(client, 2);
	}
}

public fireball_cast(client)
{
	if(RoundToFloor(GetGameTime()-player_race_fireball_timer[client]) <= 1)
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "This ability can only be used every {int} seconds", client, 1);
		PrintHintText(client, hinttext);
	}
	else
	{
		player_race_fireball_timer[client] = GetGameTime();
		item_fireball(client, 1);
	}
}

public item_ghost(client)
{
	if(ghoststate[client] == 0)
	{
		SetEntityMoveType(client, MOVETYPE_NOCLIP);
		ghoststate[client] = 2;
		SetEntProp(client, Prop_Data, "m_takedamage", 2, 1);
		player_b_ghost_end[client]=GetGameTime()+player_b_ghost[client];
		SetEntDataFloat(client,m_OffsetSpeed,1.2,true);
	}
	else
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
}

public OnGameFrame()
{
	static Float:ClientAngles[3];
	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && player_b_eye_viewing[i] != 0)
		{
			GetClientEyeAngles(i, ClientAngles);
			ClientAngles[2] = 360.0;
			TeleportEntity(player_b_eye_created[i], NULL_VECTOR, ClientAngles, NULL_VECTOR);
		}
	}
}

public item_eye(client)
{
	if (player_b_eye_created[client] == 0)
	{
		new trigger = CreateEntityByName("smokegrenade_projectile");
		if(IsValidEntity(trigger)) 
		{
			SetEntityModel(trigger, "models/error.mdl");
			//DispatchKeyValue(trigger, "spawnflags", "1");
			//DispatchKeyValue(trigger, "wait", "0");
			DispatchSpawn(trigger);
			new Float:pos[3];
			GetEntPropVector(client, Prop_Data, "m_vecOrigin", pos);
			pos[2]+=100.0;
			TeleportEntity(trigger, pos, NULL_VECTOR, NULL_VECTOR);
			player_b_eye_created[client] = trigger;
			player_b_eye_viewing[client] = 0;
			SetEntProp(trigger, Prop_Send, "m_nSolidType", 0);
			/*new m_fEffects = GetEntProp(trigger, Prop_Send, "m_fEffects");
			m_fEffects |= 32;
			SetEntProp(trigger, Prop_Send, "m_fEffects", m_fEffects);
			ActivateEntity(trigger);*/
			SetEntityRenderMode(trigger, RENDER_NONE);
			SetEntityMoveType(trigger, MOVETYPE_NONE);
		}
	}
	else
	{
		if(player_b_eye_viewing[client] > 0)
		{
			SetClientViewEntity(client, client);
			player_b_eye_viewing[client] = 0;
		}
		else
		{
			SetClientViewEntity(client, player_b_eye_created[client]);
			player_b_eye_viewing[client] = 1;
		}
	}
}

public item_windwalk(client)
{
	if (player_b_usingwind[client] == 0)
	{
		player_b_usingwind[client] = 1;
		set_renderchange(client);
		
		ClientCommand(client, "slot3");
		on_knife[client]=1;
		SetEntDataFloat(client,m_OffsetSpeed,2.0,true);
		
		new Float:val = player_b_windwalk[client] + 0.0;
		CreateTimer(val, ResetWindWalk, client); 
	}
	else if (player_b_usingwind[client] == 1)
	{
		player_b_usingwind[client] = 2;
		
		set_renderchange(client);
		set_speedchange(client);
	}
	else if (player_b_usingwind[client] == 2)
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}	
}

public Action:ResetWindWalk(Handle:timer, any:client)
{
	if (player_b_usingwind[client] == 1)
	{
		player_b_usingwind[client] = 2;
		
		set_renderchange(client);
		set_speedchange(client);
		
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "Lothars Edge stopped.", client);
		PrintHintText(client, hinttext);
		Diablo_ChatMessage(client,hinttext);
	}
}

public item_c4fake(client)
{
	if (c4state[client] > 1)
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		if (player_b_meekstone[client] > 0 && c4state[client] == 1)
		{
			new Float:playerDistance, Float:OtherPlayerPos[3];
			
			new explode = CreateEntityByName("env_explosion");
			DispatchKeyValue(explode, "iMagnitude", "300");
			DispatchKeyValue(explode, "targetname", "meekstone_exp"); 
			DispatchKeyValue(explode, "iRadiusOverride", "300");
			SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", client);
			
			DispatchSpawn(explode);
			new Float:position[3];
			GetEntPropVector(c4fake[client], Prop_Send, "m_vecOrigin", position);
			TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
			new particle = CreateEntityByName("info_particle_system");

			decl String:name[64];

			if (IsValidEdict(particle))
			{
				new Float:angle[3];
				angle[0] = 270.0;
				//angle[1] = 100.0;
				//angle[2] = 100.0;
				//position[2] += 50.0;
				TeleportEntity(particle, position, angle, NULL_VECTOR);
				GetEntPropString(client, Prop_Data, "m_iName", name, sizeof(name));
				DispatchKeyValue(particle, "targetname", "tf2particle");
				DispatchKeyValue(particle, "parentname", name);
				DispatchKeyValue(particle, "effect_name", "explosion_basic");
				DispatchSpawn(particle);
				SetVariantString(name);
				AcceptEntityInput(particle, "SetParent", particle, particle, 0);
				ActivateEntity(particle);
				AcceptEntityInput(particle, "start");
				CreateTimer(2.0, DeleteLevelupParticle, particle);
			}
			AcceptEntityInput(explode, "Explode");
			EmitSoundToAll("weapons/hegrenade/explode5.wav", explode);
			AcceptEntityInput(explode, "kill");
			for(new i=1;i<=MaxClients;i++)
			{
				if(ValidPlayer(i,true))
				{
					if(GetClientTeam(client) != GetClientTeam(i))
					{
						GetClientAbsOrigin(i,OtherPlayerPos);
						playerDistance = GetVectorDistance(position,OtherPlayerPos);
						if(playerDistance<300.0)
						{
							DealCustomDamage(i, client, 400, "meekstone");
						}
					}
				}
			}
			AcceptEntityInput(c4fake[client], "kill");
			c4state[client] = 2;
			c4fake[client] = 0;
		}
		if (player_b_meekstone[client] > 0 && c4state[client] == 0 && c4fake[client] == 0)
		{
			c4fake[client] = CreateEntityByName("prop_physics_override");
		
			new Float:PlayerPos[3];
			GetClientAbsOrigin(client,PlayerPos);
			//make sure entity was created successfully
			if (c4fake[client] != -1)
			{
				DispatchKeyValue(c4fake[client], "model", "models/weapons/w_c4_planted.mdl");  
				DispatchKeyValue(c4fake[client], "spawnflags", "8");   
				DispatchKeyValue(c4fake[client], "solid", "2");
				DispatchKeyValue(c4fake[client], "targetname", "meekstone"); 

				SetEntPropEnt(c4fake[client], Prop_Send, "m_hOwnerEntity", client);

				DispatchSpawn(c4fake[client]);
				TeleportEntity(c4fake[client], PlayerPos, NULL_VECTOR, NULL_VECTOR);
				c4state[client] = 1;
			}
		}
	}
}

public item_convertmoney(client)
{
	new maxhealth = race_heal[player_class[client]]+player_strength[client]*2;
	
	decl String:hinttext[200];
	if (GetCurrency(client) < 1000)
	{
		Format(hinttext, sizeof(hinttext), "%T", "Not enough money to convert into life", client);
		PrintHintText(client, hinttext);
	}
	else if (GetClientHealth(client) >= maxhealth)
	{
		Format(hinttext, sizeof(hinttext), "%T", "You are already on mamixum life", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		AddCurrency(client,-1000);
		add_health(client,15);		
		FlashScreen(client,RGBA_COLOR_BLUE,1.0,1.0,FFADE_OFF);
	}
}

public item_gravitybomb(client)
{
	decl String:hinttext[200];
	if(GetEntityFlags(client) & FL_ONGROUND)
	{
		Format(hinttext, sizeof(hinttext), "%T", "You have to be in the air to earth stomp", client);
		PrintHintText(client, hinttext);
	}else if(RoundToFloor(GetGameTime()-gravitytimer[client]) <= 5)
	{
		Format(hinttext, sizeof(hinttext), "%T", "This ability can only be used every {int} seconds", client, 5);
		PrintHintText(client, hinttext);
	}
	else
	{	
		gravitytimer[client] = GetGameTime();
		
		new Float:Clientpos[3];
		GetClientAbsOrigin(client,Clientpos);
		new heightjump = RoundToFloor(Clientpos[2]);
		
		if (Clientpos[2] == 0)
			earthstomp[client] = 1;
		else
			earthstomp[client] = heightjump;
	
		falling[client] = true;
		fallingtime[client] = GetGameTime();
		SetEntityGravity(client, 5.0);
	}
}

public item_hook(client)
{
	if (used_item[client])
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		new target = GetClientAimTarget(client,false);
		if(ValidPlayer(target, true) && GetClientTeam(client) != GetClientTeam(target))
		{
			new Float:OriginG[3], Float:TargetOriginG[3];
			GetEntPropVector(client, Prop_Data, "m_vecOrigin", TargetOriginG);
			GetEntPropVector(target, Prop_Data, "m_vecOrigin", OriginG);
			TargetOriginG[2]+=30.0;
			if(GetVectorDistance(TargetOriginG,OriginG, false) < 1000.0)
			{
				used_item[client] = true;
				
				new Float:velocity[3];
				velocity[0] = (TargetOriginG[0] - OriginG[0]) * 3.0;
				velocity[1] = (TargetOriginG[1] - OriginG[1]) * 3.0;
				velocity[2] = (TargetOriginG[2] - OriginG[2]) * 3.0;
				
				new Float:dy;
				dy = velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2];
				
				new Float:dx;
				dx = (4.0+player_intelligence[client]/2.0) * 120.0 / SquareRoot(dy);
				
				velocity[0] *= dx;
				velocity[1] *= dx;
				velocity[2] *= dx;
				
				TeleportEntity(target, NULL_VECTOR, NULL_VECTOR, velocity);
				TE_SetupBeamPoints(OriginG,TargetOriginG,WhiteSprite,HaloSprite,0,35,1.0,5.0,5.0,0,0,{0, 255, 0, 255},40);
				TE_SendToAll();
				EmitSoundToAll(hooksnd,client);
				
				SetPlayerColored(target,0,255,0,255,1);
			}
		}
	}
}

public item_mine(client)
{
	if (player_b_mine_count[client]== 0)
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You don't have mines", client);
		PrintHintText(client, hinttext);
	}
	else
	{		
		new entity = CreateEntityByName("prop_physics_override");
		
		new Float:PlayerPos[3];
		GetClientAbsOrigin(client,PlayerPos);
		//make sure entity was created successfully
		if (entity != -1)
		{
			DispatchKeyValue(entity, "model", "models/diablomod.net/mine.mdl");

			DispatchKeyValue(entity, "targetname", "diablomine"); 

			SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", client);
			SetEntProp(entity, Prop_Send, "m_nSolidType", 0);
			DispatchSpawn(entity);
			SetEntityModel(entity,"models/diablomod.net/mine.mdl");

			TeleportEntity(entity, PlayerPos, NULL_VECTOR, NULL_VECTOR);
			
			player_b_mine_count[client]--;
			SetEntityAlpha(entity, 50);
			SDKHook(entity, SDKHook_Touch, OnStartTouchMine);
		}
	}
}

public OnStartTouchMine(entity, other)
{
	new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if(owner!=other)
	{
		
		if(0 < other <= MaxClients)
		{
			if(GetClientTeam(other) != GetClientTeam(owner))
			{
				new Float:playerDistance, Float:OtherPlayerPos[3];
				new explode = CreateEntityByName("env_explosion");
				new String:damagestr[5];
				new damage = 55+player_intelligence[owner];
				Format(damagestr, sizeof(damagestr), "%d", damage);
				DispatchKeyValue(explode, "iMagnitude", damagestr);
				DispatchKeyValue(explode, "targetname", "mine_exp"); 
				DispatchKeyValue(explode, "iRadiusOverride", "150");
				SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
				
				DispatchSpawn(explode);
				new Float:position[3];
				GetEntPropVector(entity, Prop_Send, "m_vecOrigin", position);
				TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
				new particle = CreateEntityByName("info_particle_system");

				decl String:name[64];

				if (IsValidEdict(particle))
				{
					new Float:angle[3];
					angle[0] = 270.0;
					//angle[1] = 100.0;
					//angle[2] = 100.0;
					//position[2] += 50.0;
					TeleportEntity(particle, position, angle, NULL_VECTOR);
					GetEntPropString(owner, Prop_Data, "m_iName", name, sizeof(name));
					DispatchKeyValue(particle, "targetname", "tf2particle");
					DispatchKeyValue(particle, "parentname", name);
					DispatchKeyValue(particle, "effect_name", "explosion_basic");
					DispatchSpawn(particle);
					SetVariantString(name);
					AcceptEntityInput(particle, "SetParent", particle, particle, 0);
					ActivateEntity(particle);
					AcceptEntityInput(particle, "start");
					CreateTimer(2.0, DeleteLevelupParticle, particle);
				}
				AcceptEntityInput(explode, "Explode");
				EmitSoundToAll("weapons/hegrenade/explode5.wav", explode);
				AcceptEntityInput(explode, "kill");
				AcceptEntityInput(entity, "kill");
				player_b_mine_count[owner]++;
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true))
					{
						if(GetClientTeam(owner) != GetClientTeam(i))
						{
							GetClientAbsOrigin(i,OtherPlayerPos);
							playerDistance = GetVectorDistance(position,OtherPlayerPos);
							if(playerDistance<150.0)
							{
								DealCustomDamage(i, owner, damage, "diablomine");
							}
						}
					}
				}
			}
		}
	}
}

public item_teamshield(client)
{
	if (used_item[client])
	{
		used_item[client] = false;
		ResetPlayerRGB(player_b_teamheal_using[client]);
		set_renderchange(player_b_teamheal_using[client]);
		player_b_teamheal_shielded[player_b_teamheal_using[client]] = -1;
		player_b_teamheal_using[client] = -1;
	}
	else
	{
		decl String:hinttext[200];
		new target = GetClientAimTarget(client,false);
		if(ValidPlayer(target, true) && GetClientTeam(client) == GetClientTeam(target))
		{
			if(((player_b_inv[client] < 21) && (player_b_inv[client] > 0)) || player_class[target] == 7)
			{
				Format(hinttext, sizeof(hinttext), "%T", "Can not cast shield on invisible players", client);
				PrintHintText(client, hinttext);
			}
			else
			{
				player_b_teamheal_using[client] = target;
				player_b_teamheal_shielded[target] = client;
				used_item[client] = 1;
				SetPlayerColored(target,0,200,0,100,1);
				new Float:target_pos[3], Float:start_pos[3];
				GetClientAbsOrigin(client,start_pos);
				GetClientAbsOrigin(target,target_pos);
				target_pos[2]+=30.0;
				TE_SetupBeamPoints(start_pos,target_pos,HaloSprite,HaloSprite,0,35,1.0,1.0,1.0,0,200.0,{0,255,0,60},40);
				TE_SendToAll();
				
				if (GetClientHealth(target)+player_b_teamheal[target] <= race_heal[player_class[target]]+player_strength[target]*2) add_health(target,player_b_teamheal[client]);
			}
		}
		else
		{
			Format(hinttext, sizeof(hinttext), "%T", "No targets in range to shield", client);
			PrintHintText(client, hinttext);
		}
	}
}

public item_totemheal(client)
{
	if (used_item[client])
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		used_item[client] = true;
		
		new proppo = CreateEntityByName("prop_physics_multiplayer");
		
		new Float:PlayerPos[3];
		GetClientAbsOrigin(client,PlayerPos);
		PlayerPos[2] += 10.0;
		//make sure entity was created successfully
		if (proppo != -1)
		{
			DispatchKeyValue(proppo, "model", "models/diablomod.net/totem_heal.mdl");  
			DispatchKeyValue(proppo, "spawnflags", "8");   
			DispatchKeyValue(proppo, "solid", "2");
			DispatchKeyValue(proppo, "targetname", "totemheal"); 

			SetEntPropEnt(proppo, Prop_Send, "m_hOwnerEntity", client);

			DispatchSpawn(proppo);
			TeleportEntity(proppo, PlayerPos, NULL_VECTOR, NULL_VECTOR);
			TotemTimer[proppo] = GetGameTime()+7.0;
		}
	}
}

public item_firetotem(client)
{
	if (used_item[client])
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		used_item[client] = true;
		
		new proppo = CreateEntityByName("prop_physics_multiplayer");
		
		new Float:PlayerPos[3];
		GetClientAbsOrigin(client,PlayerPos);
		PlayerPos[2] += 10.0;
		//make sure entity was created successfully
		if (proppo != -1)
		{
			DispatchKeyValue(proppo, "model", "models/diablomod.net/totem_fire.mdl");  
			DispatchKeyValue(proppo, "spawnflags", "8");   
			DispatchKeyValue(proppo, "solid", "2");
			DispatchKeyValue(proppo, "targetname", "totemfire"); 

			SetEntPropEnt(proppo, Prop_Send, "m_hOwnerEntity", client);

			DispatchSpawn(proppo);
			TeleportEntity(proppo, PlayerPos, NULL_VECTOR, NULL_VECTOR);
			TotemTimer[proppo] = GetGameTime()+7.0;
		}
	}
}

public item_illusion(client)
{
	if (used_item[client])
	{
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "You can only use this item one time each round", client);
		PrintHintText(client, hinttext);
	}
	else
	{
		used_item[client] = true;
		player_b_illusionist_activated[client] = true;
		player_b_illusionist_end[client]=GetGameTime()+7.0;
	}
}

public item_rot(client)
{
	if (player_b_fireshield_activated[client])
	{
		player_b_fireshield_activated[client] = false;
		set_speedchange(client);
		ResetPlayerRGB(client);
		set_renderchange(client);
	}
	else
	{
		if(GetClientHealth(client) > 10)
		{
			player_b_fireshield_activated[client] = true;
			new Float:speeds;
			speeds = 1.0 + player_dextery[client]*0.006 + 0.2;
			SetEntDataFloat(client,m_OffsetSpeed,speeds,true);
			SetPlayerColored(client,255,255,0,10,1);
			
			new Float:position[3];
			new Float:playerDistance, Float:OtherPlayerPos[3];
			GetClientAbsOrigin(client,position);
			for(new i=1;i<=MaxClients;i++)
			{
				if(ValidPlayer(i,true))
				{
					if(GetClientTeam(client) != GetClientTeam(i))
					{
						GetClientAbsOrigin(i,OtherPlayerPos);
						playerDistance = GetVectorDistance(position,OtherPlayerPos);
						if(playerDistance<250.0)
						{
							if (GetRandomInt(1,2) == 1)
							{
								FlashScreen(i,RGBA_COLOR_BLIND,1.0,1.0,FFADE_OFF);
							}
							DealCustomDamage(i, client, 45, "fireshield");
							SetEntDataFloat(i,m_OffsetSpeed,0.2,true);
							player_slowed[i]++;
						}
					}
				}
			}
			add_health(client, -10);
}
		else
		{
			decl String:hinttext[200];
			Format(hinttext, sizeof(hinttext), "%T", "Not enought health", client,player_knife[client]);
			PrintHintText(client, hinttext);
		}
	}
}

public Action:Hook_SetTransmit(entity, client)
{
	//if (IsValidEdict(client))
	//{
		decl String:iWeapon[32]; 
		GetEdictClassname(entity, iWeapon, sizeof(iWeapon)); 
		if(StrEqual(iWeapon, "weaponworldmodel", false))
		{
			new owner = worldM_owner[entity];
			//PrintToChatAll("%s",iWeapon);
			if(0 < owner <= MaxClients)
			{
				if((player_class[owner] == 7) || (invisible_cast[owner] == 1) || (0 < player_b_inv[owner] < 21))
				{
					return Plugin_Handled;
				}
			}
		}
	//}
	if (client != entity && (0 < entity <= MaxClients) && IsPlayerAlive(client) && player_b_illusionist_activated[client])
	{
		return Plugin_Handled;
	}
	if (client != entity && (0 < entity <= MaxClients) && IsPlayerAlive(client) && IsPlayerAlive(entity) && player_b_illusionist_activated[entity])
	{
		return Plugin_Handled;
	}

	return Plugin_Continue;
}  

public Event_GroundEntChanged(client)
{
    if(ValidPlayer(client))
		if(player_class[client] == 7 || player_b_gravity[client] > 0)
			if(GetEntityMoveType(client) != MOVETYPE_LADDER)	
                if(GetEntityGravity(client) != get_gravitychange(client))
                    set_gravitychange(client);
}  

public OnClientDisconnect(client)
{
	SaveData(client);
	SDKUnhook(client, SDKHook_OnTakeDamagePost, Hook_OnTakeDamagePost);
	SDKUnhook(client, SDKHook_TraceAttack, TraceAttack);
	SDKUnhook(client, SDKHook_WeaponCanUse, OnWeaponCanUse); 
	SDKUnhook(client, SDKHook_WeaponEquip, OnPostWeaponEquip);
	SDKUnhook(client, SDKHook_WeaponSwitch, OnWeaponSwitch);
	SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKUnhook(client, SDKHook_SetTransmit, Hook_SetTransmit);
	SDKUnhook(client, SDKHook_PostThinkPost, OnPostThinkPost);
	SDKUnhook(client, SDKHook_GroundEntChangedPost, Event_GroundEntChanged);
	ResetPlayer(client);
	
	new i = -1;
	while ((i = FindEntityByClassname(i, "trigger_multiple")) != -1) { 
		new owner = GetEntPropEnt(i, Prop_Send, "m_hOwnerEntity");
		if(owner > -1)
		{
			decl String:iclass[32]; 
			GetEdictClassname(i, iclass, sizeof(iclass)); 
			if(StrEqual(iclass, "hegrenade_projectile", false)) 
			{ 
				AcceptEntityInput(owner, "kill");
				AcceptEntityInput(i, "kill");
			}
		}
	}
}

public Action:OnWeaponCanUse(client, weaponent)
{
	if(player_class[client] == 7)
	{
		decl String:iWeapon[32]; 
		GetEdictClassname(weaponent, iWeapon, sizeof(iWeapon)); 
		if(!StrEqual(iWeapon, "weapon_knife", false) && !StrEqual(iWeapon, "weapon_c4", false) && !StrEqual(iWeapon, "item_assaultsuit", false) && !StrEqual(iWeapon, "item_defuser", false) && !StrEqual(iWeapon, "item_kevlar", false)) 
		{ 
			
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action:OnWeaponSwitch(client, weapon)
{
	decl String:iWeapon[32]; 
	GetEdictClassname(weapon, iWeapon, sizeof(iWeapon)); 
	if(StrContains(iWeapon, "weapon_knife"))
	{
		on_knife[client] = 0;
	}
	else
	{
		on_knife[client] = 1;
	}
	
	if(player_knife2[client] == 0)
	{
		invisible_cast[client] = 0;
		set_renderchange(client);
	}
	set_gravitychange(client);
	if(player_b_usingwind[client] != 1)
	{
		set_speedchange(client);
	}
}

public Action:OnPostWeaponEquip(client, weapon)
{	
	if(weapon < 1 || !IsValidEdict(weapon) || !IsValidEntity(weapon)) return;
	
	new iWorldModel = GetEntPropEnt(weapon, Prop_Send, "m_hWeaponWorldModel");
	if(IsValidEdict(iWorldModel))
	{
		new String:sWeapon[64];
		GetEntityClassname(iWorldModel, sWeapon, sizeof(sWeapon));
		SDKHook(iWorldModel, SDKHook_SetTransmit, Hook_SetTransmit);
		worldM_owner[iWorldModel] = client;
	}
}

public Action:CS_OnBuyCommand(client, const String:chWeapon[])
{
	if(player_class[client] == 7) 
	{
		if(!(StrEqual(chWeapon, "kevlar") || StrEqual(chWeapon, "defuser") || StrEqual(chWeapon, "assaultsuit")))
		{
			return Plugin_Handled; // Block the buy.
		}
	}

	return Plugin_Continue; // Continue as normal.
} 


public Action:TimerEveryQuarSecond(Handle:timer,any:userid)
{
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true))
		{
			if(flashlight_enabled[client])
			{
				if(flashbattery[client] == 0)
				{
					ToggleFlashlight(client);
				}
				else
				{
					flashbattery[client]--;
				}
				new target = GetClientAimTarget(client,false);
				if(ValidPlayer(target, true))
				{
					if(GetClientTeam(client) != GetClientTeam(target))
					{
						new colorpick = GetRandomInt(1,3);
						new r, g, b;
						switch(colorpick)
						{
							case 1:
							{
								r = GetRandomInt(245,255);
								g = GetRandomInt(0,255);
							}
							case 2:
							{
								g = GetRandomInt(245,250);
								b = GetRandomInt(0,240);
							}
							case 3:
							{
								b = GetRandomInt(245,255);
								r = GetRandomInt(0,255);
							}
						}
						SetPlayerColored(target,r,g,b,255,7);
					}
					else if(GetClientTeam(client) != GetClientTeam(target))
					{
						if(target != -1)
						{
							new String:targetname[16];
							GetEntPropString(target, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
							if(StrContains("hideradar", targetname))
							{
								strcopy(targetname, sizeof(targetname), targetname[9]);
								new owner = StringToInt(targetname);
								new colorpick = GetRandomInt(1,3);
								new r, g, b;
								switch(colorpick)
								{
									case 1:
									{
										r = GetRandomInt(245,255);
										g = GetRandomInt(0,255);
									}
									case 2:
									{
										g = GetRandomInt(245,250);
										b = GetRandomInt(0,240);
									}
									case 3:
									{
										b = GetRandomInt(245,255);
										r = GetRandomInt(0,255);
									}
								}
								SetPlayerColored(owner,r,g,b,255,7);
							}
						}
					}
				}
			}
			else
			{
				if(player_class[client] == 1)
				{
					if(flashbattery[client] < flashbatteryMax[client])
					{
						flashbattery[client]++;
					}
				}
			}
		}
	}
}

public Action:TimerEveryHalfSecond(Handle:timer,any:userid)
{
	decl String:Itemname[200],String:racename[32],String:racenumtext[32],String:message[200];
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true) && player_class[client] != 5 && player_class[client] != 0)
		{
			new buttons = GetClientButtons(client);
			if((GetEntityFlags(client) & FL_ONGROUND) && !(buttons & IN_FORWARD+IN_BACK+IN_MOVELEFT+IN_MOVERIGHT) && on_knife[client] || (player_class[client] == 1 && on_knife[client]))
			{
				if(!bow[client])
				{
					if(casting[client]==1 && GetGameTime()>cast_end[client])
					{
						casting[client]=0;
						call_cast(client);
					}
					else if(casting[client]==0)
					{
						new Float:time_delay = GetTimeDelay(client);
						cast_end[client]=GetGameTime()+time_delay;
						casting[client]=1;
						
						ShowCharging(client,1);
					}
					else
					{
						ShowCharging(client,0);
					}
				}
			}
			else
			{
				if(casting[client]==1)
				{
					casting[client]=0;
					ShowCharging(client,2);
				}
			}
			if(on_knife[client] && bow[client])
			{
				ShowReloading(client);
			}
		}
		if(ValidPlayer(client,true) && ghoststate[client] == 2)
		{
			ShowGhostBar(client);
		}
		if(ValidPlayer(client,true) && player_b_illusionist_activated[client])
		{
			ShowIllusBar(client);
		}
		if(IsClientInGame(client))
		{
			new observer_mode = GetEntProp(client, Prop_Send, "m_iObserverMode");
			if((!IsFakeClient(client)) && ((observer_mode == SPECTATOR_FIRSTPERSON) || (observer_mode == SPECTATOR_3RDPERSON)))
			{
				new target = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
				if(target > 0 && target <= MaxClients && IsClientInGame(target) && IsPlayerAlive(target))
				{
					if(player_item_id[target] == 0)
					{
						Format(Itemname, sizeof(Itemname), "%T", "No item", client);
					}
					else if(player_item_id[target] == 100)
					{
						Format(Itemname, sizeof(Itemname), "%s", player_item_name[target]);
					}
					else
					{
						Format(Itemname, sizeof(Itemname), "%T", player_item_name[target], client);
					}
					if(player_class[target] == 0)
					{
						Format(racename, sizeof(racename), "%T", "No race", client);
					}
					else
					{
						Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[target]);
						Format(racename, sizeof(racename), "%T", racenumtext, client);
					}
					Format(message,sizeof(message),"<font color='#abff00'>%s</font>(%d)<br>%s",racename,player_lvl[target],Itemname);
					new Handle: message_handle = StartMessageOne("KeyHintText", client);
					if (message_handle != INVALID_HANDLE) 
					{
						PbAddString(message_handle, "hints", message);
						EndMessage();
					}
				}
			}
		}
	}
}

public call_cast(client)
{
	decl String:hinttext[200];
	
	switch(player_class[client])
	{
		case 1:
		{
			fireball_collected[client]++;
			if(fireball_collected[client] > 10) fireball_collected[client] = 10;
			Format(hinttext, sizeof(hinttext), "%T", "You have {int} fireballs", client, fireball_collected[client]);
			PrintHintText(client, hinttext);
		}
		case 2:
		{
			if(monk_shield[client])
			{
				createBlockAiming(client);
			}
			else
			{
				Format(hinttext, sizeof(hinttext), "%T", "You have no walls", client);
				PrintHintText(client, hinttext);
			}
		}
		case 3:
		{
			golden_bullet[client]++;
			if(golden_bullet[client]>3)
			{
				golden_bullet[client]=3;
			}
			Format(hinttext, sizeof(hinttext), "%T", "You have {int} magic bullets", client, golden_bullet[client]);
			PrintHintText(client, hinttext);
		}
		case 4:
		{
			invisible_cast[client]=1;
			set_renderchange(client);
			Format(hinttext, sizeof(hinttext), "%T", "You are invisible until weapon change", client);
			PrintHintText(client, hinttext);
		}
		case 6:
		{
			ultra_armor[client]++;
			if(ultra_armor[client]>3)
			{
				ultra_armor[client]=3;
			}
			Format(hinttext, sizeof(hinttext), "%T", "You have {int} magic armor", client, ultra_armor[client]);
			PrintHintText(client, hinttext);
		}
		case 7:
		{
			new Float:curspeed=GetEntDataFloat(client, m_OffsetSpeed);
			curspeed = curspeed + 0.02;
			SetEntDataFloat(client,m_OffsetSpeed,curspeed,true);
			Format(hinttext, sizeof(hinttext), "%T", "Your speed is temporarily increased({float})", client, curspeed);
			PrintHintText(client, hinttext);
		}
		case 8:
		{
			if(!CheckHasGrenade(client))
			{
				GivePlayerItem(client, "weapon_hegrenade");
			}
			else
			{
				Format(hinttext, sizeof(hinttext), "%T", "U already have a grenade", client);
				PrintHintText(client, hinttext);
			}
		}
	}
}

public Action:OnNormalSoundPlayed(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{	
	if (0 < entity <= MaxClients)
	{
		if(StrContains(sample, "physics") != -1 || StrContains(sample, "footstep") != -1)
		{
			// Player not ninja, play footsteps
			if(player_class[entity] != 4 && player_b_silent[entity] == 0)
			{
				numClients = 0;

				for(new i = 1; i <= MaxClients; i++)
				{
					if(IsClientInGame(i) && !IsFakeClient(i))
					{
						clients[numClients++] = i;
					}
				}
				if(StrContains(sample, "suit_") == -1) EmitSound(clients, numClients, sample, entity);
				//return Plugin_Changed;
			}
			return Plugin_Stop;
		}
	}
	return Plugin_Continue;
}

//Paladin shots
public weapon_fire(Handle:event,const String:name[],bool:dontBroadcast) 
{ 
    new userid = GetEventInt(event, "userid"); 
    new client = GetClientOfUserId(userid); 
    new entity = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");

    // timer not currently running 
    if(MissTimers[client] == INVALID_HANDLE) 
    { 
        // should filter knife and grenades 
        if(GetEntProp(entity, Prop_Send, "m_iPrimaryAmmoType") != -1 && GetEntProp(entity, Prop_Send, "m_iClip1") != 255) 
        { 
            new Handle:datapack; 
            MissTimers[client] = CreateDataTimer(0.0, Missed, datapack); 
            WritePackCell(datapack, client); 
            WritePackCell(datapack, userid); 
            ResetPack(datapack); 
        } 
    } 
} 
//Paladin shots
public Action:Missed(Handle:timer, Handle:datapack) 
{ 
    new client = ReadPackCell(datapack); 
    MissTimers[client] = INVALID_HANDLE; // Clear Timer Handle 

    new userid = ReadPackCell(datapack); 
    if(GetClientOfUserId(userid) == 0 || !IsClientInGame(client) || !IsPlayerAlive(client)) // Not in game anymore or dead 
    { 
        return; 
    } 
	
	if(golden_bullet[client]) golden_bullet[client]--; 
} 

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon) 
{    
    if (IsPlayerAlive(client) && !IsFakeClient(client))
	{	
		new fCurFlags = GetEntityFlags(client);
		
		if(player_class[client] == 4)
		{
			SendConVarValue(client, sv_footsteps, "0");
		}
		
		if ((buttons & IN_SPEED))
		{
			player_inshift[client] = 1;
		}
		else
		{
			player_inshift[client] = 0;
		}
		
		if ((buttons & IN_DUCK) && (buttons & IN_JUMP))
		{
			player_induck[client] = 1;
		}
		else
		{
			player_induck[client] = 0;
		}
		if(player_b_jumpx[client])
		{
			if (g_fLastFlags[client] & FL_ONGROUND)
			{
				if (!(g_fLastButtons[client] & IN_JUMP) && (buttons & IN_JUMP))
				{
					OriginalJump(client);
				}
			}
			else if (fCurFlags & FL_ONGROUND)
			{
				Landed(client);
			}
			else if (!(g_fLastButtons[client] & IN_JUMP) && buttons & IN_JUMP)
			{
				ReJump(client);
			}
		}
		
		if (!player_inuse[client] && buttons & IN_USE)
		{
			check_magic(client);
			player_inuse[client] = true;
		}
		else if(player_inuse[client] && !(buttons & IN_USE))
		{
			player_inuse[client] = false;
		}
		
		if (buttons & IN_ATTACK)
		{
			if(bow[client] && on_knife[client])
			{
				if((bowdelay[client] + 4.25 - RoundToFloor(player_intelligence[client]/25.0))< GetGameTime())
				{
					bowdelay[client] = GetGameTime();
					command_arrow(client);
				}
			}
			if(player_b_usingwind[client] == 1)
			{
				buttons &= ~IN_ATTACK;
			}
		}
		
		if (buttons & IN_ATTACK2)
		{
			if(player_class[client] == 8)
			{
				new iWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
			
				new String:sWeapon[64];
				GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
				if(StrEqual(sWeapon, "weapon_hegrenade", false))
				{
					player_trapgren[client] = 1;
				}
			}
			if(player_b_blink[client] && !bow[client])
			{
				if (on_knife[client])
				{
					if(GetGameTime()-player_b_blink_timer[client] >= 3.0)
					{
						UTIL_Teleport(client,300.0+15.0*player_intelligence[client]);
					}
				}
			}
			if(player_b_usingwind[client] == 1)
			{
				buttons &= ~IN_ATTACK2;
			}
		}
		
		if (!g_InReload[client] && buttons & IN_RELOAD)
		{
			new iWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
			
			new String:sWeapon[64];
			GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
			if(StrEqual(sWeapon, "weapon_knife", false))
			{
				if (iWeapon > 0 && IsValidEntity(iWeapon) && player_knife[client] > 0)
				{
					CreateKnife(client);
				}
				if (iWeapon > 0 && IsValidEntity(iWeapon) && player_class[client] == 8)
				{
					command_bow(client);
				}
				if (iWeapon > 0 && IsValidEntity(iWeapon) && player_class[client] == 1)
				{
					if(fireball_collected[client]) fireball_cast(client);
				}
			}
			g_InReload[client] = true;
		}
		else if(g_InReload[client] && !(buttons & IN_RELOAD))
		{
			g_InReload[client] = false;
		}
		
		if(g_haskit[client])
		{
			if(buttons & IN_DUCK)
			{
				if(g_bHoldingTimeFrame[client] == 0)
				{
					g_bHoldingTimeFrame[client] = GetGameTime();
				}
				g_bHoldingProp[client] = true;
				g_bHoldingCount[client]++;
				
				new entidad = GetClientAimTarget(client, false);
				if(entidad > 0)
				{			
					new Float:OriginG[3], Float:TargetOriginG[3];
					GetClientEyePosition(client, TargetOriginG);
					GetEntPropVector(entidad, Prop_Data, "m_vecOrigin", OriginG);
					if(GetVectorDistance(TargetOriginG,OriginG, false) > 90.0)
						return Plugin_Continue;
					
					new iSize = GetArraySize(g_hRagdollArray);
					if(iSize == 0)
						return Plugin_Continue;
					
					new dPlayer[Ragdolls];
					new entity;

					for(int i = 0;i < iSize;i++)
					{
						GetArrayArray(g_hRagdollArray, i, dPlayer[0]);
						entity = EntRefToEntIndex(dPlayer[Ragdollent]);

						if(entity == entidad)
						{
							ShowNecrBar(client, dPlayer[RagdollTeam], g_bHoldingTimeFrame[client], dPlayer[victimName]);
							//SetEntityRenderColor(client, 255, 0, 0, 255);
							if(GetGameTime() > g_bHoldingTimeFrame[client]+3.0)
							{
								AcceptEntityInput(entity, "kill");						
								SetArrayArray(g_hRagdollArray, i, dPlayer[0]); //TODO remove
								g_bHoldingTimeFrame[client] = 0;
								if((GetClientTeam(client) == dPlayer[RagdollTeam]) && (client != dPlayer[victim2]))
								{
									if(ValidPlayer(dPlayer[victim2]))
									{
										if(!IsPlayerAlive(dPlayer[victim2]))
										{
											
											CS_RespawnPlayer(dPlayer[victim2]);
											//OriginG[2] += 30.0;
											TeleportEntity(dPlayer[victim2],OriginG,NULL_VECTOR,NULL_VECTOR);
											new xp_award = GetConVarInt(g_hCVKillXP);
											new String:killaward[64];
											Format(killaward, sizeof(killaward), "%T", "revive", client);
											Give_Xp(client,xp_award,killaward);
											EmitSoundToAll(revivesnd,client);
										}
									}
								}
								else if(GetClientTeam(client) != dPlayer[RagdollTeam])
								{
									add_health(client,30);
									EmitSoundToAll(medshotsnd,client);
								}
							}
							break;
						}
					}
				}
			}
			else
			{
				g_bHoldingProp[client] = false;
				g_bHoldingCount[client] = 0;
				g_bHoldingTimeFrame[client] = 0;
			}
		}
		
		g_fLastFlags[client]	= fCurFlags;
		g_fLastButtons[client]	= buttons;
	}
	
    return Plugin_Continue;
}

stock OriginalJump(const any:client)
{
	g_iJumps[client]++;
}

stock Landed(const any:client)
{
	g_iJumps[client] = 0;
}

stock ReJump(const any:client)
{
	if ( 1 <= g_iJumps[client] <= player_b_jumpx[client])
	{
		g_iJumps[client]++;
		decl Float:vVel[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", vVel);
		vVel[2] = 250.0;
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, vVel);
	}
}

public UTIL_Teleport(client,Float:distance)
{	
	new bool:success = Teleport(client,distance);
	if(success)
	{
		//new Float:cooldown=GetConVarFloat(ultCooldownCvar);
		//new Float:PlayerPos[3];
		//GetClientAbsOrigin(client, PlayerPos);
		//TE_SetupSparks(PlayerPos, NULL_VECTOR, 2500, 500);
		//TE_SendToAll();
		player_b_blink_timer[client]=GetGameTime();
		CreateBlinkParticle(client, "firework_crate_ground_low_04");
	}
}

bool:Teleport(client,distance)
{
    if(!inteleportcheck[client])
    {
		new Float:angle[3];
        GetClientEyeAngles(client,angle);
        new Float:endpos[3];
        new Float:startpos[3];
        GetClientEyePosition(client,startpos);
        new Float:dir[3];
        GetAngleVectors(angle, dir, NULL_VECTOR, NULL_VECTOR);
        
        ScaleVector(dir, distance);
        
        AddVectors(startpos, dir, endpos);
        
        GetClientAbsOrigin(client,oldpos[client]);
        
        
        ClientTracer=client;
        TR_TraceRayFilter(startpos,endpos,MASK_ALL,RayType_EndPoint,AimTargetFilter);
        TR_GetEndPosition(endpos);
        
        new Float:distanceteleport=GetVectorDistance(startpos,endpos);
        if(distanceteleport<200.0){
            new String:buffer[100];
            Format(buffer, sizeof(buffer), "%T", "Distance too short.", client);
            PrintHintText(client,buffer);
            return false;
        }
        GetAngleVectors(angle, dir, NULL_VECTOR, NULL_VECTOR);///get dir again
        ScaleVector(dir, distanceteleport-33.0);
        
        AddVectors(startpos,dir,endpos);
        emptypos[0]=0.0;
        emptypos[1]=0.0;
        emptypos[2]=0.0;
        
        endpos[2]-=30.0;
        getEmptyLocationHull(client,endpos);
        
        if(GetVectorLength(emptypos)<1.0)
		{
            new String:buffer[100];
            Format(buffer, sizeof(buffer), "%T", "NoEmptyLocation", client);
            PrintHintText(client,buffer);
            return false; //it returned 0 0 0
        }
        
        
        TeleportEntity(client,emptypos,NULL_VECTOR,NULL_VECTOR);
        //EmitSoundToAll("blink.mp3",client);
        
        
        
        teleportpos[client][0]=emptypos[0];
        teleportpos[client][1]=emptypos[1];
        teleportpos[client][2]=emptypos[2];
        
        inteleportcheck[client]=true;
        CreateTimer(0.14,checkTeleport,client);
        return true;
	}
	
	return false;
}

public Action:checkTeleport(Handle:h,any:client)
{
    inteleportcheck[client]=false;
    new Float:pos[3];
    
    GetClientAbsOrigin(client,pos);
    
    if(GetVectorDistance(teleportpos[client],pos)<0.001)//he didnt move in this 0.1 second
    {
        TeleportEntity(client,oldpos[client],NULL_VECTOR,NULL_VECTOR);
        PrintHintText(client,"%T","CantTeleportHere",client);
	}
	
    return Plugin_Continue;
}

public bool:AimTargetFilter(entity,mask)
{
    return !(entity==ClientTracer);
}

public bool:getEmptyLocationHull(client,Float:originalpos[3])
{
	new Float:mins[3];
	new Float:maxs[3];
	GetClientMins(client,mins);
	GetClientMaxs(client,maxs);

	new absincarraysize=sizeof(absincarray);

	new limit=5000;
	for(new x=0;x<absincarraysize;x++){
		if(limit>0){
			for(new y=0;y<=x;y++){
				if(limit>0){
					for(new z=0;z<=y;z++){
						new Float:pos[3]={0.0,0.0,0.0};
						AddVectors(pos,originalpos,pos);
						pos[0]+=float(absincarray[x]);
						pos[1]+=float(absincarray[y]);
						pos[2]+=float(absincarray[z]);
						
						TR_TraceHullFilter(pos,pos,mins,maxs,MASK_SOLID,CanHitThis,client);
						//new ent;
						if(!TR_DidHit(_))
						{
							AddVectors(emptypos,pos,emptypos); ///set this gloval variable
							limit=-1;
							break;
						}
						
						if(limit--<0){
							break;
						}
					}
					
					if(limit--<0){
						break;
					}
				}
			}
			
			if(limit--<0){
				break;
			}
			
		}
		
	}
	return true;
}

public bool:CanHitThis(entityhit, mask, any:data)
{
    if(entityhit == data )
    {// Check if the TraceRay hit the itself.
        return false; // Don't allow self to be hit, skip this result
    }
    return true; // It didn't hit itself
}

public CreateKnife(client)
{
	new slot_knife = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
	new knife = CreateEntityByName("smokegrenade_projectile");

	if(knife == -1 || !DispatchSpawn(knife))
	{
		return;
	}

	// owner
	new team = GetClientTeam(client);
	SetEntPropEnt(knife, Prop_Send, "m_hOwnerEntity", client);
	SetEntPropEnt(knife, Prop_Send, "m_hThrower", client);
	SetEntProp(knife, Prop_Send, "m_iTeamNum", team);

	// player knife model
	new String:model[PLATFORM_MAX_PATH];
	if(slot_knife != -1)
	{
		GetEntPropString(slot_knife, Prop_Data, "m_ModelName", model, sizeof(model));
		if(ReplaceString(model, sizeof(model), "v_knife_", "w_knife_", true) != 1)
		{
			model[0] = '\0';
		}
		else if(game == Engine_CSGO && ReplaceString(model, sizeof(model), ".mdl", "_dropped.mdl", true) != 1)
		{
			model[0] = '\0';
		}
	}

	if(!FileExists(model, true))
	{
		Format(model, sizeof(model), "%s", game == Engine_CSS ? "models/weapons/w_knife_t.mdl": team == CS_TEAM_T ? "models/weapons/w_knife_default_t_dropped.mdl":"models/weapons/w_knife_default_ct_dropped.mdl");
	}
	
	EmitSoundToAll("weapons/knife/knife_deploy.wav", client);

	// model and size
	SetEntProp(knife, Prop_Send, "m_nModelIndex", PrecacheModel(model));
	SetEntPropFloat(knife, Prop_Send, "m_flModelScale", 1.0);

	// knive elasticity
	SetEntPropFloat(knife, Prop_Send, "m_flElasticity", 0.2);
	// gravity
	SetEntPropFloat(knife, Prop_Data, "m_flGravity", 1.5);


	// Player origin and angle
	new Float:origin[3], Float:angle[3];
	GetClientEyePosition(client, origin);
	GetClientEyeAngles(client, angle);

	// knive new spawn position and angle is same as player's
	new Float:pos[3];
	GetAngleVectors(angle, pos, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(pos, 50.0);
	AddVectors(pos, origin, pos);

	// knive flying direction and speed/power
	new Float:player_velocity[3], Float:velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", player_velocity);
	GetAngleVectors(angle, velocity, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(velocity, 850.0);
	AddVectors(velocity, player_velocity, velocity);

	// spin knive
	new Float:spin[] = {4000.0, 0.0, 0.0};
	SetEntPropVector(knife, Prop_Data, "m_vecAngVelocity", spin);

	// Stop grenade detonate and Kill knive after 1 - 30 sec
	SetEntProp(knife, Prop_Data, "m_nNextThinkTick", -1);
	//new String:buffer[25];
	//Format(buffer, sizeof(buffer), "!self,Kill,,%0.1f,-1", 20.0);
	//DispatchKeyValue(knife, "OnUser1", buffer);
	//AcceptEntityInput(knife, "FireUser1");

	// trail effect
	/*
		new color[4] = {255, ...}; 
		if(game == Engine_CSS)
		{
			TE_SetupBeamFollow(knife, PrecacheModel("sprites/bluelaser1.vmt"),	0, Float:0.5, Float:8.0, Float:1.0, 0, color);
		}
		else
		{
			TE_SetupBeamFollow(knife, PrecacheModel("effects/blueblacklargebeam.vmt"),	0, Float:0.5, Float:1.0, Float:0.1, 0, color);
		}
		TE_SendToAll();
	*/
	
	// Throw knive!
	TeleportEntity(knife, pos, angle, velocity);
	SDKHookEx(knife, SDKHook_Touch, KnifeHit);

	PushArrayCell(g_hThrownKnives, EntIndexToEntRef(knife));
	player_knife[client]--;
	decl String:hinttext[200];
	Format(hinttext, sizeof(hinttext), "%T", "-knife ({int})", client,player_knife[client]);
	PrintHintText(client, hinttext);
}

public command_arrow(client)
{
	if (IsPlayerAlive(client))
	{
		new bolt = CreateEntityByName("env_sprite_oriented");
		new smokeEnt = CreateEntityByName("smokegrenade_projectile");
		player_fireball_sprite[smokeEnt] = bolt;
		if(IsValidEntity(bolt) && IsValidEntity(smokeEnt))
		{
			new Float:Clientpos[3];
			GetClientAbsOrigin(client,Clientpos);
			
			DispatchKeyValue(bolt, "model", "materials/sprites/diablo/crossbolt.vmt");  
			
			DispatchKeyValue(bolt, "spawnflags", "1");
			DispatchKeyValue(bolt, "rendermode", "5");
			DispatchKeyValue(bolt, "scale", "0.1");
			
			SetEntPropEnt(bolt, Prop_Send, "m_hOwnerEntity", client);
			SetEntPropEnt(smokeEnt, Prop_Send, "m_hOwnerEntity", client);
			
			DispatchKeyValue(smokeEnt, "spawnflags", "1");
			DispatchKeyValue(smokeEnt, "rendermode", "0");
			DispatchKeyValue(smokeEnt, "renderfx", "0");
			DispatchKeyValue(smokeEnt, "rendercolor", "0 0 0");
			DispatchKeyValue(smokeEnt, "renderamt", "0");
			DispatchKeyValue(smokeEnt, "scale", "0.3");

			Clientpos[2] = Clientpos[2]+60;
			decl Float:vecOrigin[3];
			decl Float:vecAngles[3];
			decl Float:vecVeloc[3];
			decl Float:end[3];
			
			GetClientEyePosition(client, vecOrigin);
			GetClientEyeAngles(client, vecAngles);
			
			AddInFrontOf(vecOrigin, vecAngles, 40.0, vecOrigin);
			
			GetAngleVectors(vecAngles, vecVeloc, end, NULL_VECTOR);
			ScaleVector(end, 5.0);
			AddVectors(vecOrigin, end, vecOrigin);
			
			ScaleVector(vecVeloc, 1500.0);
			decl Float:baseVel[3];
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", baseVel);
			AddVectors(vecVeloc, baseVel, vecVeloc);
			DispatchSpawn(smokeEnt);
			DispatchSpawn(bolt);
			SetEntityRenderMode(smokeEnt, RENDER_NONE);
			SetEntityMoveType(smokeEnt, MOVETYPE_FLY);
			
			// knive elasticity
			SetEntPropFloat(smokeEnt, Prop_Send, "m_flElasticity", 0.0);
			// gravity
			SetEntPropFloat(smokeEnt, Prop_Data, "m_flGravity", 0.0);
			SetEntProp(smokeEnt, Prop_Data, "m_nNextThinkTick", -1);
			SetVariantString("!activator");
			AcceptEntityInput(bolt, "SetParent", smokeEnt);
			EmitAmbientSound( arrowsnd, Clientpos, _, SNDLEVEL_TRAIN  );
			TeleportEntity(smokeEnt, vecOrigin, vecAngles, vecVeloc);
			SDKHook(smokeEnt, SDKHook_Touch, OnStartTouchCross);
		}
	}
}

public OnStartTouchCross(caller, other)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(owner!=other)
	{
		
		new ignore = 0;
		if(0 < other <= MaxClients)
		{
			if(GetClientTeam(other) != GetClientTeam(owner))
			{
				SetVariantString("csblood");
				AcceptEntityInput(caller, "DispatchEffect");
				
				new Float:damagePosition[3];

				GetEntPropVector(caller, Prop_Data, "m_vecOrigin", damagePosition);
				
				new Float:damage = (player_intelligence[owner] * 2.0) + 120.0;
				damage = damage - player_dextery[other]*2.0;
				
				// create damage
				DealCustomDamage(other, owner, RoundToFloor(damage), "arrow");
				
				new color[] = {255, 0, 0, 255};
				new Float:dir[3];

				TE_SetupBloodSprite(damagePosition, dir, color, 1, PrecacheDecal("sprites/blood.vmt"), PrecacheDecal("sprites/blood.vmt"));
				TE_SendToAll(0.0);
			}
		}
		else
		{
			if(IsValidEntity(other))
			{
				new String:classname[64];
				GetEntityClassname(other, classname, sizeof(classname));
				if(StrEqual(classname, "func_breakable", false))
				{
					AcceptEntityInput(other, "Break");
				}
				if(StrEqual(classname, "prop_door_rotating", false))
				{
					AcceptEntityInput(other, "Kill");
				}
				if((StrContains(classname, "trigger", false) == 0) || (StrContains(classname, "info_", false) == 0) || (StrContains(classname, "env_", false) == 0) || (StrContains(classname, "game_", false) == 0) || (StrContains(classname, "logic_", false) == 0)  || (StrContains(classname, "point_", false) == 0) || StrEqual(classname, "func_buyzone", false) || StrEqual(classname, "func_bomb_target", false) || StrEqual(classname, "func_hostage_rescue", false) || StrEqual(classname, "func_no_defuse", false) || StrEqual(classname, "func_illusionary", false))
				{
					ignore = 1;
				}
			}
		}
		if(ignore == 0)
		{
			AcceptEntityInput(caller, "DispatchEffect");
			AcceptEntityInput(caller, "Kill");
			AcceptEntityInput(player_fireball_sprite[caller], "Kill");
		}
	}
}

public command_bow(client)
{
	if(bow[client] == 0)
	{
		bowdelay[client] = GetGameTime();
		FPVMI_AddViewModelToClient(client, "weapon_knife", g_crossModel); // add custom view model to the player
		FPVMI_AddWorldModelToClient(client, "weapon_knife", g_wcrossModel);
		bow[client] = 1;
	}
	else
	{
		FPVMI_RemoveViewModelToClient(client, "weapon_knife");
		FPVMI_RemoveWorldModelToClient(client, "weapon_knife");
		new zeus = GetPlayerWeaponSlot(client, 2);

		if(zeus != -1) 
		{
			new taser = GivePlayerItem(client, "weapon_taser");
			EquipPlayerWeapon(client, taser);
			RemovePlayerItem(client, taser);
			RemoveEdict(taser);
		}
		else
		{
			RemovePlayerItem(client, zeus);
			RemoveEdict(zeus);
			new taser = GivePlayerItem(client, "weapon_taser");
			EquipPlayerWeapon(client, taser);
			RemovePlayerItem(client, taser);
			RemoveEdict(taser);
			GivePlayerItem(client, "weapon_taser");
		}
		CreateTimer(0.1, SwitchCrow, client);
		bow[client] = 0;
	}
}

public Action:SwitchCrow(Handle:timer, any:client)
{
	ClientCommand(client, "slot3"); 
}

public OnEntityDestroyed(entity)
{
	if(!IsValidEdict(entity))
	{
		return;
	}

	new index = FindValueInArray(g_hThrownKnives, EntIndexToEntRef(entity));
	if(index != -1) RemoveFromArray(g_hThrownKnives, index);
}

public Action:KnifeHit(knife, other)
{
	if(0 < other <= MaxClients) // Hits player index
	{
		new victim = other;

		SetVariantString("csblood");
		AcceptEntityInput(knife, "DispatchEffect");
		AcceptEntityInput(knife, "Kill");

		new attacker = GetEntPropEnt(knife, Prop_Send, "m_hThrower");
		new inflictor = GetPlayerWeaponSlot(attacker, CS_SLOT_KNIFE);

		if(inflictor == -1)
		{
			inflictor = attacker;
		}

		new Float:victimeye[3];
		GetClientEyePosition(victim, victimeye);

		new Float:damagePosition[3];
		new Float:damageForce[3];

		GetEntPropVector(knife, Prop_Data, "m_vecOrigin", damagePosition);
		GetEntPropVector(knife, Prop_Data, "m_vecVelocity", damageForce);

		if(GetVectorLength(damageForce) == 0.0) // knife movement stop
		{
			return;
		}

		// Headshot - shitty way check it, clienteyeposition almost player back...
		new Float:distance = GetVectorDistance(damagePosition, victimeye);
		g_bHeadshot[attacker] = distance <= 20.0;

		// damage values and type
		new Float:damage[2];
		//Assassin damage TODO
		damage[0] = 20.0;
		damage[1] = 50.0;
		new dmgtype = game == Engine_CSS ? DMG_BULLET|DMG_NEVERGIB:DMG_SLASH|DMG_NEVERGIB;
		//new dmgtype = game == DMG_BULLET|DMG_NEVERGIB;

		if(g_bHeadshot[attacker])
		{
			dmgtype |= (1 << 30);
		}

		// create damage
		SDKHooks_TakeDamage(victim, inflictor, attacker,
		g_bHeadshot[attacker] ? damage[1]:damage[0],
		dmgtype, knife, damageForce, damagePosition);

		// blood effect
		new color[] = {255, 0, 0, 255};
		new Float:dir[3];

		TE_SetupBloodSprite(damagePosition, dir, color, 1, PrecacheDecal("sprites/blood.vmt"), PrecacheDecal("sprites/blood.vmt"));
		TE_SendToAll(0.0);

		// ragdoll effect
		new ragdoll = GetEntPropEnt(victim, Prop_Send, "m_hRagdoll");
		if(ragdoll != -1)
		{
			ScaleVector(damageForce, 50.0);
			damageForce[2] = FloatAbs(damageForce[2]); // push up!
			SetEntPropVector(ragdoll, Prop_Send, "m_vecForce", damageForce);
			SetEntPropVector(ragdoll, Prop_Send, "m_vecRagdollVelocity", damageForce);
		}
	}
	else if(FindValueInArray(g_hThrownKnives, EntIndexToEntRef(other)) != -1) // knives collide
	{
		SDKUnhook(knife, SDKHook_Touch, KnifeHit);
		SDKHook(knife, SDKHook_Touch, KnifePickup);
		new Float:pos[3], Float:dir[3];
		GetEntPropVector(knife, Prop_Data, "m_vecOrigin", pos);
		TE_SetupArmorRicochet(pos, dir);
		TE_SendToAll(0.0);

		DispatchKeyValue(knife, "OnUser1", "!self,Kill,,1.0,-1");
		AcceptEntityInput(knife, "FireUser1");
	}
	else
	{
		if(IsValidEntity(other))
		{
			//new String:targetname[60];
			//GetEntPropString(other, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
			//if(StrEqual(targetname, "monkshield", false))
			//{
				new String:classname[64];
				GetEntityClassname(other, classname, sizeof(classname));
				if(StrEqual(classname, "func_breakable", false))
				{
					AcceptEntityInput(other, "Break");
					AcceptEntityInput(knife, "Kill");
				}
				if(StrEqual(classname, "prop_door_rotating", false))
				{
					AcceptEntityInput(other, "Kill");
					AcceptEntityInput(knife, "Kill");
				}
			//}
		}
	}
	
	

	new Float:vel[3];
	GetEntPropVector(knife, Prop_Data, "m_vecAbsVelocity", vel);
	new speed = RoundToFloor(GetVectorLength(vel));
	if(speed < 110)
	{
		SDKUnhook(knife, SDKHook_Touch, KnifeHit);
		
		new knife2 = CreateEntityByName("prop_physics_override");
		new owner = GetEntPropEnt(knife, Prop_Send, "m_hOwnerEntity");
		if(ValidPlayer(owner))
		{
			new String:model[PLATFORM_MAX_PATH];
			new team = GetClientTeam(owner);
			Format(model, sizeof(model), "%s", game == Engine_CSS ? "models/weapons/w_knife_t.mdl": team == CS_TEAM_T ? "models/weapons/w_knife_default_t_dropped.mdl":"models/weapons/w_knife_default_ct_dropped.mdl");
			SetEntityModel(knife2,model);
		}
		else
		{
			SetEntityModel(knife2,"models/weapons/w_knife_default_t_dropped.mdl");
		}
		if(knife2 == -1 || !DispatchSpawn(knife2))
		{
			return;
		}
		//decl String:m_ModelName[PLATFORM_MAX_PATH];
		//GetEntPropString(knife, Prop_Data, "m_nModelIndex", m_ModelName, sizeof(m_ModelName));
		
		new Float:position[3], Float:fAng[3];
		//GetEntPropString(knife, Prop_Data, "m_nModelIndex", m_ModelName, sizeof(m_ModelName));
		GetEntPropVector(knife, Prop_Send, "m_vecOrigin", position);
		GetEntPropVector(knife, Prop_Data, "m_angRotation", fAng);
		//DispatchKeyValue(knife2, "model", m_ModelName); 
		//SetEntProp(knife2, Prop_Send, "m_nModelIndex", PrecacheModel("models/weapons/w_knife_t.mdl"));
		TeleportEntity(knife2, position, fAng, NULL_VECTOR);
		
		AcceptEntityInput(knife, "Kill");
		SDKHook(knife2, SDKHook_Touch, KnifePickup);
	}
}

public Action:KnifePickup(knife, client)
{
	if(0 < client <= MaxClients) // Hits player index
	{
		SDKUnhook(knife, SDKHook_Touch, KnifePickup);
		AcceptEntityInput(knife, "Kill");
		player_knife[client]++;
		decl String:hinttext[200];
		Format(hinttext, sizeof(hinttext), "%T", "+knife ({int})", client,player_knife[client]);
		PrintHintText(client, hinttext);
	}
}

public PlayerJumpEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	new client=GetClientOfUserId(GetEventInt(event,"userid"));

	if(ValidPlayer(client,true))
	{
		//Paladin LJ
		if ((longjumps[client] > 0) && (player_induck[client] == 1))
		{
			new Float:velocity[3]={0.0,0.0,0.0};
			velocity[0]= GetEntDataFloat(client,m_vecVelocity_0);
			velocity[1]= GetEntDataFloat(client,m_vecVelocity_1);
			new Float:len=GetVectorLength(velocity);
			new Float:leapPower = 400.0; //Depend on intelligence?
			if(len>3.0)
			{
				longjumps[client]--;
				//PrintToChatAll("pre  vec %f %f %f",velocity[0],velocity[1],velocity[2]);
				ScaleVector(velocity,leapPower/len);
				
				//PrintToChatAll("post vec %f %f %f",velocity[0],velocity[1],velocity[2]);
				SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
				//Paladin jump sound?
				decl String:hinttext[200];
				Format(hinttext, sizeof(hinttext), "%T", "{int} jumps left", client,longjumps[client]);
				PrintHintText(client, hinttext);
				EmitSoundToAll(paladinsnd,client);
			}
		}
		if(player_induck[client] && player_b_froglegs[client])
		{
			if(GetGameTime() - player_b_froglegs_timer[client] >= 3.0)
			{
				player_b_froglegs_timer[client] = GetGameTime();
				
				new Float:velocity[3]={0.0,0.0,0.0};
				velocity[0]= GetEntDataFloat(client,m_vecVelocity_0);
				velocity[1]= GetEntDataFloat(client,m_vecVelocity_1);
				new Float:len=GetVectorLength(velocity);
				new Float:leapPower = 800.0; //Depend on intelligence?
				if(len>3.0)
				{
					//PrintToChatAll("pre  vec %f %f %f",velocity[0],velocity[1],velocity[2]);
					ScaleVector(velocity,leapPower/len);
					
					//PrintToChatAll("post vec %f %f %f",velocity[0],velocity[1],velocity[2]);
					SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
					//Paladin jump sound?
					EmitSoundToAll(paladinsnd,client);
				}
			}
		}
	}
}

new Float:vAngles1[3] = {90.0,90.0,0.0};
new Float:vAngles2[3] = {90.0,0.0,0.0};

public createBlockAiming(client)
{
	
	new Float:vOrigin[3];
	GetClientAbsOrigin(client,vOrigin);
	new Float:vAngles[3];
	GetClientEyeAngles(client, vAngles);
	//create the block
	if(vAngles[1]>45.0&&vAngles[1]<135.0)
	{
		vOrigin[0]+=34.0;
		vOrigin[1]+=34.0;
		make_shield(client,vOrigin,vAngles1,1);
	}
	else if(vAngles[1]<-45.0&&vAngles[1]>-135.0)
	{
		vOrigin[0]+=34.0;
		vOrigin[1]+=-34.0;
		make_shield(client,vOrigin,vAngles1,1);
	}
	else if(vAngles[1]>-45.0&&vAngles[1]<45.0)
	{
		vOrigin[0]+=34.0; //
		vOrigin[1]+=-34.0;
		make_shield(client,vOrigin,vAngles2,2);
	}
	else
	{
		vOrigin[0]+=-34.0; //
		vOrigin[1]+=-34.0;
		make_shield(client,vOrigin,vAngles2,2);
	}
}

public make_shield(client,Float:vOrigin[3],Float:vAngles[3],side)
{
	new fbreak = CreateEntityByName("func_breakable");
	//new fbreak4 = CreateEntityByName("func_breakable");
	//new ent4 = CreateEntityByName("func_breakable");
	new proppo = CreateEntityByName("prop_physics_override");
	//make sure entity was created successfully
	if (fbreak != -1)
	{
		//set block properties
		new String:StringHP[5];
		new HP = player_intelligence[client]*10;
		if(HP == 0)
		{
			Format(StringHP, sizeof(StringHP), "1");
		}
		else
		{
			Format(StringHP, sizeof(StringHP), "%d", HP);
		}
		
		DispatchKeyValue(proppo, "model", "models/diablomod.net/monk_shield.mdl");  
		DispatchKeyValue(proppo, "spawnflags", "8");   
		//DispatchKeyValue(fbreak6, "solid", "2");  
		DispatchKeyValue(proppo, "solid", "2");
		DispatchKeyValue(proppo, "targetname", "monkshield"); 
		DispatchKeyValue(fbreak, "targetname", "monkshield"); 
		
		
		//DispatchKeyValue(ent, "nodamageforces", "1");
		//DispatchKeyValue(ent, "propdata", "15");
		DispatchKeyValue(fbreak, "propdata", "6" );
		DispatchKeyValue(fbreak, "material", "10" );
		//DispatchKeyValue(ent, "propdata", "15");
		//DispatchKeyValue(ent, "health", "1");
		//DispatchKeyValue(ent, "material", "2");
		//DispatchKeyValue(ent, "spawnflags", "2048"); 
		DispatchKeyValue(fbreak, "health", StringHP);
		//DispatchKeyValue(fbreak4, "health", "0");
		//DispatchKeyValue(fbreak6, "health", "0");
		//PrintToChatAll("%s",StringHP);
		//DispatchKeyValue(fbreak4, "propdata", "Stone.Huge");
		//DispatchKeyValue(fbreak6, "propdata", "Stone.Huge");
		//DispatchKeyValue(ent3, "health", StringHP);
		//DispatchKeyValue(ent4, "propdata", "8");
		//DispatchKeyValue(ent4, "health", StringHP);
		//DispatchKeyValue(ent, "spawnobject", "2");
		//DispatchKeyValue(ent, "material", "2");
		//DispatchKeyValue(ent, "spawnflags", "2048");  
		
		SetEntPropEnt(fbreak, Prop_Send, "m_hOwnerEntity", proppo);
		SetEntPropEnt(proppo, Prop_Send, "m_hOwnerEntity", client);
		//SetEntPropEnt(fbreak4, Prop_Send, "m_hOwnerEntity", proppo);
		//SetEntPropEnt(fbreak6, Prop_Send, "m_hOwnerEntity", proppo);
		
		DispatchSpawn(fbreak);
		//DispatchSpawn(fbreak4);
		DispatchSpawn(proppo);
		//DispatchSpawn(ent3);
		ActivateEntity(fbreak);
		//ActivateEntity(fbreak4);
		//ActivateEntity(fbreak6);
		
		new Float:vOrigin4[3]; 
		vOrigin4 = vOrigin;
		if(side==1)
		{
			vOrigin4[0]+=5.0;
			vOrigin4[1]+=5.0;
		}
		else
		{
			vOrigin4[0]+=5.0;
			vOrigin4[1]+=5.0;
		}
		
		TeleportEntity(fbreak, vOrigin, vAngles, NULL_VECTOR);
		TeleportEntity(proppo, vOrigin, vAngles, NULL_VECTOR);
		//TeleportEntity(fbreak4, vOrigin4, vAngles, NULL_VECTOR);
		//TeleportEntity(ent3, vOrigin, vAngles, NULL_VECTOR);
		//TeleportEntity(ent4, vOrigin, vAngles, NULL_VECTOR);

		SetEntityModel(fbreak, "models/diablomod.net/monk_shield.mdl"); //Need, but invisible, more here https://forums.alliedmods.net/showthread.php?t=129597
		//SetEntityModel(fbreak4, "models/diablomod.net/monk_shield.mdl"); //Need, but invisible, more here https://forums.alliedmods.net/showthread.php?t=129597
		//SetEntityModel(fbreak6, "models/diablomod.net/monk_shield.mdl"); //Need, but invisible, more here https://forums.alliedmods.net/showthread.php?t=129597
		//SetEntityModel(ent4, "models/diablomod.net/monk_shield.mdl"); //Need, but invisible, more here https://forums.alliedmods.net/showthread.php?t=129597
		
		new Float:minbounds[3], Float:maxbounds[3];
		
		if(side==1)
		{
			minbounds = {-64.0, -1.2, 0.0};  //1410 & 1403
			maxbounds = {1.2, 1.2, 64.0};
		}
		else
		{
			minbounds = {-1.2, 1.2, -64.0};  //1417 & 1424
			maxbounds = {1.2, 64.0, 64.0};
		}
		
		SetEntPropVector(fbreak, Prop_Send, "m_vecMins", minbounds);
		SetEntPropVector(fbreak, Prop_Send, "m_vecMaxs", maxbounds);
		//SetEntPropVector(fbreak4, Prop_Send, "m_vecMins", minbounds);
		//SetEntPropVector(fbreak4, Prop_Send, "m_vecMaxs", maxbounds);
		//SetEntPropVector(ent4, Prop_Send, "m_vecMaxs", maxbounds);
		//SetEntPropVector(ent4, Prop_Send, "m_vecMaxs", maxbounds);
		
		SetEntProp(fbreak, Prop_Send, "m_nSolidType", 2);
		//SetEntProp(fbreak4, Prop_Send, "m_nSolidType", 2);

		new enteffects = GetEntProp(fbreak, Prop_Send, "m_fEffects"); //block client errors
		enteffects |= 32; //block client errors
		SetEntProp(fbreak, Prop_Send, "m_fEffects", enteffects); //block client errors
		//enteffects = GetEntProp(fbreak4, Prop_Send, "m_fEffects"); //block client errors
		//enteffects |= 32; //block client errors
		//SetEntProp(fbreak4, Prop_Send, "m_fEffects", enteffects); //block client errors
		//enteffects = GetEntProp(ent4, Prop_Send, "m_fEffects"); //block client errors
		//enteffects |= 32; //block client errors
		//SetEntProp(ent4, Prop_Send, "m_fEffects", enteffects); //block client errors
		
		HookSingleEntityOutput(fbreak, "OnBreak", EntityOutput:OnBreak_Func_Monk, true); 
		//HookSingleEntityOutput(fbreak, "OnTakeDamage", EntityOutput:OnTakeDamageMonk, true); 
		SDKHook(fbreak, SDKHook_OnTakeDamagePost, Hook_OnTakeDamagePostMost);
		
		monk_shield[client]--;
		
		return 1;
	}
	return 0;
}

public OnBreak_Func_Monk(const String:output[], caller, activator, Float:delay) 
{ 
    new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	AcceptEntityInput(owner, "kill");
	SDKHook(caller, SDKHook_OnTakeDamagePost, Hook_OnTakeDamagePostMost);
}

public Hook_OnTakeDamagePostMost(shield, player, inflictor, Float:damage, damagetype, weapon, const Float:damageForce[3], const Float:damagePosition[3])
{ 
    new owner = GetEntPropEnt(shield, Prop_Send, "m_hOwnerEntity");
    new owner2 = GetEntPropEnt(owner, Prop_Send, "m_hOwnerEntity");
	if(player == owner2)
	{
		new iWeapon = inflictor;
		if(inflictor > 0 && inflictor <= MaxClients)
			iWeapon = GetEntPropEnt(inflictor, Prop_Send, "m_hActiveWeapon");
		
		if (iWeapon > 0 && IsValidEntity(iWeapon))
		{
			new String:sWeapon[64];
			GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
			if(StrEqual(sWeapon, "weapon_knife", false))
			{
				SDKHook(shield, SDKHook_OnTakeDamagePost, Hook_OnTakeDamagePostMost);
				AcceptEntityInput(shield, "kill");
				AcceptEntityInput(owner, "kill");
				monk_shield[player]++;
			}
		}
	}
	
	return Plugin_Continue;
}

public item_fireball(client, type)
{
//1 - by race, 2 by item
	
	if (IsPlayerAlive(client))
	{
		if(type == 1) fireball_collected[client]--;
		new fireball = CreateEntityByName("env_sprite_oriented");
		new smokeEnt = CreateEntityByName("smokegrenade_projectile");
		player_fireball_sprite[smokeEnt] = fireball;
		if(IsValidEntity(fireball) && IsValidEntity(smokeEnt))
		{
			new Float:Clientpos[3];
			GetClientAbsOrigin(client,Clientpos);
			
			DispatchKeyValue(fireball, "model", "materials/sprites/diablo/fireball.vmt");  
			
			DispatchKeyValue(fireball, "spawnflags", "1");
			DispatchKeyValue(fireball, "rendermode", "5");
			
			SetEntPropEnt(fireball, Prop_Send, "m_hOwnerEntity", client);
			SetEntPropEnt(smokeEnt, Prop_Send, "m_hOwnerEntity", client);
			
			DispatchKeyValue(smokeEnt, "spawnflags", "1");
			DispatchKeyValue(smokeEnt, "rendermode", "0");
			DispatchKeyValue(smokeEnt, "renderfx", "0");
			DispatchKeyValue(smokeEnt, "rendercolor", "0 0 0");
			DispatchKeyValue(smokeEnt, "renderamt", "0");

			decl Float:vecOrigin[3];
			decl Float:vecAngles[3];
			decl Float:vecVeloc[3];
			decl Float:end[3];
			
			GetClientEyePosition(client, vecOrigin);
			GetClientEyeAngles(client, vecAngles);
			
			AddInFrontOf(vecOrigin, vecAngles, 40.0, vecOrigin);
			
			GetAngleVectors(vecAngles, vecVeloc, end, NULL_VECTOR);
			ScaleVector(end, 5.0);
			AddVectors(vecOrigin, end, vecOrigin);
			
			ScaleVector(vecVeloc, 500.0);
			decl Float:baseVel[3];
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", baseVel);
			AddVectors(vecVeloc, baseVel, vecVeloc);
			/*GetClientEyeAngles( client, fAng );
			GetAngleVectors(fAng, fVel, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(fVel, 500.0);
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", fPVel);
			AddVectors(fVel, fPVel, fVel);*/
			Clientpos[2] = Clientpos[2]+50;
			DispatchSpawn(smokeEnt);
			DispatchSpawn(fireball);
			SetEntityRenderMode(smokeEnt, RENDER_NONE);
			SetEntityMoveType(smokeEnt, MOVETYPE_FLY);
			
			// knive elasticity
			SetEntPropFloat(smokeEnt, Prop_Send, "m_flElasticity", 0.0);
			// gravity
			SetEntPropFloat(smokeEnt, Prop_Data, "m_flGravity", 0.0);
			SetEntProp(smokeEnt, Prop_Data, "m_nNextThinkTick", -1);
			SetVariantString("!activator");
			AcceptEntityInput(fireball, "SetParent", smokeEnt);
			EmitAmbientSound( fireballsnd, Clientpos, _, SNDLEVEL_TRAIN  );
			TeleportEntity(smokeEnt, vecOrigin, vecAngles, vecVeloc);
			SDKHook(smokeEnt, SDKHook_Touch, OnStartTouch);
		}
	}
}

public OnStartTouch(caller, client)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(owner!=client)
	{		
		new ignore = 0;
		if(IsValidEntity(caller))
		{
			new String:classname[64];
			GetEntityClassname(client, classname, sizeof(classname));
			if((StrContains(classname, "trigger", false) == 0) || (StrContains(classname, "info_", false) == 0) || (StrContains(classname, "env_", false) == 0) || (StrContains(classname, "game_", false) == 0) || (StrContains(classname, "logic_", false) == 0)  || (StrContains(classname, "point_", false) == 0) || StrEqual(classname, "func_buyzone", false) || StrEqual(classname, "func_bomb_target", false) || StrEqual(classname, "func_hostage_rescue", false) || StrEqual(classname, "func_no_defuse", false) || StrEqual(classname, "func_illusionary", false))
			{
				ignore = 1;
			}
		}
		if(ignore == 0)
		{
			new explode = CreateEntityByName("env_explosion");
			new String:StringDamage[3];
			Format(StringDamage, sizeof(StringDamage), "%d", 55+player_intelligence[owner]);
			DispatchKeyValue(explode, "iMagnitude", StringDamage);
			DispatchKeyValue(explode, "targetname", "fireball");
			DispatchKeyValue(explode, "iMagnitude", StringDamage);
			if(player_b_fireball[owner] > 0)
			{
				Format(StringDamage, sizeof(StringDamage), "%d", player_b_fireball[owner]);
				DispatchKeyValue(explode, "iRadiusOverride", StringDamage);
			}
			else
			{
				DispatchKeyValue(explode, "iRadiusOverride", "150");
			}
			SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
			
			DispatchSpawn(explode);
			new Float:position[3];
			GetEntPropVector(caller, Prop_Send, "m_vecOrigin", position);
			TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
			AcceptEntityInput(explode, "Explode");
			CreateLevelupParticle(caller, "explosion_molotov_air");
			AcceptEntityInput(caller, "DispatchEffect");
			AcceptEntityInput(caller, "Kill");
			AcceptEntityInput(player_fireball_sprite[caller], "Kill");
			AcceptEntityInput(explode, "kill");
			EmitAmbientSound( fire_expsnd, position, _, SNDLEVEL_TRAIN  );
		}
	}
}

public Float:GetTimeDelay(client)
{
	new Float:time_delay = 8.0-(player_intelligence[client]/8.0);

	if(player_class[client] == 7) time_delay*=2.0;
	else if(player_class[client] == 1)
	{
		time_delay = 4.0 - (3.0 * (player_intelligence[client] / 50.0));
	}
	else if(player_class[client] == 4) time_delay*=2.0;
	else if(player_class[client] == 3) time_delay*=1.4;
	
	return time_delay;
}

public ShowCharging(client, start)
{
	decl String:chargetext[200];
	Format(chargetext, sizeof(chargetext), "%T", "Charging", client);
	new Float:maxtime = GetTimeDelay(client);
	//new maxtime2 = RoundToFloor(maxtime);
	new Float:curtime = cast_end[client]-GetGameTime();
	new Float:time = maxtime-curtime;
	if(start==1)
	{

	}
	else if(start==2)
	{
		Format(chargetext, sizeof(chargetext), "Charging <font color='#ff0000'>FAILED</font> - dont move<br>or change weapon. Knife only");
		PrintHintText(client, chargetext);
	}
	else if(time>0)
	{
		new Float:percent = time/maxtime;
		new num_lines = RoundToFloor(percent*50);
		new left_lines = 50-num_lines;
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#00ff00'>");
		for(new i=1;i<=num_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(lines, sizeof(lines), "</font>%s<font color='#ffffff'>", lines);
		for(new i=1;i<=left_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
	else
	{
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#00ff00'>", lines);
		for(new i=1;i<=50;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "Charging 100%");
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
}

public ShowNecrBar(client, team, Float:time, String:ragdollName[])
{
	new timeBar;
	new Float:timePassed = GetGameTime() - time;
	switch(timePassed)
	{
		case 0:
		{
			timeBar = 0;
		}
		case 1.0:
		{
			timeBar = 1;
		}
		case 2.0:
		{
			timeBar = 2;
		}
		case 3.0:
		{
			timeBar = 3;
		}
		default:
		{
			timeBar = -1;
		}
	}
	
	if(timeBar != -1)
	{
		decl String:chargetext[512], String:lines[300], String:color[10];
		if(GetClientTeam(client) == team)
		{
			Format(chargetext, sizeof(chargetext), "%T", "Reviving {player}", client, ragdollName);
			Format(color, sizeof(color), "00c3ff");
		}
		else
		{
			Format(chargetext, sizeof(chargetext), "%T", "Eating {player}", client, ragdollName);
			Format(color, sizeof(color), "ff0000");
		}
		switch(timeBar)
		{
			case 0:
			{
				Format(lines, sizeof(lines), "<font color='#ffffff'>", lines);
				for(new i=1;i<=50;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
				PrintHintText(client, chargetext);
			}
			case 1:
			{
				Format(lines, sizeof(lines), "<font color='#%s'>", color);
				for(new i=1;i<=16;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(lines, sizeof(lines), "%s</font><font color='#ffffff'>", lines);
				for(new i=1;i<=34;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
				PrintHintText(client, chargetext);
			}
			case 2:
			{
				Format(lines, sizeof(lines), "<font color='#%s'>", color);
				for(new i=1;i<=32;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(lines, sizeof(lines), "%s</font><font color='#ffffff'>", lines);
				for(new i=1;i<=18;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
				PrintHintText(client, chargetext);
			}
			case 3:
			{
				Format(lines, sizeof(lines), "<font color='#%s'>", color);
				for(new i=1;i<=50;i++)
				{
					Format(lines, sizeof(lines), "%s|", lines);
				}
				Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
				PrintHintText(client, chargetext);
			}
		}
	}
}

public ShowIllusBar(client)
{
	decl String:chargetext[200];
	Format(chargetext, sizeof(chargetext), "%T", "Invisibility", client);
	new Float:maxtime = 7.0;
	//new maxtime2 = RoundToFloor(maxtime);
	new Float:curtime = player_b_illusionist_end[client]-GetGameTime();
	new Float:time = maxtime-curtime;
	if(time>0)
	{
		new Float:percent = time/maxtime;
		new num_lines = RoundToFloor(percent*50);
		new left_lines = 50-num_lines;
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#00ff80'>");
		for(new i=1;i<=num_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(lines, sizeof(lines), "</font>%s<font color='#ffffff'>", lines);
		for(new i=1;i<=left_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
	else
	{
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#00ff80'>", lines);
		for(new i=1;i<=50;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "%T", "Invisibility", client);
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
}

public ShowGhostBar(client)
{
	decl String:chargetext[200];
	Format(chargetext, sizeof(chargetext), "%T", "Ghosting", client);
	new Float:maxtime = player_b_ghost[client]*1.0;
	//new maxtime2 = RoundToFloor(maxtime);
	new Float:curtime = player_b_ghost_end[client]-GetGameTime();
	new Float:time = maxtime-curtime;
	if(time>0)
	{
		new Float:percent = time/maxtime;
		new num_lines = RoundToFloor(percent*50);
		new left_lines = 50-num_lines;
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#ff00ff'>");
		for(new i=1;i<=num_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(lines, sizeof(lines), "</font>%s<font color='#ffffff'>", lines);
		for(new i=1;i<=left_lines;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
	else
	{
		new String:lines[200];
		Format(lines, sizeof(lines), "<font color='#ff00ff'>", lines);
		for(new i=1;i<=50;i++)
		{
			Format(lines, sizeof(lines), "%s|", lines);
		}
		Format(chargetext, sizeof(chargetext), "%T", "Ghosting", client);
		Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
		PrintHintText(client, chargetext);
	}
}

public ShowReloading(client)
{
	decl String:chargetext[200];
	Format(chargetext, sizeof(chargetext), "%T", "Reloading", client);
	new Float:maxtime = 4.25 - (player_intelligence[client]/25.0);
	//new maxtime2 = RoundToFloor(maxtime);
	new Float:time = GetGameTime()-bowdelay[client];
	if((maxtime-time)>0)
	{
		if(time>0)
		{
			new Float:percent = time/maxtime;
			new num_lines = RoundToFloor(percent*50);
			new left_lines = 50-num_lines;
			new String:lines[200];
			Format(lines, sizeof(lines), "<font color='#ff7700'>");
			for(new i=1;i<=num_lines;i++)
			{
				Format(lines, sizeof(lines), "%s|", lines);
			}
			Format(lines, sizeof(lines), "</font>%s<font color='#ffffff'>", lines);
			for(new i=1;i<=left_lines;i++)
			{
				Format(lines, sizeof(lines), "%s|", lines);
			}
			Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
			PrintHintText(client, chargetext);
		}
		else
		{
			new String:lines[200];
			Format(lines, sizeof(lines), "<font color='#ff7700'>", lines);
			for(new i=1;i<=50;i++)
			{
				Format(lines, sizeof(lines), "%s|", lines);
			}
			Format(chargetext, sizeof(chargetext), "%T", "Reloading", client);
			Format(chargetext, sizeof(chargetext), "%s<br>%s", chargetext, lines);
			PrintHintText(client, chargetext);
		}
	}
}

public Action:TimerEverySecond(Handle:timer,any:userid)
{
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true))
		{
			new flash = GetEntProp(client, Prop_Send, "m_fEffects");
			if(flash == 0)
			{
				flashlight_enabled[client] = false;
			}
			else
			{
				flashlight_enabled[client] = true;
			}
			if(player_colored[client] == 1)
			{
				if(player_colored_left[client] < 1)
				{
					ResetPlayerRGB(client);
					set_renderchange(client);
					player_colored[client] = 0;
					FlashScreen(client,RGBA_COLOR_BLUE,1.0,1.0,FFADE_OFF);
				}
				else
				{
					player_colored_left[client]--;
					if(IsOdd(player_colored_left[client]))
					{
						//FlashScreen(client,RGBA_COLOR_BLUE,1.0,1.0,FFADE_OFF);
					}
					else
					{
						FlashScreen(client,RGBA_COLOR_RED,1.0,1.0,FFADE_OFF);
					}
				}
			}
			if(moneyshield[client] == 1)
			{
				if(IsOdd(RoundToFloor(GetGameTime())))
				{
					if (GetCurrency(client)-250 > 0)
					{
						AddCurrency(client,-250);
					}
					else
					{
						moneyshield[client] = 0;
					}
				}
			}
			if(fallingtime[client]+5.0 > GetGameTime())
			{
				if(falling[client])
				{
					falling[client]=false;
					set_gravitychange(client);
				}
			}
			if(player_b_heal[client] != 0)
			{
				if(player_b_heal_timer[client] == 5)
				{
					add_health(client, player_b_heal[client]*5);
					player_b_heal_timer[client] = 0;
				}
				else
				{
					player_b_heal_timer[client]++;
				}
			}
			if(player_slowed[client] == 0)
			{
				player_slowed[client] = -1;
				set_speedchange(client);
			}
			if(player_slowed[client] != -1)
			{				
				player_slowed[client]--;
				SetEntDataFloat(client,m_OffsetSpeed,0.2,true);
			}
			if(player_b_fireshield_activated[client])
			{
				if(GetClientHealth(client) > 10)
				{
					SetPlayerColored(client,255,255,0,255,1);
					new Float:speeds;
					speeds = 1.0 + player_dextery[client]*0.006 + 0.2;
					SetEntDataFloat(client,m_OffsetSpeed,speeds,true);
					
					new Float:position[3];
					new Float:playerDistance, Float:OtherPlayerPos[3];
					GetClientAbsOrigin(client,position);
					for(new i=1;i<=MaxClients;i++)
					{
						if(ValidPlayer(i,true))
						{
							if(GetClientTeam(client) != GetClientTeam(i))
							{
								GetClientAbsOrigin(i,OtherPlayerPos);
								playerDistance = GetVectorDistance(position,OtherPlayerPos);
								if(playerDistance<250.0)
								{
									if (GetRandomInt(1,2) == 1)
									{
										FlashScreen(i,RGBA_COLOR_BLIND,1.0,1.0,FFADE_OFF);
									}
									DealCustomDamage(i, client, 45, "fireshield");
									SetEntDataFloat(i,m_OffsetSpeed,0.2,true);
									player_slowed[i]++;
								}
							}
						}
					}
					add_health(client, -10);
				}
				else
				{
					player_b_fireshield_activated[client] = false;
					set_speedchange(client);
					ResetPlayerRGB(client);
					set_renderchange(client);
				}
			}
			if(player_b_teamheal_using[client] != -1)
			{
				new target = GetClientAimTarget(client,false);
				if(ValidPlayer(target, true) && GetClientTeam(client) == GetClientTeam(target) && target == player_b_teamheal_using[client])
				{
					SetPlayerColored(player_b_teamheal_using[client],0,200,0,100,1);
					new Float:target_pos[3], Float:start_pos[3];
					GetClientAbsOrigin(client,start_pos);
					GetClientAbsOrigin(player_b_teamheal_using[client],target_pos);
					target_pos[2]+=30.0;
					TE_SetupBeamPoints(start_pos,target_pos,HaloSprite,HaloSprite,0,35,1.0,1.0,1.0,0,200.0,{0,255,0,60},40);
					TE_SendToAll();
				}
				else
				{
					ResetPlayerRGB(player_b_teamheal_using[client]);
					set_renderchange(player_b_teamheal_using[client]);
					player_b_teamheal_shielded[player_b_teamheal_using[client]] = -1;
					player_b_teamheal_using[client] = -1;
					used_item[client] = false;
				}
			}
			if(ghoststate[client] == 2)
			{
				if(GetGameTime()>player_b_ghost_end[client])
				{
					SetEntProp(client, Prop_Data, "m_takedamage", 2, 1);
					SetEntityMoveType(client, MOVETYPE_WALK);
					ghoststate[client] = 3;
					set_speedchange(client);
					if(IsPlayerStuck(client))
					{
						ForcePlayerSuicide(client);
						Diablo_ChatMessage(client,"%T","You're stucked. Ghost killed you.",client);
					}
				}
			}
			if(player_b_illusionist_activated[client])
			{
				if(GetGameTime()>player_b_illusionist_end[client])
				{
					player_b_illusionist_activated[client]=false;
				}
			}
			if(player_b_firetotem_left[client] > 0)
			{
				if(IsClientInGame(player_b_firetotem_owner[client]))
				{					
					player_b_firetotem_left[client]--;
					
					new damage;
					if (player_dextery[client] > 20)
						damage = 1;
					else if (player_dextery[client] > 15)
						damage = 2;
					else if (player_dextery[client] > 10)
						damage = 3;
					else
						damage = 4;
						
					CreateBlinkParticle(client, "env_fire_small");
					DealCustomDamage(client, player_b_firetotem_owner[client], damage, "totemfire");
				}
				else
				{
					player_b_firetotem_left[client] = 0;
				}
			}
		}
	}
	new item = -1;
	while ((item = FindEntityByClassname(item, "prop_physics")) != -1)
	{ 
		new String:targetname[60];
		GetEntPropString(item, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
		if(StrEqual(targetname, "totemheal", false))
		{
			if(GetGameTime()>TotemTimer[item])
			{
				AcceptEntityInput(item, "kill");
			}
			
			new owner = GetEntPropEnt(item, Prop_Send, "m_hOwnerEntity");
			
			if(!IsClientConnected(owner))
			{
				AcceptEntityInput(item, "kill");
			}
			else
			{	
				new Float:position[3];
				GetEntPropVector(item, Prop_Send, "m_vecOrigin", position);
				
				TE_SetupBeamRingPoint(position, 10.0, 300.0, WhiteSprite, HaloSprite, 0, 0, 1.0, 3.0, 0, {255,100,100,128}, 3, 0);
				TE_SendToAll();
				
				new Float:playerDistance, Float:OtherPlayerPos[3];
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true))
					{
						if(GetClientTeam(owner) == GetClientTeam(i))
						{
							GetClientAbsOrigin(i,OtherPlayerPos);
							playerDistance = GetVectorDistance(position,OtherPlayerPos);
							if(playerDistance<300.0)
							{
								add_health(i,player_b_heal[owner]);
							}
						}
					}
				}
				if((TotemTimer[item]-GetGameTime())<=1.0) SetEntityAlpha(item,100);
			}
		}
		if(StrEqual(targetname, "totemfire", false))
		{
			if(GetGameTime()>TotemTimer[item])
			{
				AcceptEntityInput(item, "kill");
			}
			
			new owner = GetEntPropEnt(item, Prop_Send, "m_hOwnerEntity");
			
			if(!IsClientConnected(owner))
			{
				AcceptEntityInput(item, "kill");
			}
			else
			{				
				
				new Float:position[3];
				GetEntPropVector(item, Prop_Send, "m_vecOrigin", position);
				
				new radius = player_b_firetotem[owner]*1.0;
				
				TE_SetupBeamRingPoint(position, 10.0, radius, WhiteSprite, HaloSprite, 0, 0, 1.0, 3.0, 0, {255,128,0,128}, 3, 0);
				TE_SendToAll();
				
				new Float:playerDistance, Float:OtherPlayerPos[3];
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true))
					{
						if(GetClientTeam(owner) != GetClientTeam(i))
						{
							GetClientAbsOrigin(i,OtherPlayerPos);
							playerDistance = GetVectorDistance(position,OtherPlayerPos);
							if(playerDistance<(player_b_firetotem[owner]+0.0))
							{
								new damage;
								if (player_dextery[i] > 20)
									damage = 1;
								else if (player_dextery[i] > 15)
									damage = 2;
								else if (player_dextery[i] > 10)
									damage = 3;
								else
									damage = 4;
									
								CreateBlinkParticle(i, "env_fire_small");
								DealCustomDamage(owner, i, damage, "totemfire");
								
								if(player_b_firetotem_left[i] > 0)
								{
									player_b_firetotem_left[i]+=1;
								}
								else
								{
									player_b_firetotem_left[i]=40;
									decl String:hinttext[200];
									Format(hinttext, sizeof(hinttext), "%T", "You are burning. Damage someone to make the fire stop!", i);
									PrintHintText(i, hinttext);
									Diablo_ChatMessage(i,"%T","You are burning. Damage someone to make the fire stop!",i);
								}
								
								player_b_firetotem_owner[i]=owner;
							}
						}
					}
				}
				if((TotemTimer[item]-GetGameTime())<=1.0) SetEntityAlpha(item,100);
			}
		}
	}
	item = -1;
	while ((item = FindEntityByClassname(item, "prop_physics_multiplayer")) != -1)
	{ 
		new String:targetname[60];
		GetEntPropString(item, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
		if(StrEqual(targetname, "totemheal", false))
		{
			if(GetGameTime()>TotemTimer[item])
			{
				AcceptEntityInput(item, "kill");
			}
			
			new owner = GetEntPropEnt(item, Prop_Send, "m_hOwnerEntity");
			
			if(!IsClientConnected(owner))
			{
				AcceptEntityInput(item, "kill");
			}
			else
			{	
				new Float:position[3];
				GetEntPropVector(item, Prop_Send, "m_vecOrigin", position);
				
				TE_SetupBeamRingPoint(position, 10.0, 300.0, WhiteSprite, HaloSprite, 0, 0, 1.0, 3.0, 0, {255,100,100,128}, 3, 0);
				TE_SendToAll();
				
				new Float:playerDistance, Float:OtherPlayerPos[3];
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true))
					{
						if(GetClientTeam(owner) == GetClientTeam(i))
						{
							GetClientAbsOrigin(i,OtherPlayerPos);
							playerDistance = GetVectorDistance(position,OtherPlayerPos);
							if(playerDistance<300.0)
							{
								add_health(i,player_b_heal[owner]);
							}
						}
					}
				}
				if((TotemTimer[item]-GetGameTime())<=1.0) SetEntityAlpha(item,100);
			}
		}
		if(StrEqual(targetname, "totemfire", false))
		{
			if(GetGameTime()>TotemTimer[item])
			{
				AcceptEntityInput(item, "kill");
			}
			
			new owner = GetEntPropEnt(item, Prop_Send, "m_hOwnerEntity");
			
			if(!IsClientConnected(owner))
			{
				AcceptEntityInput(item, "kill");
			}
			else
			{				
				
				new Float:position[3];
				GetEntPropVector(item, Prop_Send, "m_vecOrigin", position);
				
				new radius = player_b_firetotem[owner]*1.0;
				
				TE_SetupBeamRingPoint(position, 10.0, radius, WhiteSprite, HaloSprite, 0, 0, 1.0, 3.0, 0, {255,128,0,128}, 3, 0);
				TE_SendToAll();
				
				new Float:playerDistance, Float:OtherPlayerPos[3];
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true))
					{
						if(GetClientTeam(owner) != GetClientTeam(i))
						{
							GetClientAbsOrigin(i,OtherPlayerPos);
							playerDistance = GetVectorDistance(position,OtherPlayerPos);
							if(playerDistance<(player_b_firetotem[owner]+0.0))
							{
								new damage;
								if (player_dextery[i] > 20)
									damage = 1;
								else if (player_dextery[i] > 15)
									damage = 2;
								else if (player_dextery[i] > 10)
									damage = 3;
								else
									damage = 4;
									
								CreateBlinkParticle(i, "env_fire_small");
								DealCustomDamage(i, owner, damage, "totemfire");
								
								if(player_b_firetotem_left[i] > 0)
								{
									player_b_firetotem_left[i]+=1;
								}
								else
								{
									player_b_firetotem_left[i]=40;
									decl String:hinttext[200];
									Format(hinttext, sizeof(hinttext), "%T", "You are burning. Damage someone to make the fire stop!", i);
									PrintHintText(i, hinttext);
								}
								
								player_b_firetotem_owner[i]=owner;
							}
						}
					}
				}
				if((TotemTimer[item]-GetGameTime())<=1.0) SetEntityAlpha(item,100);
			}
		}
	}
}

/*public CreateFireTotemSprite(client)
{
	if(g_unClientSprite[client] != INVALID_ENT_REFERENCE)
		return;

	new m_unEnt = CreateEntityByName("env_sprite");
	if (IsValidEntity(m_unEnt))
	{
		decl String:iTarget[16];
		Format(iTarget, 16, "client%d", client);
		DispatchKeyValue(client, "targetname", iTarget);
		DispatchKeyValue(m_unEnt, "model", "materials/sprites/diablo/firetotem.vmt");
		DispatchSpawn(m_unEnt);

		new Float:m_flPosition[3];
		GetClientAbsOrigin(client, m_flPosition);
		m_flPosition[2]+=80.0;

		TeleportEntity(m_unEnt, m_flPosition, NULL_VECTOR, NULL_VECTOR);
	   
		SetVariantString("!activator");
		AcceptEntityInput(m_unEnt, "SetParent", client, m_unEnt, 0);
		
		//SetVariantString("francisco");
		SetVariantString(iTarget);
		//AcceptEntityInput(iTarget, "SetParent", Ent, Ent, 0);
	  
		g_unClientSprite[client] = EntIndexToEntRef(m_unEnt);
	}
}

public ResetFireTotemSprite(client)
{
	if(g_unClientSprite[client] == INVALID_ENT_REFERENCE)
		return;

	new m_unEnt = EntRefToEntIndex(g_unClientSprite[client]);
	g_unClientSprite[client] = INVALID_ENT_REFERENCE;
	if(m_unEnt == INVALID_ENT_REFERENCE)
		return;

	AcceptEntityInput(m_unEnt, "Kill");
}*/


public Event_OnPlayerSpawn(Handle:event, const String:error[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client <= 0)
		return;
	
	if(player_class[client] == 0)
	{
		TryToOpenChangerace(client);
	}
	
	flashbattery[client] = MAX_FLASH+player_intelligence[client]*5;
	flashbatteryMax[client] = MAX_FLASH+player_intelligence[client]*5;
	flashlight_enabled[client] = false;
	
	if(IsFakeClient(client))
	{
		ResetPlayer(client);
		player_class[client] = GetRandomInt(1,6);
		player_xp[client] = GetRandomInt(100,70200);
		player_lvl[client] = 1;
		LevelCalculated = 1;
		for (new i = 1; i <= sizeof(LevelXPTable)-1; i++ )
		{
			// User has enough XP to advance to the next level
			if ( player_xp[client] >= LevelXPTable[i])
			{
				LevelCalculated = i+1;
			}
			else
			{
				break;
			}
		}
		reset_skill(client);
		player_lvl[client] = LevelCalculated;
		player_totallvl[client] = 100;
		player_point[client] = (player_lvl[client]-1)*2;
		while(player_point[client] != 0)
		{
			if(player_point[client])
			{
				player_intelligence[client]++;
				player_point[client]--;
			}
			if(player_point[client])
			{
				player_strength[client]++;
				player_point[client]--;
			}
			if(player_point[client])
			{
				player_agility[client]++;
				player_point[client]--;
			}
			if(player_point[client])
			{
				player_dextery[client]++;
				player_point[client]--;
			}
		}
	}
	
	add_health(client,9999,true);
	
	player_colored_left[client] = 0;
	player_colored[client] = 0;
	
	invisible_cast[client]=0;
	
	GivePlayerKnifeIfHasNot(client);
	
	InitRaceSpawn(client);
	CacheClip(client);
	if(IsPlayerAlive(client) && player_lvl[client] < 6 && hint_disable[client] == 0)
	{
		CreateTimer(0.5, ShowPlayerHintRace, client);
	}
}

public Event_ItemPickup(Handle:event, const String:name[], bool:dontBroadcast)
{
	new String:sz_item[32];
	GetEventString(event, "item", sz_item, sizeof(sz_item));
	CacheClipSize(GetClientOfUserId(GetEventInt(event, "userid")), sz_item);
}

public CacheClip(client_index)
{
	new String:sz_classname[32];
	new entity_index = GetPlayerWeaponSlot(client_index, _:Slot_Primary);
	if (IsValidEdict(entity_index))
	{
		GetEdictClassname(entity_index, sz_classname, sizeof(sz_classname));
		CacheClipSize(client_index, sz_classname[7]);
	}
	entity_index = GetPlayerWeaponSlot(client_index, _:Slot_Secondary);
	if (IsValidEdict(entity_index))
	{
		GetEdictClassname(entity_index, sz_classname, sizeof(sz_classname));
		CacheClipSize(client_index, sz_classname[7]);
	}
}

stock CacheClipSize(client_index, const String:sz_item[])
{
	// Convert first 4 characters of item into an integer for fast comparison (big endian byte ordering)
	// sizeof(sz_item) must be >= 4
	new gun = (sz_item[0] << 24) + (sz_item[1] << 16) + (sz_item[2] << 8) + (sz_item[3]);

	if  (gun==0x6D616737)													// mag7
		g_PlayerPrimaryAmmo[client_index]=5;
	else if  (gun==0x786D3130 || gun==0x73617765)							// xm1014,  sawedoff
		g_PlayerPrimaryAmmo[client_index]=7;
	else if  (gun==0x6D330000 || gun==0x6E6F7661)							// m3,  nova
		g_PlayerPrimaryAmmo[client_index]=8;
	else if  (gun==0x73636F75 || gun==0x61777000 || gun==0x73736730)		// scout,  awp,  ssg08
		g_PlayerPrimaryAmmo[client_index]=10;
	else if  (gun==0x67337367 || gun==0x73636172)							// g3sg1,  scar20
		g_PlayerPrimaryAmmo[client_index]=20;
	else if  (gun==0x66616D61 || gun==0x756D7034)							// famas,  ump45
		g_PlayerPrimaryAmmo[client_index]=25;
	// ak47,  aug,  m4a1,  sg550,  mp5navy,  tmp,  mac10,  mp7,  mp9
	else if  (gun==0x616B3437 || gun==0x61756700 || gun==0x6D346131 || gun==0x73673535
		|| gun==0x6D70356E || gun==0x746D7000 || gun==0x6D616331 || gun==0x6D703700 || gun==0x6D703900)
		g_PlayerPrimaryAmmo[client_index]=30;
	else if  (gun==0x67616C69)												// galil
		g_PlayerPrimaryAmmo[client_index]=35;
	else if  (gun==0x70393000)												// p90
		g_PlayerPrimaryAmmo[client_index]=50;
	else if  (gun==0x62697A6F)												// bizon
		g_PlayerPrimaryAmmo[client_index]=64;
	else if  (gun==0x6D323439)												// m249
		g_PlayerPrimaryAmmo[client_index]=100;
	else if  (gun==0x6E656765)												// negev
		g_PlayerPrimaryAmmo[client_index]=150;
	else if  (gun==0x64656167)												// deagle
		g_PlayerSecondaryAmmo[client_index]=7;
	else if  (gun==0x75737000)												// usp
		g_PlayerSecondaryAmmo[client_index]=12;
	else if  (gun==0x70323238 || gun==0x686B7032 || gun==0x70323530)		// p228,  hkp2000,  p250
		g_PlayerSecondaryAmmo[client_index]=13;
	else if  (gun==0x676C6F63 || gun==0x66697665)							// glock,  fiveseven
		g_PlayerSecondaryAmmo[client_index]=20;
	else if  (gun==0x656C6974)												// elite
		g_PlayerSecondaryAmmo[client_index]=30;
	else if  (gun==0x74656339)												// tec9
		g_PlayerSecondaryAmmo[client_index]=32;
}

stock RefillAmmo(client_index)
{
	new clip;
	new entity_index = GetEntDataEnt2(client_index, g_hActiveWeapon);
	if (IsValidEdict(entity_index))
	{
		if (entity_index == GetPlayerWeaponSlot(client_index, _:Slot_Primary))
			clip = g_PlayerPrimaryAmmo[client_index];
		else if (entity_index == GetPlayerWeaponSlot(client_index, _:Slot_Secondary))
			clip = g_PlayerSecondaryAmmo[client_index];

		if (clip)
		{
			clip += 1;
			SetEntData(entity_index, g_iClip1, clip, 4, true);
		}
	}
}

public GivePlayerKnifeIfHasNot(client)
{
	new offset = Client_GetWeaponsOffset(client) - 4;
	
	new numWeaponsRemoved = 0;
	new hasknife = 0;
	for (new i=0; i < MAX_WEAPONS; i++) {
		offset += 4;

		new weapon = GetEntDataEnt2(client, offset);
		
		if (!Weapon_IsValid(weapon)) {
			continue;
		}
		
		if(Entity_ClassNameMatches(weapon, "weapon_knife")) {
			hasknife = 1;
			continue;
		}

		numWeaponsRemoved++;
	}
	
	if(!hasknife)
	{
		GivePlayerItem(client, "weapon_knife");
	}
}

public CheckHasGrenade(client)
{
	new offset = Client_GetWeaponsOffset(client) - 4;
	
	new numWeaponsRemoved = 0;
	new hasknife = 0;
	for (new i=0; i < MAX_WEAPONS; i++) {
		offset += 4;

		new weapon = GetEntDataEnt2(client, offset);
		
		if (!Weapon_IsValid(weapon)) {
			continue;
		}
		
		if(Entity_ClassNameMatches(weapon, "weapon_hegrenade")) {
			return 1;
		}

		numWeaponsRemoved++;
	}
	
	return 0;
}

public Event_OnPlayerDeath(Handle:event, const String:error[], bool:dontBroadcast)
{
	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(victim <= 0)
		return;
	
	if(attacker <= 0)
		return;

	new assister=GetClientOfUserId(GetEventInt(event,"assister"));

	if(victim!=attacker&&ValidPlayer(attacker))
	{
		if(GetClientTeam(attacker)!=GetClientTeam(victim))
		{
			decl String:weapon[64];
			GetEventString(event,"weapon",weapon,sizeof(weapon));
			if(assister>=0 && player_class[assister]>0)
			{
				new xp_award = GetConVarInt(g_hCVXPassist);
				decl String:assistaward[64];
				Format(assistaward, sizeof(assistaward), "%T", "assisting a kill", attacker);
				Give_Xp(assister,xp_award,assistaward);
			}
			if(player_b_teamheal_shielded[attacker] != -1)
			{
				new xp_award = GetConVarInt(g_hCVXPassist);
				decl String:assistaward[64];
				Format(assistaward, sizeof(assistaward), "%T", "assisting a kill", attacker);
				Give_Xp(player_b_teamheal_shielded[attacker],xp_award,assistaward);
			}
			award_kill(attacker,victim);
			
			award_item(attacker, 0);
			
			add_barbarian_bonus(attacker);
		}
	}
	
	add_respawn_bonus(victim);
	add_bonus_explode(victim);
	set_hideradar(victim, 0);
	
	
	new iRagdoll = 0;
	iRagdoll = GetEntPropEnt(victim, Prop_Send, "m_hRagdoll");
	if (iRagdoll > 0)
		AcceptEntityInput(iRagdoll, "Kill");
		
	/*issanta = 0;
	if(bIsVSHRunning)
	{
		santais = WhoIsSanta();
		if(victim == santais)
		{
			issanta = 1;
		}
	}*/
	
	//if(!IsFakeClient(victim) && !issanta)
	if(!IsFakeClient(victim))
	{
		new String:playermodel[128];
		GetClientModel(victim, playermodel, 128);
		
		new Float:origin[3], Float:angles[3], Float:velocity[3];
		
		GetClientAbsOrigin(victim, origin);
		GetClientAbsAngles(victim, angles);
		GetEntPropVector(victim, Prop_Data, "m_vecAbsVelocity", velocity);
		
		new iEntity = CreateEntityByName("prop_ragdoll");
		DispatchKeyValue(iEntity, "model", playermodel);
		
		// Prevent crash. If spawn isn't dispatched successfully,
		// TeleportEntity() crashes the server. This left some very
		// odd crash dumps, and usually only happened when 2 players
		// died inside each other in the same tick.
		// Thanks to -
		// 		Phoenix Gaming Network (pgn.site)
		// 		Prestige Gaming Organization
		
		if(blockSpawn == false)
		{
			blockSpawn = true;
			
			ActivateEntity(iEntity);
			if(DispatchSpawn(iEntity))
			{
				new Float:speed = GetVectorLength(velocity);
				if(speed >= 500)
					TeleportEntity(iEntity, origin, angles, NULL_VECTOR);
				else
					TeleportEntity(iEntity, origin, angles, velocity);
			}
			
			blockSpawn = false;
		}
		
		SetEntData(iEntity, g_iCollisionGroup, 2, 4, true);
		
		new String:name[MAX_NAME_LENGTH];
		GetClientName(victim, name, sizeof(name));
		new dPlayer[Ragdolls];
		dPlayer[Ragdollent] = EntIndexToEntRef(iEntity);
		dPlayer[victim2] = victim;
		Format(dPlayer[victimName], MAX_NAME_LENGTH, name);
		GetClientName(attacker, name, sizeof(name));
		dPlayer[attacker2] = attacker;
		dPlayer[RagdollTeam] = GetClientTeam(victim);
		Format(dPlayer[attackerName], MAX_NAME_LENGTH, name);
		dPlayer[gameTime] = GetGameTime();
		GetEventString(event, "weapon", dPlayer[weaponused], sizeof(dPlayer[weaponused]));
		
		PushArrayArray(g_hRagdollArray, dPlayer[0]);
		
		SetEntPropEnt(victim, Prop_Send, "m_hRagdoll", iEntity);
	}
	
	if(bow[victim] == 1)
	{
		FPVMI_RemoveViewModelToClient(victim, "weapon_knife");
		FPVMI_RemoveWorldModelToClient(victim, "weapon_knife");
		bow[victim] = 0;
	}
	
	if(moneyshield[victim] == 1)
	{
		moneyshield[victim] = 0;
	}
	
	player_b_fireshield_activated[victim] = 0;
	player_b_illusionist_activated[victim] = 0;
	ResetPlayerRGB(victim);
	
	if(player_b_firetotem_left[victim])
	{
		player_b_firetotem_left[victim] = 0;
	}
	//new String:sWeapon[64];
	// FIXME: Not all games might have this resource in the player_death event..
	//GetEventString(event, "weapon", sWeapon, sizeof(sWeapon));
	
	//Stats_PlayerKill(attacker, victim, sWeapon);
}

public Action:Event_ChangeName(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	if (!IsClientInGame(client)) return;

	new String:userName[MAX_NAME_LENGTH];
	GetEventString(event, "newname", userName, sizeof(userName));

 	new iSize = GetArraySize(g_hRagdollArray);

	if(iSize == 0)
		return;

	new dPlayer[Ragdolls];

	for(new i = 0;i < GetArraySize(g_hRagdollArray);i++)
	{
		GetArrayArray(g_hRagdollArray, i, dPlayer[0]);

		if(client == dPlayer[attacker2])
		{
			Format(dPlayer[attackerName], MAX_NAME_LENGTH, userName);
			SetArrayArray(g_hRagdollArray, i, dPlayer[0]);
		}
		else if(client == dPlayer[victim2])
		{
			Format(dPlayer[victimName], MAX_NAME_LENGTH, userName);
			SetArrayArray(g_hRagdollArray, i, dPlayer[0]);
		}
	}
}

public award_kill(attacker,victim)
{
	if(player_class[attacker]==0)
		return;
		
	new xp_award = GetConVarInt(g_hCVKillXP);
	
	if(player_class[attacker] == 2)
	{
		xp_award+=RoundToFloor(xp_award/4.0);
	}
	
	new String:killaward[64];
	Format(killaward, sizeof(killaward), "%T", "a kill", attacker);
	Give_Xp(attacker,xp_award,killaward);
	
	if(player_lvl[attacker]<player_lvl[victim])
	{
		new diff = player_lvl[victim]-player_lvl[attacker];
		if(diff > 1)
		{
			new diff_award = RoundToFloor(diff*0.5);
			Format(killaward, sizeof(killaward), "%T", "killing high level player", attacker);
			Give_Xp(attacker,diff_award,killaward);
		}
	}
}

public Give_Xp(client,amount,String:awardstring[])
{
	
	if(player_class[client]>0)
	{
		new min_players = GetConVarInt(g_hCVXPminplayers);
		new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
		if(current_plr>=min_players)
		{
			if(player_xp[client]+amount>0)
			{
				if(VIPGetPlayerdoublexp(client))
				{
					amount=amount*2;
				}
				
				if(!StrEqual(awardstring, "free bonus", false))
				{
					if(isAimMap) amount=RoundToFloor(amount/2.0);
				}
				player_xp[client]+=amount;
				if(player_lvl[client] < sizeof(LevelXPTable))
				{
					if(amount > 10000)
					{
						new last_level = player_lvl[client];
						player_lvl[client] = 1;
						LevelCalculated = 1;
						for (new i = 1; i <= sizeof(LevelXPTable)-1; i++ )
						{
							// User has enough XP to advance to the next level
							if ( player_xp[client] >= LevelXPTable[i])
							{
								LevelCalculated = i+1;
							}
							else
							{
								break;
							}
						}
						player_lvl[client] = LevelCalculated;
						player_point[client]=(player_lvl[client]-1)*2-player_intelligence[client]-player_strength[client]-player_dextery[client]-player_agility[client];
						decl String:sName[32];
						GetClientName(client, sName, sizeof(sName));
						decl String:racename[32], String:racenumtext[32];
						Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[client]);
						Format(racename, sizeof(racename), "%T", racenumtext, client);
						Diablo_ChatMessage(0,"%T","{player} has leveled {racename} to {amount}",client,sName,racename,player_lvl[client]);
						EmitSoundToAll(levelupsnd,client);
						CreateLevelupParticle(client, "firework_crate_ground_effect_fallback2");
						player_class_lvl[client][player_class[client]]=player_lvl[client];
						new total_diff = player_lvl[client] - last_level;
						player_totallvl[client] = player_totallvl[client]+total_diff;
					}
					else
					{
						if ((player_xp[client] > LevelXPTable[player_lvl[client]]))
						{
							player_lvl[client]+=1;
							player_point[client]+=2;
							decl String:sName[32];
							GetClientName(client, sName, sizeof(sName));
							decl String:racename[32], String:racenumtext[32];
							Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[client]);
							Format(racename, sizeof(racename), "%T", racenumtext, client);
							Diablo_ChatMessage(0,"%T","{player} has leveled {racename} to {amount}",client,sName,racename,player_lvl[client]);
							EmitSoundToAll(levelupsnd,client);
							CreateLevelupParticle(client, "firework_crate_ground_effect_fallback2");
							player_class_lvl[client][player_class[client]]=player_lvl[client];
							player_totallvl[client]++;
						}
					}					
				}
				
				if (player_xp[client] < LevelXPTable[player_lvl[client]-1])
				{
					player_lvl[client]-=1;
					player_point[client]-=2;
					Diablo_ChatMessage(client,"%T","You lost 1 level",client);
					player_class_lvl[client][player_class[client]]=player_lvl[client];
					player_totallvl[client]--;
				}
				Diablo_ChatMessage(client,"%T","You have gained {amount} XP for {award}",client, amount, awardstring);
			}
		}
		else
		{
			Diablo_ChatMessage(client, "%T", "No XP is given when less than {amount} players are playing", client, min_players);
		}
	}
}

public bool:TraceRayHitAnything(entity, mask)
{
	// Check if the ray hits an entity, and stop if it does
	if(entity > MaxClients)
		return true;
	return false;
}

public bool:TraceRayHitPlayer(entity, mask)
{
	// Check if the ray hits an entity, and stop if it does
	if(entity > 0 && entity <= MaxClients)
		return true;
	return false;
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3])
{
	if(IsPlayer(victim))
	{
		if(attacker == 0)
		{
			if(falling[victim])
			{
				damage=0.0;
				falling[victim]=false;
				set_gravitychange(victim);
				return Plugin_Changed;
			}
			else if((weapon == -1) && player_b_gravity[victim])
			{
				damage=0.0;
				falling[victim]=false;
				set_gravitychange(victim);
				add_bonus_stomp(victim);
				return Plugin_Changed;
			}
		}
		if(IsPlayer(attacker))
		{
			if(GetClientTeam(attacker) != GetClientTeam(victim))
			{
				if (ValidPlayer(victim) && GetEntityMoveType(victim) == MOVETYPE_NOCLIP)
				{
					SetEntityMoveType(victim, MOVETYPE_WALK);
				}
				if(player_b_firetotem_left[attacker] > 0) player_b_firetotem_left[attacker]=0;
				if(player_b_usingwind[attacker] == 1)
				{
					new iWeapon = inflictor;
					if(inflictor > 0 && inflictor <= MaxClients)
					{
						iWeapon = GetEntPropEnt(inflictor, Prop_Send, "m_hActiveWeapon");
						new String:sWeapon[64];
						if (iWeapon > 0 && IsValidEntity(iWeapon))
						{
							GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
							if(StrContains(sWeapon, "weapon_knife", false)==-1)
							{
								damage=0.0;
								return Plugin_Changed;
							}
						}
					}
				}
				if(player_b_sniper[attacker]>0)
				{
					new iWeapon = inflictor;
					if(inflictor > 0 && inflictor <= MaxClients)
					{
						iWeapon = GetEntPropEnt(inflictor, Prop_Send, "m_hActiveWeapon");
						new String:sWeapon[64];
						if (iWeapon > 0 && IsValidEntity(iWeapon))
						{
							GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
							if(StrContains(sWeapon, "weapon_ssg08", false)!=-1)
							{
								new rol1 = GetRandomInt(1,player_b_sniper[attacker]);
								if(rol1 == 1)
								{
									damage=500.0;
									return Plugin_Changed;
								}
							}
						}
					}
				}
				if(player_b_teamheal_using[victim] != -1)
				{
					damage=500.0;
					return Plugin_Changed;
				}
				if(player_b_illusionist_activated[victim])
				{
					damage=500.0;
					return Plugin_Changed;
				}
				if(player_b_teamheal_shielded[victim] != -1)
				{
					DealCustomDamage(attacker, victim, RoundToFloor(damage), "teamheal");
					damage=damage/2.0;
					return Plugin_Changed;
				}

				if(damagetype & DMG_FALL)
				{
				   if(falling[victim])
				   {
						damage=0.0;
						falling[victim]=false;
						set_gravitychange(victim);
						add_bonus_stomp(victim);
						return Plugin_Changed;
				   }
				}
				new iWeapon = inflictor;
				if(inflictor > 0 && inflictor <= MaxClients)
					iWeapon = GetEntPropEnt(inflictor, Prop_Send, "m_hActiveWeapon");
				new String:sWeapon[64];
				if (iWeapon > 0 && IsValidEntity(iWeapon))
				{
					GetEntityClassname(iWeapon, sWeapon, sizeof(sWeapon));
				}
				if (player_b_grenade[attacker] > 0 && (StrContains(sWeapon, "hegrenade", false) != -1) && player_b_fireshield[victim] == 0)
				{
					new roll = GetRandomInt(1,player_b_grenade[attacker]);
					if (roll == 1)
					{
						damage = 500.0;
						Diablo_ChatMessage(attacker,"%T","Item: You got HE kill chance.",attacker);
						return Plugin_Changed;
					}
				}
				
				if (damagetype & DMG_BULLET == 0 || // Damage was not from a bullet
					attacker < 1 ||
					attacker > MaxClients || // Attacker is not a player
					victim < 0 ||
					victim > MaxClients || // Victim is not a player
					!IsClientInGame(attacker) ||
					!IsClientInGame(victim) )
					return Plugin_Continue; // Allow damage to go through
				
				// Init variables
				decl Float:attackerloc[3], Float:bulletvec[3], Float:bulletang[3];
				GetClientEyePosition(attacker, attackerloc);
				MakeVectorFromPoints(attackerloc, damagePosition, bulletvec);
				GetVectorAngles(bulletvec, bulletang);
				
				// Traceray to victim
				TR_TraceRayFilter(attackerloc, bulletang, MASK_ALL, RayType_Infinite, TraceRayHitAnything); // Try to hit an entity
				new obstruction = TR_GetEntityIndex(); // Find first entity in traceray path

				// If traceray hit an entity beside the victim, then that entity is an obstruction
				if(obstruction != victim)
				{
					
					// Obstruction is a player
					if( obstruction > 0 &&
						obstruction <= MaxClients &&
						IsClientInGame(obstruction) )
					{
					if(IsValidEntity(obstruction))
						{
							GetEntityClassname(obstruction, sWeapon, sizeof(sWeapon));
						}
					}
					
					// Obstruction is not a player
					else
					{
						new String:targetname[60];
						GetEntPropString(obstruction, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
						if(IsValidEntity(obstruction))
						{
							GetEntityClassname(obstruction, sWeapon, sizeof(sWeapon));
						}
						if(StrEqual(targetname, "monkshield", false))
						{
							damage = 0.0; // Since we can only trace to a non-player entity if geometry penetration is disabled, stop the damage
							return Plugin_Changed;				
						}
						
					}
				}
			}
		}
	}
	
	return Plugin_Continue;
}

public Hook_OnTakeDamagePost(victim, attacker, inflictor, Float:damage, damagetype, weapon, const Float:damageForce[3], const Float:damagePosition[3])
{
	if (ValidPlayer(victim, true) && ghoststate[victim] == 2)
	{
		SetEntityMoveType(victim, MOVETYPE_NOCLIP);
		SetEntProp(victim, Prop_Data, "m_takedamage", 2, 1); //Not sure if necessary
	}
}

public Action:Command_Flashlight(client, const String:command[], argc)
{
	if(!IsClientInGame(client)) //If player is not in-game then ignore!
		return Plugin_Continue;

	if(!IsPlayerAlive(client)) //If player is not alive then continue the command.
		return Plugin_Continue;	
		
	if(player_class[client] != 1)
		return Plugin_Continue;

	ToggleFlashlight(client);

	return 	Plugin_Handled;
}

ToggleFlashlight(client) 
{	
	SetEntProp(client, Prop_Send, "m_fEffects", GetEntProp(client, Prop_Send, "m_fEffects") ^ 4);
	EmitSoundToAll("items/flashlight1.wav", client);
	
	new flash = GetEntProp(client, Prop_Send, "m_fEffects");
	if(flash == 0)
	{
		flashlight_enabled[client] = false;
	}
	else
	{
		flashlight_enabled[client] = true;
	}
}

public Event_BombPlantedEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	if(GetEventInt(event,"userid")>0)
	{
		new client=GetClientOfUserId(GetEventInt(event,"userid"));
		
		if(ValidPlayer(client,true))
		{
			new String:plantaward[64];
			new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
			new xp_award = current_plr * GetConVarInt(g_hCVXPplant);
			new team=GetClientTeam(client);
			for(new i=1;i<=MaxClients;i++)
			{
				 if(ValidPlayer(i,true)&&GetClientTeam(i)==team)
				 {
					Format(plantaward,sizeof(plantaward),"%T","planting the bomb",i);
					Give_Xp(i,xp_award,plantaward);
				}
			}
		}
	}
}

public Event_BombDefusedEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	if(GetEventInt(event,"userid")>0)
	{
		new client=GetClientOfUserId(GetEventInt(event,"userid"));
		
		if(ValidPlayer(client,true))
		{
			new String:defusaward[64];
			new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
			new xp_award = current_plr * GetConVarInt(g_hCVXPdefuse);
			new team=GetClientTeam(client);
			for(new i=1;i<=MaxClients;i++)
			{
				 if(ValidPlayer(i,true)&&GetClientTeam(i)==team)
				 {
					Format(defusaward,sizeof(defusaward),"%T","defusing the bomb",i);
					Give_Xp(i,xp_award,defusaward);
				}
			}
		}
	}
}

public PostThinkPost(client){
	if(player_class[client] == 7)
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits",0);
	}
	if(invisible_cast[client] == 1)
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits",0);
	}
	if((player_b_inv[client] < 21) && (player_b_inv[client] > 0))
	{
		SetEntProp(client, Prop_Send, "m_iAddonBits",0);
	}
}

public Event_HostageRescuedEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	if(GetEventInt(event,"userid")>0)
	{
		new client=GetClientOfUserId(GetEventInt(event,"userid"));
		
		if(ValidPlayer(client,true))
		{
			new String:plantaward[64];
			new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
			new xp_award = current_plr * GetConVarInt(g_hCVXPrescue);
			new team=GetClientTeam(client);
			for(new i=1;i<=MaxClients;i++)
			{
				 if(ValidPlayer(i,true)&&GetClientTeam(i)==team)
				 {
					Format(plantaward,sizeof(plantaward),"%T","rescuing a hostage",i);
					Give_Xp(i,xp_award,plantaward);
				}
			}
		}
	}
}

public Event_OnRoundEnd(Handle:event, const String:error[], bool:dontBroadcast)
{
	round_end = 1;
	SaveAllPlayers();
	
	new team=-1;
	team=GetEventInt(event,"winner");
	if(team>-1)
	{
		new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
		new xp_award = current_plr * GetConVarInt(g_hCVXPwinning);
		for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i)&&  GetClientTeam(i)==team)
			{
				new String:teamwinaward[64];
				Format(teamwinaward,sizeof(teamwinaward),"%T","being on the winning team",i);
				Give_Xp(i,xp_award,teamwinaward);
			}
		}
	}
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i,true))
		{
			set_hideradar(i, 0);
		}
	}
}

public DiabloGetReqXP(level)
{
	if(level > sizeof(LevelXPTable))
	{
		level = MAXLEVELXPDEFINED;
	}
	return LevelXPTable[level];
}

public ShowXP(client)
{
	if(player_class[client]==0)
	{
		//if(bXPLoaded[client])
		Diablo_ChatMessage(client,"%T","You must first select a race with changerace!",client);
		return;
	}

	decl String:racename[32], String:racenumtext[32];
	Format(racenumtext, sizeof(racenumtext), "RACE%d", player_class[client]);
	Format(racename, sizeof(racename), "%T", racenumtext, client);
	
	new reqlevel;
	
	if(player_lvl[client]<MAXLEVELXPDEFINED)
	{
		reqlevel = DiabloGetReqXP(player_lvl[client]);
		Diablo_ChatMessage(client,"%T","{racename} - Level {amount} - {amount} XP / {amount} XP",client,racename,player_lvl[client],player_xp[client],reqlevel);
	}
	else
	{
		Diablo_ChatMessage(client,"%T","{racename} - Level {amount} - {amount} XP",client,racename,player_lvl[client],player_xp[client]);
	}
}

public ShowItemNewbie(client)
{
	if(player_totallvl[client] < 25 && hint_disable[client] == 0)
	{
		if(player_item_id[client] > 0)
		{
			Diablo_ChatMessage(client,"%T","You have item, look",client);
		}
	}
}

public Action:Event_OnPlayerSay(client, args)
{
	decl String:sText[70];
	GetCmdArg(1,sText,sizeof(sText));
	TrimString(sText);
	
	if(StrEqual(sText, "diablo", false) || StrEqual(sText, "menu", false))
		DisplayMainMenu(client);
	else if(StrEqual(sText, "ii", false) || StrEqual(sText, "iteminfo", false))
	{
		iteminfo(client,2,1);
	}
	else if(StrEqual(sText, "drop", false))
	{
		dropitem(client);
	}
	else if(StrEqual(sText, "class", false) || StrEqual(sText, "changerace", false))
	{
		TryToOpenChangerace(client);
	}
	else if(StrEqual(sText, "xp", false) || StrEqual(sText, "showxp", false))
	{
		ShowXP(client);
	}
	else if(StrEqual(sText, "reset", false))
	{
		reset_skill(client);
	}
	else if(StrEqual(sText, "showskills", false) || StrEqual(sText, "skills", false))
	{
		showskills(client);
	}
	else if(StrEqual(sText, "rune", false) || StrEqual(sText, "shop", false))
	{
		DisplayShopMenu(client);
	}
	else if(StrEqual(sText, "who", false))
	{
		DisplayWho(client);
	}
	else if(StrEqual(sText, "wcs", false) || StrEqual(sText, "war3menu", false) || StrEqual(sText, "rpg", false))
	{
		Diablo_ChatMessage(client,"%T","This is not WCS",client);
	}
	else if(StrEqual(sText, "buyitem", false))
	{
		BuyItemCMD(client,0);
	}
	else if(StrEqual(sText, "zal", false))
	{
		ZalCMD(client,0);
	}
	else if(StrEqual(sText, "upgrade", false))
	{
		UpgradeCMD(client,0);
	}
	else if(StrEqual(sText, "!free", false))
	{
		FreeBonus(client);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:MenuCMD(client,args)
{
	DisplayMainMenu(client);
}

public Action:MenuItemInfo(client,args)
{
	iteminfo(client,2,1);
}

public Action:DropItem(client,args)
{
	dropitem(client);
}

public Action:MenuCMDclass(client,args)
{
	TryToOpenChangerace(client);
}

public Action:MenuCMDXP(client,args)
{
	ShowXP(client);
}

public Action:MenuCMDreset(client,args)
{
	reset_skill(client);
}

public Action:MenuCMDskills(client,args)
{
	showskills(client);
}

public Action:MenuCMDshop(client,args)
{
	DisplayShopMenu(client);
}

public Action:MenuCMDwho(client,args)
{
	DisplayWho(client);
}

public Action:UpgradeCMD(client,args)
{
	new currency = GetCurrency(client);
	if(player_item_id[client] > 0)
	{
		if(currency > 8999)
		{
			upgrade_item(client);
			AddCurrency(client, -9000);
		}
		else
		{
			Diablo_ChatMessage(client,"%T","Insufficient funds",client);
		}
	}
}

public Action:BuyItemCMD(client,args)
{
	new currency = GetCurrency(client);
	if(player_item_id[client] == 0)
	{
		if(currency > 4999)
		{
			award_item(client, 0);
			AddCurrency(client, -5000);
		}
		else
		{
			Diablo_ChatMessage(client,"%T","Insufficient funds",client);
		}
	}
	else
	{
		Diablo_ChatMessage(client,"%T","Already own item",client);
	}
}

public Action:ZalCMD(client,args)
{
	if(GetPlayerPremium(client))
	{
		new currency = GetCurrency(client);
		if(currency > 7499)
		{
			new xp_award = GetRandomInt(12,50);
			new String:killaward[64];
			Format(killaward, sizeof(killaward), "%T", "purchasing Zal", client);
			Give_Xp(client,xp_award,killaward);
			if(round_end)
			{
				player_spendmoney[client] += 7500;
				if(!player_spend_1_time[client]) 
				{
					player_lastmoney[client] = currency;
					player_spend_1_time[client] = 1;
				}
			}
			AddCurrency(client, -7500);
		}
		else
		{
			Diablo_ChatMessage(client,"%T","Insufficient funds",client);
		}
	}
	else
	{
		Diablo_ChatMessage(client,"%T","Buy VIP",client);
	}
}

//fetch using column string
stock DSQLPlayerInt(Handle:query,const String:columnname[]) //fech from query
{
	new column;
	SQL_FieldNameToNum(query,columnname,column);
	decl String:result[16];
	SQL_FetchString(query,column,result,sizeof(result));
	return StringToInt(result);
}
//fetch using column string
stock DSQLPlayerFloat(Handle:query,const String:columnname[]) //fech from query
{
	new column;
	SQL_FieldNameToNum(query,columnname,column);
	decl String:result[16];
	SQL_FetchString(query,column,result,sizeof(result));
	return StringToFloat(result);
}
//fetch using column string
stock DSQLPlayerString(Handle:query,const String:columnname[],String:out_buffer[],size_out) //fech from query
{
	new column;
	if(SQL_FieldNameToNum(query,columnname,column))
	{
		SQL_FetchString(query,column,out_buffer,size_out);
		return true;
	}
	return false;
}
/*
 * Closes the given hindle and sets it to INVALID_HANDLE.
 * 
 * @param handle   handle
 * @noreturn
 */
stock ClearHandle(&Handle:handle)
{
	if (handle != INVALID_HANDLE) {
		CloseHandle(handle);
		handle = INVALID_HANDLE;
	}
}

GetClientByPlayerID(iPlayerId)
{
	for(new i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i) && g_iDBPlayerUniqueID[i] == iPlayerId)
			return i;
	}
	return -1;
}

stock add_health(client,hp,bool:spawn=false)
{
	if(ValidPlayer(client))
	{
		new health = GetClientHealth(client);
		new m_health;
		if(player_item_id[client]==17)
		{
			m_health = 5;
		}
		else
		{
			m_health = race_heal[player_class[client]]+player_strength[client]*2;
		}
		if(spawn==true)
		{
			SetEntProp(client, Prop_Send, "m_iHealth", m_health, 1);
		}
		else
		{
			new new_health = health+hp;
			if(new_health > m_health)
			{
				if(player_item_id[client]==17 && hp>0)
				{
					SetEntProp(client, Prop_Send, "m_iHealth", 5, 1);
				}
				else if(player_item_id[client]!=17)
				{
					SetEntProp(client, Prop_Send, "m_iHealth", m_health, 1);
				}
			}
			else
			{
				if(player_item_id[client]==17 && hp>0)
				{
					SetEntProp(client, Prop_Send, "m_iHealth", new_health, 1);
				}
				else
				{
					SetEntProp(client, Prop_Send, "m_iHealth", new_health, 1);
				}
			}
		}
	}
}

stock Diablo_ChatMessage(client, const String:szMessage[], any:...)
{
	if (client == 0)
	{
		decl String:szBuffer[MAX_MESSAGE_LENGTH];
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i) && !IsFakeClient(i))
				{
				SetGlobalTransTarget(i);
				VFormat(szBuffer, sizeof(szBuffer), szMessage, 3);
				Format(szBuffer, sizeof(szBuffer), "%T%s", "[Diablomod]", i, szBuffer);
				CPrintToChat(i, szBuffer);
			}
		}
	}
	else
	{
		decl String:szBuffer[MAX_MESSAGE_LENGTH];
		SetGlobalTransTarget(client);
		VFormat(szBuffer, sizeof(szBuffer), szMessage, 3);
		Format(szBuffer, sizeof(szBuffer), "%T%s", "[Diablomod]", client, szBuffer);
		CPrintToChat(client, szBuffer);
	}
}

stock SetSafeMenuTitle(Handle:menu, const String:fmt[], any:...)
{
  decl String:menuTitle[4096];
  VFormat(menuTitle, sizeof(menuTitle), fmt, 3);
  
  SetMenuTitle(menu, menuTitle);
}

stock PlayersOnTeam(team) 
{
  new num;
  for (new x = 1; x <= MaxClients; x++) {
    if (IsClientInGame(x) && GetClientTeam(x) == team) {
      num++;
    }
  }
  return num;
}

stock CurrentLivePlayers() 
{
  new num;
  for (new x = 1; x <= MaxClients; x++) 
  {
    if(IsClientInGame(x))
	{
		if (IsPlayerAlive(x)) 
		{
		  num++;
		}
	}
  }
  return num;
}

stock CreateLevelupParticle(ent, String:particleType[])
{
	new particle = CreateEntityByName("info_particle_system");

	decl String:name[64];

	if (IsValidEdict(particle))
	{
		new Float:position[3];
		GetEntPropVector(ent, Prop_Send, "m_vecOrigin", position);
		TeleportEntity(particle, position, NULL_VECTOR, NULL_VECTOR);
		GetEntPropString(ent, Prop_Data, "m_iName", name, sizeof(name));
		DispatchKeyValue(particle, "targetname", "tf2particle");
		DispatchKeyValue(particle, "parentname", name);
		DispatchKeyValue(particle, "effect_name", particleType);
		DispatchSpawn(particle);
		SetVariantString(name);
		AcceptEntityInput(particle, "SetParent", particle, particle, 0);
		ActivateEntity(particle);
		AcceptEntityInput(particle, "start");
		CreateTimer(2.0, DeleteLevelupParticle, particle);
	}
}

stock CreateBlinkParticle(ent, String:particleType[])
{
	new particle = CreateEntityByName("info_particle_system");

	decl String:name[64];

	if (IsValidEdict(particle))
	{
		new Float:position[3];
		GetEntPropVector(ent, Prop_Send, "m_vecOrigin", position);
		position[2]+=30.0;
		TeleportEntity(particle, position, NULL_VECTOR, NULL_VECTOR);
		GetEntPropString(ent, Prop_Data, "m_iName", name, sizeof(name));
		DispatchKeyValue(particle, "targetname", "tf2particle");
		DispatchKeyValue(particle, "parentname", name);
		DispatchKeyValue(particle, "effect_name", particleType);
		DispatchSpawn(particle);
		SetVariantString(name);
		AcceptEntityInput(particle, "SetParent", particle, particle, 0);
		ActivateEntity(particle);
		AcceptEntityInput(particle, "start");
		decl String:Buffer[64];
        Format(Buffer, sizeof(Buffer), "Client%d", ent);
        DispatchKeyValue(ent, "targetname", Buffer);
        SetVariantString(Buffer);
        AcceptEntityInput(particle, "SetParent");  
		CreateTimer(1.0, DeleteLevelupParticle, particle);
	}
}

public Action:DeleteLevelupParticle(Handle:timer, any:particle)
{
	if (IsValidEntity(particle))
	{
		new String:classN[64];
		GetEdictClassname(particle, classN, sizeof(classN));
		if (StrEqual(classN, "info_particle_system", false))
		{
			RemoveEdict(particle);
		}
	}
}

stock bool:ValidPlayer(client,bool:check_alive=false,bool:alivecheckbyhealth=false) {
  if(client>0 && client<=MaxClients && IsClientConnected(client) && IsClientInGame(client))
  {
    if(check_alive && !IsPlayerAlive(client))
    {
      return false;
    }
    if(alivecheckbyhealth&&GetClientHealth(client)<1) {
      return false;
    }
    return true;
  }
  return false;
}

stock GetEntityAlpha(index)
{
  return GetEntData(index,m_OffsetClrRender+3,1);
}

stock SetEntityAlpha(index,alpha)
{
  new String:class[32];
  GetEntityNetClass(index, class, sizeof(class) );
  if(FindSendPropOffs(class,"m_nRenderFX")>-1){
    SetEntityRenderMode(index,RENDER_TRANSCOLOR);
    SetEntityRenderColor(index,GetPlayerR(index),GetPlayerG(index),GetPlayerB(index),alpha);
  }
}

stock GetPlayerR(index)
{
  return GetEntData(index,m_OffsetClrRender,1);
}

stock GetPlayerG(index)
{
  return GetEntData(index,m_OffsetClrRender+1,1);
}

stock GetPlayerB(index)
{
  return GetEntData(index,m_OffsetClrRender+2,1);
}

public GetTargetInViewCone(client,Float:max_distance,bool:include_friendlys,Float:cone_angle)
{
    if(ValidPlayer(client))
    {
        if(max_distance<0.0)    max_distance=0.0;
        if(cone_angle<0.0)    cone_angle=0.0;
        
        new Float:PlayerEyePos[3];
        new Float:PlayerAimAngles[3];
        new Float:PlayerToTargetVec[3];
        new Float:OtherPlayerPos[3];
        GetClientEyePosition(client,PlayerEyePos);
        GetClientEyeAngles(client,PlayerAimAngles);
        new Float:ThisAngle;
        new Float:playerDistance;
        new Float:PlayerAimVector[3];
        GetAngleVectors(PlayerAimAngles,PlayerAimVector,NULL_VECTOR,NULL_VECTOR);
        new bestTarget=0;
        new Float:bestTargetDistance;
        for(new i=1;i<=MaxClients;i++)
        {
            if(cone_angle<=0.0)    break;
            if(ValidPlayer(i,true)&& client!=i)
            {
                if(!include_friendlys && GetClientTeam(client) == GetClientTeam(i))
                {
                    continue;
                }
                GetClientEyePosition(i,OtherPlayerPos);
                playerDistance = GetVectorDistance(PlayerEyePos,OtherPlayerPos);
                if(max_distance>0.0 && playerDistance>max_distance)
                {
                    continue;
                }
                SubtractVectors(OtherPlayerPos,PlayerEyePos,PlayerToTargetVec);
                ThisAngle=ArcCosine(GetVectorDotProduct(PlayerAimVector,PlayerToTargetVec)/(GetVectorLength(PlayerAimVector)*GetVectorLength(PlayerToTargetVec)));
                ThisAngle=ThisAngle*360/2/3.14159265;
                if(ThisAngle<=cone_angle)
                {
                    if(TR_DidHit())
                    {
                        new entity=TR_GetEntityIndex();
                        if(entity!=i)
                        {
                            continue;
                        }
                    }
                    if(bestTarget>0)
                    {
                        if(playerDistance<bestTargetDistance)
                        {
                            bestTarget=i;
                            bestTargetDistance=playerDistance;
                        }
                    }
                    else
                    {
                        bestTarget=i;
                        bestTargetDistance=playerDistance;
                    }
                }
            }
        }
        if(bestTarget==0) //still no target, use direct trace
        {
            new Float:endpos[3];
            if(max_distance>0.0)
			{
                ScaleVector(PlayerAimVector,max_distance);
            }
            else
			{
            
                ScaleVector(PlayerAimVector,56756.0);
                AddVectors(PlayerEyePos,PlayerAimVector,endpos);
                if(TR_DidHit())
                {
                    new entity=TR_GetEntityIndex();
                    if(entity>0 && entity<=MaxClients && IsClientConnected(entity) && IsPlayerAlive(entity))
                    {
                        new result=1;
                        if(result!=0)
                        {
                            bestTarget=entity;
                        }
                    }
                }
            }
        }
        return bestTarget;
    }
    return 0;
}

stock SetPlayerColored(client,r,g,b,alpha,time)
{
	SetEntityAlpha(client,255);
	if(player_colored[client] == 0)
	{
		player_colored[client] = 1;
	}
	SetPlayerRGB(client,r,g,b,alpha);
	player_colored_left[client] = time;
	set_hideradar(client,0);
}

stock ResetPlayerRGB(client)
{
	SetEntityRenderColor(client,255,255,255,255);
	SetEntityRenderMode(client,RENDER_NORMAL);
}

stock SetPlayerRGB(index,r,g,b,alpha)
{
	SetEntityRenderMode(index,RENDER_TRANSCOLOR);
	SetEntityRenderColor(index,r,g,b,alpha);    
}

stock Client_RemoveAllWeapons(client, const String:exclude[]="", bool:clearAmmo=false)
{
	new offset = Client_GetWeaponsOffset(client) - 4;
	
	new numWeaponsRemoved = 0;
	for (new i=0; i < MAX_WEAPONS; i++) {
		offset += 4;

		new weapon = GetEntDataEnt2(client, offset);
		
		if (!Weapon_IsValid(weapon)) {
			continue;
		}
		
		if(Entity_ClassNameMatches(weapon, "weapon_c4")) {
			continue;
		}
		
		if (exclude[0] != '\0' && Entity_ClassNameMatches(weapon, exclude)) {
			Client_SetActiveWeapon(client, weapon);
			continue;
		}
		
		if (clearAmmo) {
			Client_SetWeaponPlayerAmmoEx(client, weapon, 0, 0);
		}

		if (RemovePlayerItem(client, weapon)) {
			Entity_Kill(weapon);
		}

		numWeaponsRemoved++;
	}
	
	return numWeaponsRemoved;
}

stock Client_GetWeaponsOffset(client)
{
	static offset = -1;

	if (offset == -1) {
		offset = FindDataMapOffs(client, "m_hMyWeapons");
	}
	
	return offset;
}

stock Weapon_IsValid(weapon)
{
	if (!IsValidEdict(weapon)) {
		return false;
	}
	
	return Entity_ClassNameMatches(weapon, "weapon_", true);
}

stock bool:Entity_ClassNameMatches(entity, const String:className[], partialMatch=false)
{
	decl String:entity_className[64];
	Entity_GetClassName(entity, entity_className, sizeof(entity_className));

	if (partialMatch) {
		return (StrContains(entity_className, className, false) != -1);
	}

	return StrEqual(entity_className, className);
}

stock Client_SetActiveWeapon(client, weapon)
{
	SetEntPropEnt(client, Prop_Data, "m_hActiveWeapon", weapon);
	ChangeEdictState(client, FindDataMapOffs(client, "m_hActiveWeapon"));
}

stock bool:Client_SetWeaponPlayerAmmo(client, const String:className[], primaryAmmo=-1, secondaryAmmo=-1)
{
	new weapon = Client_GetWeapon(client, className);

	if (weapon == INVALID_ENT_REFERENCE) {
		return false;
	}

	Client_SetWeaponPlayerAmmoEx(client, weapon, primaryAmmo, secondaryAmmo);

	return true;
}

stock Client_SetWeaponPlayerAmmoEx(client, weapon, primaryAmmo=-1, secondaryAmmo=-1)
{
	new offset_ammo = FindDataMapOffs(client, "m_iAmmo");

	if (primaryAmmo != -1) {
		new offset = offset_ammo + (Weapon_GetPrimaryAmmoType(weapon) * 4);
		SetEntData(client, offset, primaryAmmo, 4, true);
	}

	if (secondaryAmmo != -1) {
		new offset = offset_ammo + (Weapon_GetSecondaryAmmoType(weapon) * 4);
		SetEntData(client, offset, secondaryAmmo, 4, true);
	}
}

stock bool:Entity_Kill(kenny, killChildren=false)
{
	if (Entity_IsPlayer(kenny)) {
		// Oh My God! They Killed Kenny!!
		ForcePlayerSuicide(kenny);
		return true;
	}
	
	if(killChildren){
		return AcceptEntityInput(kenny, "KillHierarchy");
	}
	else {
		return AcceptEntityInput(kenny, "Kill");
	}
}

stock Entity_GetClassName(entity, String:buffer[], size)
{
	return GetEntPropString(entity, Prop_Data, "m_iClassname", buffer, size);	
}

stock Weapon_GetPrimaryAmmoType(weapon)
{
	return GetEntProp(weapon, Prop_Data, "m_iPrimaryAmmoType");
}

stock Weapon_GetSecondaryAmmoType(weapon)
{
	return GetEntProp(weapon, Prop_Data, "m_iSecondaryAmmoType");
}

stock bool:Entity_IsPlayer(entity)
{
	if (entity < 1 || entity > MaxClients) {
		return false;
	}
	
	return true;
}

stock FlashScreen(client,color[4],Float:holdduration=0.1,Float:fadeduration=0.2,flags=FFADE_IN)
{
    if(ValidPlayer(client,false))
    {
        new Handle:hBf = StartMessageExOne(g_umsgFade,client);
        if(hBf != INVALID_HANDLE)
        {
            if (g_bCanEnumerateMsgType && GetUserMessageType() == UM_Protobuf)
            {
                PbSetInt(hBf, "duration", RoundFloat(255.0*fadeduration));
                PbSetInt(hBf, "hold_time", RoundFloat(255.0*holdduration));
                PbSetInt(hBf, "flags", flags);
                PbSetColor(hBf, "clr", color);
            }
            else
            {
                BfWriteShort(hBf,RoundFloat(255.0*fadeduration));
                BfWriteShort(hBf,RoundFloat(255.0*holdduration)); //holdtime
                BfWriteShort(hBf,flags);
                BfWriteByte(hBf,color[0]);
                BfWriteByte(hBf,color[1]);
                BfWriteByte(hBf,color[2]);
                BfWriteByte(hBf,color[3]);
            }
            EndMessage();
        }
    }
}

stock Handle:StartMessageExOne(UserMsg:msg, client, flags=0)
{
    new players[1];
    players[0] = client;

    return StartMessageEx(msg, players, 1, flags);
}

stock D2_PrecacheParticle( const String:p_strEffectName[] )
{
    static s_numStringTable = INVALID_STRING_TABLE;
    if (s_numStringTable == INVALID_STRING_TABLE)
    {
        s_numStringTable = FindStringTable("ParticleEffectNames");
    }
    AddToStringTable(s_numStringTable, p_strEffectName );
}

public Action:ADM_SetXP(client,args)
{
	if(client!=0&&!HasSMAccess(client,ADMFLAG_RCON)){
		ReplyToCommand(client,"No Access");
	}
	else if(args!=2)
		PrintToConsole(client,"The syntax of the command is: diablo_setxp <player> <xp>",client);
	else
	{
		decl String:match[64];
		GetCmdArg(1,match,sizeof(match));
		decl String:buf[32];
		GetCmdArg(2,buf,sizeof(buf));
		new String:adminname[64];
		if(client!=0)
			GetClientName(client,adminname,sizeof(adminname));
		else
			adminname="Console";
		new xp=StringToInt(buf);
		if(xp<0)
			xp=0;
		new playerlist[66];
		new results=Diablo_PlayerParse(match,playerlist);
		for(new x=0;x<results;x++)
		{
			if(player_class[playerlist[x]]>0)
			{
				player_xp[playerlist[x]] = xp;
				player_lvl[playerlist[x]] = 1;
				LevelCalculated = 1;
				for (new i = 1; i <= sizeof(LevelXPTable)-1; i++ )
				{
					// User has enough XP to advance to the next level
					if ( xp >= LevelXPTable[i])
					{
						LevelCalculated = i+1;
					}
					else
					{
						break;
					}
				}
				reset_skill(playerlist[x]);
				player_lvl[playerlist[x]] = LevelCalculated;
			}
		}
		if(results==0)
			PrintToConsole(client,"No players matched your query",client);
	}
	return Plugin_Handled;
}

public Action:ADM_SetRace(client,args)
{
	if(client!=0&&!HasSMAccess(client,ADMFLAG_RCON)){
		ReplyToCommand(client,"No Access");
	}
	else if(args!=2)
		PrintToConsole(client,"The syntax of the command is: diablo_setrace <player> <race>",client);
	else
	{
		decl String:match[64];
		GetCmdArg(1,match,sizeof(match));
		decl String:buf[32];
		GetCmdArg(2,buf,sizeof(buf));
		new String:adminname[64];
		if(client!=0)
			GetClientName(client,adminname,sizeof(adminname));
		else
			adminname="Console";
		new race=StringToInt(buf);
		if(race<0)
			return Plugin_Handled;
		new playerlist[66];
		new results=Diablo_PlayerParse(match,playerlist);
		for(new x=0;x<results;x++)
		{
			ResetPlayer(playerlist[x]);
			LoadRaceData(playerlist[x],race);
		}
		if(results==0)
			PrintToConsole(client,"No players matched your query",client);
	}
	return Plugin_Handled;
}

public Action:ADM_SetItem(client,args)
{
	if(client!=0&&!HasSMAccess(client,ADMFLAG_RCON)){
		ReplyToCommand(client,"No Access");
	}
	else if(args!=2)
		PrintToConsole(client,"The syntax of the command is: diablo_setitem <player> <item>",client);
	else
	{
		decl String:match[64];
		GetCmdArg(1,match,sizeof(match));
		decl String:buf[32];
		GetCmdArg(2,buf,sizeof(buf));
		new String:adminname[64];
		if(client!=0)
			GetClientName(client,adminname,sizeof(adminname));
		else
			adminname="Console";
		new item=StringToInt(buf);
		if(item<0)
			return Plugin_Handled;
		new playerlist[66];
		new results=Diablo_PlayerParse(match,playerlist);
		for(new x=0;x<results;x++)
		{
			award_item(playerlist[x], item);
			PrintToConsole(client,"Item sended",client);
		}
		if(results==0)
			PrintToConsole(client,"No players matched your query",client);
	}
	return Plugin_Handled;
}

public Diablo_PlayerParse(String:matchstr[],playerlist[])
{
	new i=0;
	if(StrEqual(matchstr,"@all",false))
	{
		// All?
		
		for(new x=1;x<=MaxClients;x++)
		{
			if(ValidPlayer(x))
			{
				playerlist[i++]=x;
			}
		}
	}
	else
	{
		// Team?
		if(StrEqual(matchstr,"@ct",false))
		{
			for(new x=1;x<=MaxClients;x++)
			{
				if(ValidPlayer(x))
				{	
					if(GetClientTeam(x)==3){
						playerlist[i++]=x;
					}
				}
			}
		}
		else if(StrEqual(matchstr,"@t",false))
		{
			for(new x=1;x<=MaxClients;x++)
			{
				if(ValidPlayer(x))
				{	
					if(GetClientTeam(x)==2){
						playerlist[i++]=x;
					}
				}
			}
		}
		else
		{
			// Userid?
			if(matchstr[0]=='@')
			{
				new uid=StringToInt(matchstr[1]); //startign from index 1
				for(new x=1;x<=MaxClients;x++)
				{
					if(ValidPlayer(x))
					{	
						if(GetClientUserId(x)==uid){
							playerlist[i++]=x;
							break;
						}
					}
				}
			}
			else
			{
				// Player name?
				for(new x=1;x<=MaxClients;x++)
				{
					if(ValidPlayer(x))
					{	
						new String:name[64];
						GetClientName(x,name,sizeof(name));
						if(StrContains(name,matchstr,false)!=-1)
						{
							playerlist[i++]=x;
							break;
						}
					}
				}
			}
		}
	}
	return i;
}

stock HasSMAccess(client, flag) {
	new flags = GetUserFlagBits(client);
	//DP("flags %d",flags);
	if (flags & (flag | ADMFLAG_ROOT)) //ADMFLAG_ROOT is "z"
	{
		return true;
	}
	return false;
}

GetCurrency(client)
{
	new currency =  GetEntProp(client, Prop_Send, "m_iAccount");

	return currency;
}

bool:SetCurrency(client, newCurrency)
{
	new oldCurrency = GetCurrency(client);

	if(newCurrency > 16000)
	{
		newCurrency = 16000;
	}
	else if (newCurrency < 0)
	{
		newCurrency = 0;
	}

	// Oops, nothing to do here~!
	if(oldCurrency == newCurrency)
	{
		return false;
	}

	SetEntProp(client, Prop_Send, "m_iAccount", newCurrency);

	return true;
}

public AddCurrency(client, currencyToAdd)
{
	return SetCurrency(client, GetCurrency(client) + currencyToAdd);
}

stock bool:IsOdd(num)
{
    return (num & 1);
}

public ShakeScreen(client, Float:duration, Float:magnitude, Float:noise)
{
    if(ValidPlayer(client,false))
    {
        new Handle:hBf = StartMessageExOne(g_umsgShake,client);
        if(hBf != INVALID_HANDLE)
        {
            if (g_bCanEnumerateMsgType && GetUserMessageType() == UM_Protobuf)
            {
                PbSetInt(hBf, "command", 0);
                PbSetFloat(hBf, "local_amplitude", magnitude);
                PbSetFloat(hBf, "frequency", noise);
                PbSetFloat(hBf, "duration", duration);
            }
            else
            {
                BfWriteByte(hBf,0);
                BfWriteFloat(hBf,magnitude);
                BfWriteFloat(hBf,noise);
                BfWriteFloat(hBf,duration);
            }
            EndMessage();
        }
    }
}

public DealCustomDamage(victim, attacker, damage, String:weapon[])
{
	decl String:dmg_str[16];
    Format(dmg_str,sizeof(dmg_str), "%d", damage);
		
	new pointHurt=CreateEntityByName("point_hurt");
	if(pointHurt)
	{
		//    PrintToChatAll("%d %d %d",victim,damage,g_CurActualDamageDealt);
		DispatchKeyValue(victim,"targetname","diablo_hurtme"); //set victim as the target for damage
		DispatchKeyValue(pointHurt,"Damagetarget","diablo_hurtme");
		DispatchKeyValue(pointHurt,"Damage",dmg_str);
		//DispatchKeyValue(pointHurt,"DamageType",dmg_type_str);
		if(!StrEqual(weapon,"",false))
		{
			DispatchKeyValue(pointHurt,"classname",weapon);
		}
		else{
			DispatchKeyValue(pointHurt,"classname","diablo_damage");
		}
		DispatchSpawn(pointHurt);
		AcceptEntityInput(pointHurt,"Hurt",(attacker>0)?attacker:-1);
		//DispatchKeyValue(pointHurt,"classname","point_hurt");
		DispatchKeyValue(victim,"targetname","diablo_donthurtme"); //unset the victim as target for damage
		RemoveEdict(pointHurt);
		//    PrintToChatAll("%d %d %d",victim,damage,g_CurActualDamageDealt);
	}
}

public Action:Event_OnFlashPlayer(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
		if(!client || !IsClientInGame(client))
			return Plugin_Continue;

		if(wear_sun[client])
		{
			SetEntPropFloat(client, Prop_Send, "m_flFlashMaxAlpha", 0.5);
		}

	return Plugin_Continue;
}

stock bool:IsPlayerStuck(iClient)
{
	decl Float:vecMin[3], Float:vecMax[3], Float:vecOrigin[3];
	
	GetClientMins(iClient, vecMin);
	GetClientMaxs(iClient, vecMax);
	GetClientAbsOrigin(iClient, vecOrigin);
	
	TR_TraceHullFilter(vecOrigin, vecOrigin, vecMin, vecMax, MASK_PLAYERSOLID, TraceEntityFilterSolid);
	return TR_DidHit();	// head in wall ?
}


public bool:TraceEntityFilterSolid(entity, contentsMask) 
{
	if(entity != contentsMask)
	{
		return 0;
	}
	return 1;
}

bool:IsBackDamage(client, target)
{
	new Float:angles1[3], Float:angles2[3];
	GetClientEyeAngles(target,angles2);
	GetClientEyeAngles(client,angles1);

	if(angles1[1] < 0)
	{
		angles1[1]=-1.0*angles1[1];
		angles1[1]=360.0-angles1[1];
	}
	if(angles2[1] < 0)
	{
		angles2[1]=-1.0*angles2[1];
		angles2[1]=360.0-angles2[1];
	}
	
	if(angles1[1] > angles2[1])
	{
		if((angles1[1] >= 270.0) && (angles2[1] <= 90.0))
		{
			return true;
		}
		else if(angles1[1] - angles2[1] < 180.0)
		{
			return true;
		}
	}
	else
	{
		if((angles2[1] >= 270.0) && (angles1[1] <= 90.0))
		{
			return true;
		}
		else if(angles2[1] - angles1[1] < 180.0)
		{
			return true;
		}
	}
	return false;
}


stock AddInFrontOf(const Float:vecOrigin[3], const Float:vecAngle[3], Float:units, Float:output[3])
{
	decl Float:vecView[3];
	GetAngleVectors(vecAngle, vecView, NULL_VECTOR, NULL_VECTOR);
    
	output[0] = vecView[0] * units + vecOrigin[0];
	output[1] = vecView[1] * units + vecOrigin[1];
	output[2] = vecView[2] * units + vecOrigin[2];
}

stock GetShortTeamName(team,String:retstr[],maxlen) 
{
	if(team==1) 
	{
		Format(retstr,maxlen,"%t","CS Spec");
		return;
	}
	if(team==TEAM_T) 
	{
		Format(retstr,maxlen,"%t","CS T");
		return;
	}
	if(team==TEAM_CT) 
	{
		Format(retstr,maxlen,"%t","CS CT");
		return;
	}

	Format(retstr,maxlen,"%t","Unknown Team");
	return;
}

public GetHeadshotDMG(String:weapon[], isarmored, entity)
{
	if(StrEqual(weapon, "weapon_ak47", false))
	{
		switch(isarmored)
		{
			case 0: return 143;
			case 1: return 111;
		}
	}
	else if(StrEqual(weapon, "weapon_m4a1", false))
	{
		switch(isarmored)
		{
			case 0: return 131;
			case 1: return 92;
		}
	}
	else if(StrEqual(weapon, "weapon_awp", false))
	{
		switch(isarmored)
		{
			case 0: return 459;
			case 1: return 448;
		}
	}
	else if(StrEqual(weapon, "weapon_deagle", false))
	{
		switch (GetEntProp(entity, Prop_Send, "m_iItemDefinitionIndex"))
		{
			case 64:
			{
				switch(isarmored)
				{
					case 0: return 131;
					case 1: return 101;
				}
			}
			default:
			{
				switch(isarmored)
				{
					case 0: return 342;
					case 1: return 319;
				}
			}
		}
	}
	else if(StrEqual(weapon, "weapon_fiveseven", false))
	{
		switch(isarmored)
		{
			case 0: return 126;
			case 1: return 115;
		}
	}
	else if(StrEqual(weapon, "weapon_glock", false))
	{
		switch(isarmored)
		{
			case 0: return 111;
			case 1: return 52;
		}
	}
	else if(StrEqual(weapon, "weapon_sg556", false))
	{
		switch(isarmored)
		{
			case 0: return 120;
			case 1: return 120;
		}
	}
	else if(StrEqual(weapon, "weapon_hkp2000", false))
	{
		switch(isarmored)
		{
			case 0: return 140;
			case 1: return 70;
		}
	}
	else if(StrEqual(weapon, "weapon_aug", false))
	{
		switch(isarmored)
		{
			case 0: return 112;
			case 1: return 100;
		}
	}
	else if(StrEqual(weapon, "weapon_famas", false))
	{
		switch(isarmored)
		{
			case 0: return 120;
			case 1: return 84;
		}
	}
	else if(StrEqual(weapon, "weapon_g3sg1", false))
	{
		switch(isarmored)
		{
			case 0: return 319;
			case 1: return 263;
		}
	}
	else if(StrEqual(weapon, "weapon_p90", false))
	{
		switch(isarmored)
		{
			case 0: return 103;
			case 1: return 71;
		}
	}
	else if(StrEqual(weapon, "weapon_ump45", false))
	{
		switch(isarmored)
		{
			case 0: return 140;
			case 1: return 90;
		}
	}
	else if(StrEqual(weapon, "weapon_elite", false))
	{
		switch(isarmored)
		{
			case 0: return 152;
			case 1: return 86;
		}
	}
	else if(StrEqual(weapon, "weapon_scar20", false))
	{
		switch(isarmored)
		{
			case 0: return 319;
			case 1: return 263;
		}
	}
	else if(StrEqual(weapon, "weapon_mp9", false))
	{
		switch(isarmored)
		{
			case 0: return 104;
			case 1: return 61;
		}
	}
	else if(StrEqual(weapon, "weapon_scout", false))
	{
		switch(isarmored)
		{
			case 0: return 299;
			case 1: return 254;
		}
	}
	else if(StrEqual(weapon, "weapon_mp7", false))
	{
		switch(isarmored)
		{
			case 0: return 115;
			case 1: return 71;
		}
	}
	else if(StrEqual(weapon, "weapon_galilar", false))
	{
		switch(isarmored)
		{
			case 0: return 120;
			case 1: return 92;
		}
	}
	else if(StrEqual(weapon, "weapon_mac10", false))
	{
		switch(isarmored)
		{
			case 0: return 114;
			case 1: return 65;
		}
	}
	else if(StrEqual(weapon, "weapon_bizon", false))
	{
		switch(isarmored)
		{
			case 0: return 108;
			case 1: return 61;
		}
	}
	else if(StrEqual(weapon, "weapon_mag7", false))
	{
		switch(isarmored)
		{
			case 0: return 120;
			case 1: return 90;
		}
	}
	else if(StrEqual(weapon, "weapon_negev", false))
	{
		switch(isarmored)
		{
			case 0: return 140;
			case 1: return 105;
		}
	}
	else if(StrEqual(weapon, "weapon_nova", false))
	{
		switch(isarmored)
		{
			case 0: return 106;
			case 1: return 52;
		}
	}
	else if(StrEqual(weapon, "weapon_xm1014", false))
	{
		switch(isarmored)
		{
			case 0: return 80;
			case 1: return 64;
		}
	}
	else if(StrEqual(weapon, "weapon_m249", false))
	{
		switch(isarmored)
		{
			case 0: return 128;
			case 1: return 102;
		}
	}
	else if(StrEqual(weapon, "weapon_fiveseven", false))
	{
		switch(isarmored)
		{
			case 0: return 126;
			case 1: return 115;
		}
	}
	else if(StrEqual(weapon, "weapon_p250", false))
	{
		switch (GetEntProp(entity, Prop_Send, "m_iItemDefinitionIndex"))
		{
			case 63:
			{
				switch(isarmored)
				{
					case 0: return 131;
					case 1: return 101;
				}
			}
			default:
			{
				switch(isarmored)
				{
					case 0: return 138;
					case 1: return 107;
				}
			}
		}
	}
	else if(StrEqual(weapon, "weapon_tec9", false))
	{
		switch(isarmored)
		{
			case 0: return 130;
			case 1: return 118;
		}
	}
	else if(StrEqual(weapon, "weapon_mp5sd", false))
	{
		switch(isarmored)
		{
			case 0: return 107;
			case 1: return 66;
		}
	}
	else if(StrEqual(weapon, "weapon_sawedoff", false))
	{
		switch(isarmored)
		{
			case 0: return 128;
			case 1: return 96;
		}
	}
	else
	{
		return 0;
	}
}

public SpawnWeaponRegister(client)
{
	new offset = Client_GetWeaponsOffset(client) - 4;
	
	new numWeaponsRemoved = 0;
	for (new i=0; i < MAX_WEAPONS; i++) 
	{
		offset += 4;

		new weapon = GetEntDataEnt2(client, offset);
		
		if(weapon < 1 || !IsValidEdict(weapon) || !IsValidEntity(weapon))
		{
		}
		else
		{		
			new String:sWeapon[64];
			GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
			new iWorldModel = GetEntPropEnt(weapon, Prop_Send, "m_hWeaponWorldModel");
			if(IsValidEdict(iWorldModel))
			{
				SDKHook(iWorldModel, SDKHook_SetTransmit, Hook_SetTransmit);
				worldM_owner[iWorldModel] = client;				
			}
			else
			{
				switch (GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex"))
				{
					case 23: strcopy(sWeapon, 64, "weapon_mp5sd");
					case 60: strcopy(sWeapon, 64, "weapon_m4a1_silencer");
					case 61: strcopy(sWeapon, 64, "weapon_usp_silencer");
					case 63: strcopy(sWeapon, 64, "weapon_cz75a");
					case 64: strcopy(sWeapon, 64, "weapon_revolver");
				}
				
				Client_SetWeaponPlayerAmmoEx(client, weapon, 0, 0);

				if (RemovePlayerItem(client, weapon))
				{
					Entity_Kill(weapon);
				}
				GivePlayerItem(client, sWeapon);
			}
		}
		
	}

}

public ShowHUDHint(client, const String:szMessage[], any:...)
{
	decl String:szBuffer[MAX_MESSAGE_LENGTH];
	if (IsClientInGame(client) && !IsFakeClient(client))
	{
		SetGlobalTransTarget(client);
		VFormat(szBuffer, sizeof(szBuffer), szMessage, 3);
		SetHudTextParams(-0.6, 0.4, 5.0, 0, 255, 0, 255, 0, 0, 0, 0);
		ShowHudText(client, -1, "%s", szBuffer);
	}
}

public ShowHUDHintLower(client, const String:szMessage[], any:...)
{
	decl String:szBuffer[MAX_MESSAGE_LENGTH];
	if (IsClientInGame(client) && !IsFakeClient(client))
	{
		SetGlobalTransTarget(client);
		VFormat(szBuffer, sizeof(szBuffer), szMessage, 3);
		SetHudTextParams(0.4, 0.3, 10.0, 0, 255, 0, 255, 0, 0, 0, 0);
		ShowHudText(client, -1, "%s", szBuffer);
	}
}

public Action:ShowPlayerHintRace(Handle:timer, any:client)
{
	if(player_class[client] != 0)
	{
		new String:message[512];
		
		switch(player_class[client])
		{
			case 1: //Mage
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Get knife for fireball", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Press F to use", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Mage", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						Format(message, sizeof(message), "%T", "This race is not recommended", client);
						player_lasthint[client]++;
					}
					case 4:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Get knife for fireball", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 2: //Monk
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "This race is not recommended", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Use Monk Wall", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Monk", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "This race is not recommended", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 3: //Paladin
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Use longjump", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Charge magic bullet", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Paladin", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Use longjump", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 4: //ASSASSIN
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Use throw knife", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Everyone can pick up knives", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Use assassin skill", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						Format(message, sizeof(message), "%T", "Beware blood on your body", client);
						player_lasthint[client]++;
					}
					case 4:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Assassin", client);
						player_lasthint[client]++;
					}
					case 5:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Use throw knife", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 5: //NECROMANCER
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Revive skill", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "You have vampiric", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Necromancer", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Revive skill", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 6: //BARBARIAN
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Get armour for kill", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Skill Barbarian", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Barbarian", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Get armour for kill", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 7: //Ninja
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Knife only race", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Use throw knife", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Everyone can pick up knives", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						Format(message, sizeof(message), "%T", "Beware blood on your body", client);
						player_lasthint[client]++;
					}
					case 4:
					{
						Format(message, sizeof(message), "%T", "Be silent ninja", client);
						player_lasthint[client]++;
					}
					case 5:
					{
						Format(message, sizeof(message), "%T", "Leveup skills ninja", client);
						player_lasthint[client]++;
					}
					case 6:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Knife only race", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
			case 8: //Amazon
			{
				switch(player_lasthint[client])
				{
					case 0:
					{
						Format(message, sizeof(message), "%T", "Use crossbow", client);
						player_lasthint[client]++;
					}
					case 1:
					{
						Format(message, sizeof(message), "%T", "Trap grenades", client);
						player_lasthint[client]++;
					}
					case 2:
					{
						Format(message, sizeof(message), "%T", "Charge Amazon", client);
						player_lasthint[client]++;
					}
					case 3:
					{
						Format(message, sizeof(message), "%T", "Shift enemy", client);
						player_lasthint[client]++;
					}
					case 4:
					{
						Format(message, sizeof(message), "%T", "Leveup skills Amazon", client);
						player_lasthint[client]++;
					}
					case 5:
					{
						if(player_item_id[client])
						{
							Format(message, sizeof(message), "%T", "You have an item", client);
							player_lasthint[client] = 0;
						}
						else
						{
							Format(message, sizeof(message), "%T", "Use crossbow", client);
							player_lasthint[client] = 1;
						}
					}
				}
			}
		}
		
		ShowHUDHintLower(client, message);
	}
}

public FreeBonus(client)
{
	if(player_totallvl[client] < 20 && player_class[client] > 0)
	{
		new String:killaward[64];
		Format(killaward, sizeof(killaward), "%T", "free bonus", client);
		Give_Xp(client,11750,killaward);
		new String:authid[64];
		GetClientAuthId(client, AuthId_Steam2, authid, sizeof(authid));
		new min_players = GetConVarInt(g_hCVXPminplayers);
		new current_plr = PlayersOnTeam(2) + PlayersOnTeam(3);
		if(current_plr>=min_players)
		{
			LogToFile(diablo_bonus,"Steam_id: %s | Class: %d | Bonus 11750",authid,player_class[client]);
		}
	}
	else
	{
		Diablo_ChatMessage(client,"%T","free is not aviable",client);
	}
}

stock bool:IsPlayer(number)
{ 
    return MaxClients>=number>0;
}  