/** Double-include prevention */
#if defined _war3_vip_included_
  #endinput
#endif
#define _war3_vip_included_
 
/**
 * 
 */
native bool:GetPlayerPremium(player);
native bool:VIPGetPlayerarmour(player);
native bool:VIPGetPlayergrenade(player);
native bool:VIPGetPlayerhp(player);
native bool:VIPGetPlayerdoublexp(player);