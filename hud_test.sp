#include <sourcemod> 
#include <sdktools> 


new Handle:gameinstructor_enable;
new cvar_value;

public OnPluginStart() 
{ 
    RegConsoleCmd("sm_testing", probar); 
	AddAmbientSoundHook(AmbientSHook:OnNormalSoundPlayed);
	gameinstructor_enable = FindConVar("gameinstructor_enable");
}

public OnClientPostAdminCheck(client) {
	// Don't send the value to fake clients
	if (IsFakeClient(client)) {
		return;
	}

		SendConVarValue(client, gameinstructor_enable, "1");
		ClientCommand(client, "gameinstructor_enable 1");
}

public Action:OnNormalSoundPlayed(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{	
	if (0 < entity <= MaxClients)
	{
		if(StrContains(sample, "beepclear") != -1)
		{
			StopSound(entity, SNDCHAN_STATIC, "ui/beepclear.wav");
			return Plugin_Stop;
		}
	}
	return Plugin_Continue;
}

public Action probar (client, args) 
{ 
    QueryClientConVar(client, "gameinstructor_enable", ConVarQueryFinished:ClientConVar, client)
     
    return Plugin_Handled; 
}

public ClientConVar(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[])
{
	cvar_value = StringToInt(cvarValue);
	if(cvar_value > 0)
	{
		DisplayInstructorHint(client, 5.0, 0.1, 0.1, false, false, "use_binding", "use_binding", "reload", false, {0, 255, 0}, "Жми %+reload% для метания ножей");
	}
	else
	{
		PrintToChat(client, "Жми %+jump% для активации предмета"); // show chat debug 
	}
}

stock void DisplayInstructorHint(int iTargetEntity, float fTime, float fHeight, float fRange, bool bFollow, bool bShowOffScreen, char[] sIconOnScreen, char[] sIconOffScreen, char[] sCmd, bool bShowTextAlways, int iColor[3], char sText[100])  
{  
    int iEntity = CreateEntityByName("env_instructor_hint");  
      
    if(iEntity <= 0)  
        return;  
          
    char sBuffer[32];  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", iTargetEntity);  
      
    // Target  
    DispatchKeyValue(iTargetEntity, "targetname", sBuffer);  
    DispatchKeyValue(iEntity, "hint_target", sBuffer);  
      
    // Static  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", !bFollow);  
    DispatchKeyValue(iEntity, "hint_static", sBuffer);  
      
    // Timeout  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fTime));  
    DispatchKeyValue(iEntity, "hint_timeout", sBuffer);  
    if(fTime > 0.0)  
        RemoveEntity(iEntity, fTime);  
      
    // Height  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fHeight));  
    DispatchKeyValue(iEntity, "hint_icon_offset", sBuffer);  
      
    // Range  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fRange));  
    DispatchKeyValue(iEntity, "hint_range", sBuffer);  
      
    // Show off screen  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", !bShowOffScreen);  
    DispatchKeyValue(iEntity, "hint_nooffscreen", sBuffer);  
      
    // Icons  
    DispatchKeyValue(iEntity, "hint_icon_onscreen", sIconOnScreen);  
    DispatchKeyValue(iEntity, "hint_icon_offscreen", sIconOffScreen);  
      
    // Command binding  
    DispatchKeyValue(iEntity, "hint_binding", sCmd);  
      
    // Show text behind walls  
    FormatEx(sBuffer, sizeof(sBuffer), "%d", bShowTextAlways);  
    DispatchKeyValue(iEntity, "hint_forcecaption", sBuffer);  
      
    // Text color  
    FormatEx(sBuffer, sizeof(sBuffer), "%d %d %d", iColor[0], iColor[1], iColor[2]);  
    DispatchKeyValue(iEntity, "hint_color", sBuffer);  
      
    //Text  
    ReplaceString(sText, sizeof(sText), "\n", " ");  
    DispatchKeyValue(iEntity, "hint_caption", sText);  
      
    DispatchSpawn(iEntity);  
    AcceptEntityInput(iEntity, "ShowHint");  
}  

stock void RemoveEntity(entity, float time = 0.0)  
{  
    if (time == 0.0)  
    {  
        if (IsValidEntity(entity))  
        {  
            char edictname[32];  
            GetEdictClassname(entity, edictname, 32);  

            if (!StrEqual(edictname, "player"))  
                AcceptEntityInput(entity, "kill");  
        }  
    }  
    else if(time > 0.0)  
        CreateTimer(time, RemoveEntityTimer, EntIndexToEntRef(entity), TIMER_FLAG_NO_MAPCHANGE);  
}  

public Action RemoveEntityTimer(Handle Timer, any entityRef)  
{  
    int entity = EntRefToEntIndex(entityRef);  
    if (entity != INVALID_ENT_REFERENCE)  
        RemoveEntity(entity); // RemoveEntity(...) is capable of handling references  
      
    return (Plugin_Stop);  
}  