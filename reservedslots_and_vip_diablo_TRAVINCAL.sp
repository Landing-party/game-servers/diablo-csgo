/**
 * vim: set ts=4 :
 * =============================================================================
 * SourceMod Reserved Slots Plugin
 * Provides basic reserved slots.
 *
 * SourceMod (C)2004-2008 AlliedModders LLC.  All rights reserved.
 * =============================================================================
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As a special exception, AlliedModders LLC gives you permission to link the
 * code of this program (as well as its derivative works) to "Half-Life 2," the
 * "Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
 * by the Valve Corporation.  You must obey the GNU General Public License in
 * all respects for all other code used.  Additionally, AlliedModders LLC grants
 * this exception to all derivative works.  AlliedModders LLC defines further
 * exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
 * or <http://www.sourcemod.net/license.php>.
 *
 * Version: $Id$
 */

#pragma semicolon 1
#include <dbi>

#include <sourcemod>

#define sql_table "vip_trav"

public Plugin:myinfo = 
{
	name = "Reserved Slots and VIP SQL system Diablo",
	author = "AlliedModders LLC",
	description = "Provides basic reserved slots",
	version = SOURCEMOD_VERSION,
	url = "http://www.sourcemod.net/"
};

new g_adminCount = 0;
new bool:g_isAdmin[MAXPLAYERS+1];

/* Handles to convars used by plugin */
new Handle:sm_reserved_slots;
new Handle:sm_hide_slots;
new Handle:sv_visiblemaxplayers;
new Handle:sm_reserve_type;
new Handle:sm_reserve_maxadmins;
new Handle:sm_reserve_kicktype;

new Handle:db;
new bool:IsAPremium[ MAXPLAYERS + 1 ];
new bool:doublexp[ MAXPLAYERS + 1 ];
new bool:armour[ MAXPLAYERS + 1 ];
new bool:grenade[ MAXPLAYERS + 1 ];
new bool:hp[ MAXPLAYERS + 1 ];
new bool:expired[ MAXPLAYERS + 1 ];
new bool:changed[ MAXPLAYERS + 1 ];
new bool:IsSteam[ MAXPLAYERS + 1 ];

#define MAX_BUFF        512
//new String:part[MAX_BUFF];
new steamclients = 0;

#define CHOICE1 "#choice1"
#define CHOICE2 "#choice2"
#define CHOICE3 "#choice3"
#define CHOICE4 "#choice4"

enum KickType
{
	Kick_HighestPing,
	Kick_HighestTime,
	Kick_Random,	
};

public OnPluginStart()
{
	LoadTranslations("reservedslots.phrases");
	LoadTranslations("war_vip.phrases");
	RegConsoleCmd("vip", VipMenu);
	
	sm_reserved_slots = CreateConVar("sm_reserved_slots", "0", "Number of reserved player slots", 0, true, 0.0);
	sm_hide_slots = CreateConVar("sm_hide_slots", "0", "If set to 1, reserved slots will hidden (subtracted from the max slot count)", 0, true, 0.0, true, 1.0);
	sv_visiblemaxplayers = FindConVar("sv_visiblemaxplayers");
	sm_reserve_type = CreateConVar("sm_reserve_type", "0", "Method of reserving slots", 0, true, 0.0, true, 2.0);
	sm_reserve_maxadmins = CreateConVar("sm_reserve_maxadmins", "1", "Maximum amount of admins to let in the server with reserve type 2", 0, true, 0.0);
	sm_reserve_kicktype = CreateConVar("sm_reserve_kicktype", "0", "How to select a client to kick (if appropriate)", 0, true, 0.0, true, 2.0);
	
	HookConVarChange(sm_reserved_slots, SlotCountChanged);
	HookConVarChange(sm_hide_slots, SlotHideChanged);
	HookEvent("round_start",RoundStartEvent);
	steamclients = 0;
}

stock SetSafeMenuTitle(Handle:menu, const String:fmt[], any:...)
{
  decl String:menuTitle[4096];
  VFormat(menuTitle, sizeof(menuTitle), fmt, 3);
  
  SetMenuTitle(menu, menuTitle);
}

stock GetLastDay(day)
{
	new Float:time, Float:time2;
	time = float(day) - GetTime();
	time2 = time/86400;
	return RoundToFloor(time2);
}

public ShowVip(client)
{
	if(IsAPremium[ client ])
	{
		new String:enabled[16], String:disabled[16], String:doublexp_t[32], String:armour_t[32], String:grenade_t[32], String:days_t[32], String:expired_text[64], String:menu_item[64], String:menu_title[32];
		
		Format(enabled, sizeof(enabled), "%T", "Enabled", LANG_SERVER);
		Format(disabled, sizeof(disabled), "%T", "Disabled", LANG_SERVER);
		Format(doublexp_t, sizeof(doublexp_t), "%T", "doublexp", LANG_SERVER);
		Format(armour_t, sizeof(armour_t), "%T", "armour", LANG_SERVER);
		Format(grenade_t, sizeof(grenade_t), "%T", "grenade", LANG_SERVER);
		Format(days_t, sizeof(days_t), "%T", "days left", LANG_SERVER);
		
		Format(menu_title, sizeof(menu_title), "%T", "Vip menu", LANG_SERVER);
		Format(expired_text, sizeof(expired_text), "%s\n%d %s", menu_title, GetLastDay(expired[client]), days_t);
		
		new Handle:menu = CreateMenu(MenuHandler1, MENU_ACTIONS_ALL);
		SetSafeMenuTitle(menu, expired_text);
		if(doublexp[client])
		{
			Format(menu_item, sizeof(menu_item), "%s %s", doublexp_t, enabled);
			AddMenuItem(menu, CHOICE1, menu_item);
		}
		else
		{
			Format(menu_item, sizeof(menu_item), "%s %s", doublexp_t, disabled);
			AddMenuItem(menu, CHOICE1, menu_item);
		}
		if(armour[client])
		{
			Format(menu_item, sizeof(menu_item), "%s %s", armour_t, enabled);
			AddMenuItem(menu, CHOICE2, menu_item);
		}
		else
		{
			Format(menu_item, sizeof(menu_item), "%s %s", armour_t, disabled);
			AddMenuItem(menu, CHOICE2, menu_item);
		}
		if(grenade[client])
		{
			Format(menu_item, sizeof(menu_item), "%s %s", grenade_t, enabled);
			AddMenuItem(menu, CHOICE3, menu_item);
		}
		else
		{
			Format(menu_item, sizeof(menu_item), "%s %s", grenade_t, disabled);
			AddMenuItem(menu, CHOICE3, menu_item);
		}
		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public Action:VipMenu(client, args)
{
	ShowVip(client);
 
	return Plugin_Handled;
}

public MenuHandler1(Handle:menu, MenuAction:action, param1, param2)
{
	switch(action)
	{
 
		case MenuAction_Select:
		{
			decl String:info[32];
			GetMenuItem(menu, param2, info, sizeof(info));
			if(StrEqual(info, CHOICE1))
			{
				changed[param1] = true;
				if(doublexp[param1])
				{
					doublexp[param1] = false;
				}
				else
				{
					doublexp[param1] = true;
				}
			}
			if (StrEqual(info, CHOICE2))
			{
				changed[param1] = true;
				if(armour[param1])
				{
					armour[param1] = false;
				}
				else
				{
					armour[param1] = true;
				}
			}
			if (StrEqual(info, CHOICE3))
			{
				changed[param1] = true;
				if(grenade[param1])
				{
					grenade[param1] = false;
				}
				else
				{
					grenade[param1] = true;
				}
			}
			ShowVip(param1);
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
 
	return 0;
}

public OnPluginEnd()
{
	/* 	If the plugin has been unloaded, reset visiblemaxplayers. In the case of the server shutting down this effect will not be visible */
	ResetVisibleMax();
}

public OnMapStart()
{
	if (GetConVarBool(sm_hide_slots))
	{		
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - GetConVarInt(sm_reserved_slots));
	}
}

stock bool:IsValidPlayer(const any:client) {
 return (
  0 < client <= MaxClients &&
  IsClientInGame(client)
 );
}

public RoundStartEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
    for(new x=1;x<=MAXPLAYERS;x++)
	{
        if(IsValidPlayer(x))
		{
			if(changed[x])
			{
				new String:authid[64];
				GetClientAuthId(x, AuthId_Steam2, authid, 64);

				new String:Query[255];
				Format(Query, sizeof(Query), "UPDATE `%s` SET `doublexp` = '%d', `armour` = '%d', `grenade` = '%d', `hp` = '%d' WHERE `steamid` LIKE  '%s';", sql_table, doublexp[x], armour[x], grenade[x], hp[x], authid);
				SQL_TQuery(db, SQL_ErrorCheckCallBack2, Query, x);
				changed[x] = false;
			}
		}
	}
}

public SQL_Init_Connect(&Handle:DbHNDL)
{
	// Errormessage Buffer
	new String:Error[255];
	
	// COnnect to the DB
	DbHNDL = SQL_Connect("shop", true, Error, sizeof(Error));
	
	if(DbHNDL == INVALID_HANDLE)
	{
		SetFailState(Error);
	}
}

public SQL_ErrorCheckCallBack2(Handle:owner, Handle:hndl, const String:error[], any:id)
{
	// This is just an errorcallback for function who normally don't return any data
	
	if(hndl == INVALID_HANDLE)
	{
		LogToFile ("conn_error.log"," %s", error);
		SetFailState("Query failed! %s", error);
	}
}

public SQL_ErrorCheckCallBack(Handle:owner, Handle:hndl, const String:error[], any:id)
{
	// This is just an errorcallback for function who normally don't return any data
	
	if(hndl == INVALID_HANDLE)
	{
		LogToFile ("conn_error.log"," %s", error);
		SetFailState("Query failed! %s", error);
	}
	if(SQL_FetchRow(hndl))
	{
		IsAPremium[ id ] = 1;
		doublexp[ id ] = SQL_FetchInt(hndl, 0);
		armour[ id ] = SQL_FetchInt(hndl, 1);
		grenade[ id ] = SQL_FetchInt(hndl, 2);
		hp[ id ] = SQL_FetchInt(hndl, 3);
		expired[ id ] = SQL_FetchInt(hndl, 4);
	}
}

public OnConfigsExecuted()
{
	SQL_Init_Connect(db);
	if (GetConVarBool(sm_hide_slots))
	{
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - GetConVarInt(sm_reserved_slots));
	}	
}

public Action:OnTimedKick(Handle:timer, any:client)
{	
	if (!client || !IsClientInGame(client))
	{
		return Plugin_Handled;
	}
	
	KickClient(client, "%T", "Slot reserved", client);
	
	if (GetConVarBool(sm_hide_slots))
	{				
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - GetConVarInt(sm_reserved_slots));
	}
	
	return Plugin_Handled;
}

public OnClientPutInServer(id) 
{
	new String:authid[64];
	GetClientAuthId(id, AuthId_Steam2, authid, 64);

	new String:Query[255];
	Format(Query, sizeof(Query), "SELECT `doublexp`, `armour`, `grenade`, `hp`, `expired` FROM  `%s` WHERE  `steamid` LIKE  '%s';", sql_table, authid);

	// Send our Query to the Function
	//LogToFile ("check.log"," %s", Query);
	if(IsClientConnected(id))
	{
		SQL_TQuery(db, SQL_ErrorCheckCallBack, Query, id);
	}
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("GetPlayerPremium", Native_GetPlayerPremium);
	CreateNative("VIPGetPlayerarmour", Native_VIPGetPlayerarmour);
	CreateNative("VIPGetPlayergrenade", Native_VIPGetPlayergrenade);
	CreateNative("VIPGetPlayerhp", Native_VIPGetPlayerhp);
	CreateNative("VIPGetPlayerdoublexp", Native_VIPGetPlayerdoublexp);
	RegPluginLibrary("war3_vip");
	return APLRes_Success;
}

public Native_GetPlayerPremium(Handle:plugin, numParams)
{
   if(IsAPremium[GetNativeCell(1)])
   {
		return 1;
   }
   else
   {
		return 0;
   }
}

public Native_VIPGetPlayerarmour(Handle:plugin, numParams)
{
   if(armour[GetNativeCell(1)])
   {
		return 1;
   }
   else
   {
		return 0;
   }
}

public Native_VIPGetPlayergrenade(Handle:plugin, numParams)
{
   if(grenade[GetNativeCell(1)])
   {
		return 1;
   }
   else
   {
		return 0;
   }
}

public Native_VIPGetPlayerhp(Handle:plugin, numParams)
{
   if(hp[GetNativeCell(1)])
   {
		return 1;
   }
   else
   {
		return 0;
   }
}

public Native_VIPGetPlayerdoublexp(Handle:plugin, numParams)
{
   if(doublexp[GetNativeCell(1)])
   {
		return 1;
   }
   else
   {
		return 0;
   }
}

public OnClientAuthorized(client, const String:auth2[])
{
	//ReplaceString(authid, sizeof(authid), "STEAM_1:", "", false);
	//LogToFile ("steamid.log","STEAM %s", authid);
	//LogToFile ("steamid.log","STEAM2 %s", authid[0]);
	if( strlen(auth2) < 19 )
    {
		//LogToFile ("steamid.log","%s IS STEAM", auth2);
		IsSteam[ client ] = 1;
		steamclients++;
		//new len = strlen(auth2);
		//LogToFile ("resconn.log","Steam %s is steam. len %d. steamclients now %d", auth2, len, steamclients);
	}
	else
	{
		//LogToFile ("steamid.log","%s IS NON-STEAM", auth2);
	}
	
	new String:Query[255];
	Format(Query, sizeof(Query), "SELECT `id` FROM  `%s` WHERE  `steamid` LIKE  '%s';", sql_table, auth2);

	// Send our Query to the Function
	SQL_TQuery(db, SQL_ErrorCheckCallBack, Query, client);
}

public OnClientPostAdminCheck(client)
{
	new reserved = GetConVarInt(sm_reserved_slots);

	if (reserved > 0)
	{
		new clients = GetClientCount(false);
		new limit = GetMaxHumanPlayers() - reserved;
		new flags = GetUserFlagBits(client);
		
		new type = GetConVarInt(sm_reserve_type);
		
		if (type == 0)
		{
			if (clients <= limit || IsFakeClient(client) || flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION || IsAPremium[client])
			{
				if (GetConVarBool(sm_hide_slots))
				{
					SetVisibleMaxSlots(clients, limit);
				}
				
				return;
			}
			
			/* Kick player because there are no public slots left */
			CreateTimer(0.1, OnTimedKick, client);
		}
		else if (type == 1)
		{	
			new String:authid2[64];	
			GetClientAuthId(client, AuthId_Steam2, authid2, 64);
			if ((flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION) && !IsSteam[client])
			{
				steamclients++;
				//LogToFile ("resconn.log","Steam %s is non-steam admin. steamclients now %d", authid2, steamclients);
			}
			if (IsAPremium[client] && !IsSteam[client])
			{
				steamclients++;
				//LogToFile ("resconn.log","Steam %s is non-steam premium. steamclients now %d", authid2, steamclients);
			}
			if (clients >= limit)
			{
				if (flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION || IsAPremium[client])
				{
					new target;
					if(steamclients >= limit)
					{
						target = SelectKickClient();
					}
					else
					{				
						target = SelectKickNoSteamClient();
					}
						
					if (target)
					{
						/* Kick public player to free the reserved slot again */
						CreateTimer(0.1, OnTimedKick, target);
						new String:authid[64];	
						GetClientAuthId(target, AuthId_Steam2, authid, 64);
						//LogToFile ("reserved.log","Steam %s kicked. %s connected.", authid, authid2);
						//LogToFile ("reserved.log","Premium or Admin. Issteam %d. lim %d.steamcount %d. currplr %d.", IsSteam[client], limit, steamclients, clients);
					}
				}
				else if(IsSteam[client])
				{
					new target;
					if(steamclients >= limit)
					{
						CreateTimer(0.1, OnTimedKick, client);
					}
					else
					{				
						target = SelectKickNoSteamClient();
					}
						
					if (target)
					{
						/* Kick public player to free the reserved slot again */
						CreateTimer(0.1, OnTimedKick, target);
						new String:authid[64];	
						GetClientAuthId(target, AuthId_Steam2, authid, 64);
						//LogToFile ("reserved.log","Steam %s kicked. %s connected.", authid, authid2);
						//LogToFile ("reserved.log","Steam. Issteam %d. lim %d.steamcount %d. currplr %d.", IsSteam[client], limit, steamclients, clients);
					}
				
				}
				else
				{				
					/* Kick player because there are no public slots left */
					CreateTimer(0.1, OnTimedKick, client);
					new String:authid[64];	
					GetClientAuthId(client, AuthId_Steam2, authid, 64);
					//LogToFile ("reserved.log","Steam %s connected and kicked.", authid);
					//LogToFile ("reserved.log","NonSteam, Nopremium. Issteam %d. lim %d.steamcount %d. currplr %d.", IsSteam[client], limit, steamclients, clients);
				}
			}
		}
		else if (type == 2)
		{
			if (flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION || IsAPremium[client])
			{
				g_adminCount++;
				g_isAdmin[client] = true;
			}
			
			if (clients > limit && g_adminCount < GetConVarInt(sm_reserve_maxadmins))
			{
				/* Server is full, reserved slots aren't and client doesn't have reserved slots access */
				
				if (g_isAdmin[client])
				{
					new target = SelectKickClient();
						
					if (target)
					{
						/* Kick public player to free the reserved slot again */
						CreateTimer(0.1, OnTimedKick, target);
					}
				}
				else
				{				
					/* Kick player because there are no public slots left */
					CreateTimer(0.1, OnTimedKick, client);
				}		
			}
		}
	}
}

public OnClientDisconnect(client)
{
	if (GetConVarBool(sm_hide_slots))
	{		
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - GetConVarInt(sm_reserved_slots));
	}
	new flags = GetUserFlagBits(client);
	
	if (g_isAdmin[client])
	{
		g_adminCount--;
		g_isAdmin[client] = false;	
	}
	
	if(IsSteam[ client ] || IsAPremium[ client ] || (flags & ADMFLAG_ROOT) || (flags & ADMFLAG_RESERVATION))
	{
		steamclients--;
		new String:authid2[64];	
		GetClientAuthId(client, AuthId_Steam2, authid2, 64);
		//LogToFile ("resconn.log","Steam %s is disconnect. steamclients now %d", authid2, steamclients);
	}
	IsAPremium[ client ] = 0;
	IsSteam[ client ] = 0;
	doublexp[ client ] = 0;
	armour[ client ] = 0;
	grenade[ client ] = 0;
	hp[ client ] = 0;
}

public SlotCountChanged(Handle:convar, const String:oldValue[], const String:newValue[])
{
	/* Reserved slots or hidden slots have been disabled - reset sv_visiblemaxplayers */
	new slotcount = GetConVarInt(convar);
	if (slotcount == 0)
	{
		ResetVisibleMax();
	}
	else if (GetConVarBool(sm_hide_slots))
	{
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - slotcount);
	}
}

public SlotHideChanged(Handle:convar, const String:oldValue[], const String:newValue[])
{
	/* Reserved slots or hidden slots have been disabled - reset sv_visiblemaxplayers */
	if (!GetConVarBool(convar))
	{
		ResetVisibleMax();
	}
	else
	{
		SetVisibleMaxSlots(GetClientCount(false), GetMaxHumanPlayers() - GetConVarInt(sm_reserved_slots));
	}
}

SetVisibleMaxSlots(clients, limit)
{
	new num = clients;
	
	if (clients == GetMaxHumanPlayers())
	{
		num = GetMaxHumanPlayers();
	} else if (clients < limit) {
		num = limit;
	}
	
	SetConVarInt(sv_visiblemaxplayers, num);
}

ResetVisibleMax()
{
	SetConVarInt(sv_visiblemaxplayers, -1);
}

SelectKickNoSteamClient()
{
	new KickType:type = KickType:GetConVarInt(sm_reserve_kicktype);
	
	new Float:highestValue;
	new highestValueId;
	
	new Float:highestSpecValue;
	new highestSpecValueId;
	
	new bool:specFound;
	
	new Float:value;
	
	for (new i=1; i<=MaxClients; i++)
	{	
		if (!IsClientConnected(i))
		{
			continue;
		}
	
		new flags = GetUserFlagBits(i);
		
		if (IsFakeClient(i) || flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION || CheckCommandAccess(i, "sm_reskick_immunity", ADMFLAG_RESERVATION, false) || IsAPremium[i] || IsSteam[i])
		{
			continue;
		}
		
		value = 0.0;
			
		if (IsClientInGame(i))
		{
			if (type == Kick_HighestPing)
			{
				value = GetClientAvgLatency(i, NetFlow_Outgoing);
			}
			else if (type == Kick_HighestTime)
			{
				value = GetClientTime(i);
			}
			else
			{
				value = GetRandomFloat(0.0, 100.0);
			}

			if (IsClientObserver(i))
			{			
				specFound = true;
				
				if (value > highestSpecValue)
				{
					highestSpecValue = value;
					highestSpecValueId = i;
				}
			}
		}
		
		if (value >= highestValue)
		{
			highestValue = value;
			highestValueId = i;
		}
	}
	
	if (specFound)
	{
		return highestSpecValueId;
	}
	
	return highestValueId;
}

SelectKickClient()
{
	new KickType:type = KickType:GetConVarInt(sm_reserve_kicktype);
	
	new Float:highestValue;
	new highestValueId;
	
	new Float:highestSpecValue;
	new highestSpecValueId;
	
	new bool:specFound;
	
	new Float:value;
	
	for (new i=1; i<=MaxClients; i++)
	{	
		if (!IsClientConnected(i))
		{
			continue;
		}
	
		new flags = GetUserFlagBits(i);
		
		if (IsFakeClient(i) || flags & ADMFLAG_ROOT || flags & ADMFLAG_RESERVATION || CheckCommandAccess(i, "sm_reskick_immunity", ADMFLAG_RESERVATION, false) || IsAPremium[i])
		{
			continue;
		}
		
		value = 0.0;
			
		if (IsClientInGame(i))
		{
			if (type == Kick_HighestPing)
			{
				value = GetClientAvgLatency(i, NetFlow_Outgoing);
			}
			else if (type == Kick_HighestTime)
			{
				value = GetClientTime(i);
			}
			else
			{
				value = GetRandomFloat(0.0, 100.0);
			}

			if (IsClientObserver(i))
			{			
				specFound = true;
				
				if (value > highestSpecValue)
				{
					highestSpecValue = value;
					highestSpecValueId = i;
				}
			}
		}
		
		if (value >= highestValue)
		{
			highestValue = value;
			highestValueId = i;
		}
	}
	
	if (specFound)
	{
		return highestSpecValueId;
	}
	
	return highestValueId;
}